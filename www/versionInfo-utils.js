function loadVersionStrings(success, fail) {
    if (dolog) console.log('loadVersionStrings');
    var promises = []; var px = 0;

    var def1 = $.Deferred();
    navigator.versionInfo.versionName(null, 
        function(r) {
            if (dolog) console.log('versionNumber = ' + r);
            if (r) versionNumber = r;
            def1.resolve();
        }, function() {
            def1.reject();
        }
    );
    promises[px++] = def1.promise();

    var def2 = $.Deferred();
    navigator.versionInfo.appName(null, 
        function(r) {
            if (dolog) console.log('app_name = ' + r);
            if (r) app_name = r;
            def2.resolve();
        }, function() {
            def2.reject();
        }
    );
    promises[px++] = def2.promise();

    var def3 = $.Deferred();
    navigator.versionInfo.valueString("dataFolder", 
        function(r) {
            if (dolog) console.log('dataFolder = ' + r);
            if (r) dataFolder = r;
            def3.resolve();
        }, function() {
            def3.reject();
        }
    );
    promises[px++] = def3.promise();

    $.when.apply($, promises).done(function() {
        if (success) success();
    }).fail(function(error) {
        if (fail) fail(error);
    });
}
