var iBTVM = function() {
};

iBTVM.prototype.listDevices = function(argument, cbSuccess, cbError) {
    return cordova.exec(cbSuccess, cbError, 'iBTVM', 'listDevices', [argument]);
};

iBTVM.prototype.initialize = function(argument, cbSuccess, cbError) {
    return cordova.exec(cbSuccess, cbError, 'iBTVM', 'initialize', [argument]);
};

iBTVM.prototype.connect = function(dev, cbSuccess, cbError, getConfig) {
    return cordova.exec(cbSuccess, cbError, 'iBTVM', 'connect', [dev, getConfig]);
};

iBTVM.prototype.disconnect = function(argument, cbSuccess, cbError) {
    return cordova.exec(cbSuccess, cbError, 'iBTVM', 'disconnect', [argument]);
};

/* -- Simple/Diagnostic commands -- */
iBTVM.prototype.getVoltage = function(argument, cbSuccess, cbError) {
    return cordova.exec(cbSuccess, cbError, 'iBTVM', 'getVoltage', [argument]);
};
iBTVM.prototype.getRange = function(argument, cbSuccess, cbError) {
    return cordova.exec(cbSuccess, cbError, 'iBTVM', 'getRange', [argument]);
};
iBTVM.prototype.getACreject = function(argument, cbSuccess, cbError) {
    return cordova.exec(cbSuccess, cbError, 'iBTVM', 'getACreject', [argument]);
};
iBTVM.prototype.getStatus = function(doSendCmd, cbSuccess, cbError) {
    return cordova.exec(cbSuccess, cbError, 'iBTVM', 'getStatus', [doSendCmd]);
};
iBTVM.prototype.diagnostic = function(argument, cbSuccess, cbError) {
    return cordova.exec(cbSuccess, cbError, 'iBTVM', 'diagnostic', [argument]);
};
/* --  -- */


iBTVM.prototype.getConfig = function(argument, cbSuccess, cbError) {
    return cordova.exec(cbSuccess, cbError, 'iBTVM', 'getConfig', [argument]);
};

iBTVM.prototype.setConfig = function(config, cbSuccess, cbError) {
    return cordova.exec(cbSuccess, cbError, 'iBTVM', 'setConfig', [config]);
};

iBTVM.prototype.updateConfig = function(json, cbSuccess, cbError, force) {
    return cordova.exec(cbSuccess, cbError, 'iBTVM', 'updateConfig', [json, force]);
};

iBTVM.prototype.start = function(freq, successCallback, failureCallback, simple) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'start', [freq, simple]);
};

iBTVM.prototype.stop = function(argument,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'stop', [argument]);
};


iBTVM.prototype.getReading = function(reqTime,successCallback, failureCallback, logFreq) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'getReading', [reqTime, logFreq]);
};


//--------------------------------------------------------------------------------------------------------------



iBTVM.prototype.sendCommand = function(address,command,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'sendCommand', [address,command]);
};
iBTVM.prototype.getResponse = function(address,command,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'getResponse', [address,command]);
};
iBTVM.prototype.sendCmdGetResp = function(address,command,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'sendCmdGetResp', [address,command]);
};

iBTVM.prototype.sendConfig = function(address,range,style,acreject,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'sendConfig', [address,range,style,acreject]);
};

iBTVM.prototype.getStyle = function(address,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'getStyle', [address]);
};

iBTVM.prototype.getContinuous = function(address,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'getContinuous', [address]);
};

iBTVM.prototype.voltmeterInfo = function(argument,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'voltmeterInfo', [argument]);
};

iBTVM.prototype.configure = function(argument,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'configure', [argument]);
};

iBTVM.prototype.sendEmail = function(argument,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'sendEmail', [argument]);
};

iBTVM.prototype.getConfig = function(address,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'iBTVM', 'getConfig', [address]);
};


navigator.iBTVM = new iBTVM();

