var versionInfo = function() {
};

versionInfo.prototype.versionName = function(argument,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'VersionInfo', 'GetVersionName', [argument]);
};

versionInfo.prototype.versionCode = function(argument,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'VersionInfo', 'GetVersionCode', [argument]);
};

versionInfo.prototype.appName = function(argument,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'VersionInfo', 'GetAppName', [argument]);
};

versionInfo.prototype.valueString = function(argument,successCallback, failureCallback) {
    return cordova.exec(successCallback, failureCallback, 'VersionInfo', 'GetValueString', [argument]);
};


navigator.versionInfo = new versionInfo();

