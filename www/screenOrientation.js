var screenOrientation = function() {};
screenOrientation.prototype.set = function(str, success, fail) {
    // if (dolog) console.log('set screenOrientation = ' + str);
    if( device.platform == "Android") {
        cordova.exec(success, fail, "ScreenOrientation", "set", [str]);
    }   
};
screenOrientation.prototype.unlock = function(success, fail) {
    this.set('unspecified', success, fail);
};
navigator.screenOrientation = new screenOrientation();
