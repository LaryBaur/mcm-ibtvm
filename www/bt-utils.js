
function getDiscoveryHtml(devs) {
    var txt = '';
    for(var i=0;i<devs.length;i++) {
        txt += '<option value="' + devs[i].address + '">' + devs[i].name + '</option>';
    }                
    return txt;
}

function getDiscoveryHtmlFromString(str) {
    var devs = eval(str);
    return getDiscoveryHtml(devs);
}
