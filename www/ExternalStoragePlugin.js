var externalStoragePlugin = function () {
};

externalStoragePlugin.prototype.sd_card = function(argument,cbSuccess,cbError) {
    return cordova.exec(cbSuccess, cbError, 'ExternalStoragePlugin', 'sd_card', [argument]);
};

externalStoragePlugin.prototype.external_sd_card = function(argument,cbSuccess,cbError) {
    return cordova.exec(cbSuccess, cbError, 'ExternalStoragePlugin', 'external_sd_card', [argument]);
};

navigator.externalStoragePlugin = new externalStoragePlugin();
