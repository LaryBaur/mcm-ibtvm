var _fileSystem;    
var root = '';
var internal_root = '';
//MSM var useExternalSDCard = true;
var useExternalSDCard = false;

function resetFileSystem() {
    _fileSystem = undefined;
}

function updateError(error) {
    // sometimes message is defined and sometimes not - tired of looking up error codes!
    if (dolog) console.log('updateError: ' + JSON.stringify(error));
    
    if (error.message) return error;

    var res = error;
    res.message = 'Unknown file error: ' + res.code;
    
    if (res.code) switch (res.code) {
        case FileError.NOT_FOUND_ERR: 
            res.message = 'NOT_FOUND_ERR'; 
            break;
        case FileError.SECURITY_ERR:
            res.message = 'NOT_FOUND_ERR'; 
            break;
        case FileError.ABORT_ERR: 
            res.message = 'ABORT_ERR'; 
            break;
        case FileError.NOT_READABLE_ERR: 
            res.message = 'NOT_READABLE_ERR'; 
            break;
        case FileError.ENCODING_ERR: 
            res.message = 'ENCODING_ERR'; 
            break;
        case FileError.NO_MODIFICATION_ALLOWED_ERR: 
            res.message = 'NO_MODIFICATION_ALLOWED_ERR'; 
            break;
        case FileError.INVALID_STATE_ERR: 
            res.message = 'INVALID_STATE_ERR'; 
            break;
        case FileError.SYNTAX_ERR: 
            res.message = 'SYNTAX_ERR'; 
            break;
        case FileError.INVALID_MODIFICATION_ERR: 
            res.message = 'INVALID_MODIFICATION_ERR'; 
            break;
        case FileError.QUOTA_EXCEEDED_ERR: 
            res.message = 'QUOTA_EXCEEDED_ERR'; 
            break;
        case FileError.TYPE_MISMATCH_ERR: 
            res.message = 'TYPE_MISMATCH_ERR'; 
            break;
        case FileError.PATH_EXISTS_ERR: 
            res.message = 'PATH_EXISTS_ERR'; 
            break;    
    }
    return res;    
}

function getFileSystem(success, fail) {

    function fsFail(evt) {
        var error = updateError(evt.target.error);
        if (dolog) console.log('File System Error: ' + error.message);
        if (fail != undefined) {
            fail(error);
        }
    }               

    function gotFS(fileSystem) {
        if (_fileSystem !== fileSystem) {
            _fileSystem = fileSystem;
//MSM            internal_root = fileSystem.root.fullPath.substring('file://'.length); 
            internal_root = fileSystem.root.fullPath; 
            if (dolog) console.log('File System root = ' + internal_root);
            
            root = internal_root;
            
            if (dolog) console.log('File useExternalSDCard = ' + useExternalSDCard);
            if (dolog) console.log('File navigator.externalStoragePlugin = ' + typeof(navigator.externalStoragePlugin));
            
            if (useExternalSDCard && typeof(navigator.externalStoragePlugin) != 'undefined') {                
                navigator.externalStoragePlugin.external_sd_card(null, function(external_sd_card) {
                    if (external_sd_card) {
                        if (dolog) console.log('File System external root = ' + external_sd_card);
                        root = external_sd_card; 
                    }
                    if (success != undefined) success(fileSystem);
                });
            }
            else {
                if (success != undefined) success(fileSystem);
            }      
        } else {
            if (success != undefined) success(fileSystem);
        }
    }
    
    if (!_fileSystem) {       
        //if (dolog) console.log('getFileSystem()');               
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fsFail);
    } else {
        gotFS(_fileSystem);                
    }
    
}

function listLocalFiles(path, match, exact, success, fail) {
    //if (dolog) console.log('File: listLocalFiles');
    return listLocal(path, match, exact, false, success, fail);
}

function listLocalFolders(path, match, exact, success, fail) {
    //if (dolog) console.log('File: listLocalFolders');
    return listLocal(path, match, exact, true, success, fail);    
}

function listLocal(path, match, exact, folders, success, fail) {
    //if (dolog) console.log('File: listLocal: ' + path + ' - ' + match);

    function fsFail(error) {
        error = updateError(error);
        if (dolog) console.log('fsFail: ' + error.message);
        if (fail != undefined) {
            fail(error);
        }
    }               
           
    function gotReader(entries) {
        //if (dolog) console.log('File: gotReader: ' + entries.length + ' entries');
        var res = [];
        if (match) {
            match = match.toUpperCase();
        }
        for (var i=0; i<entries.length; i++) {
            var nm = entries[i].name; 
            //if (dolog) console.log('File: entry[' + i + '] = ' + nm);            
            if (match === undefined || 
                ((exact && nm.toUpperCase() === match) ||
                 (!exact && nm.toUpperCase().endsWith(match))))
            {
                if (folders && entries[i].isDirectory) {
                    //if (dolog) console.log('File: matching folder');                                                        
                    res.push(entries[i]);
                } else if (!folders && entries[i].isFile) {
                    //if (dolog) console.log('File: matching file');
                    res.push(entries[i]);
                }
            }
        }
        if (success != undefined) {
            //if (dolog) console.log('File: listLocal - success');
            success(res);
        }
    }

    function gotDir(dirEntry) {
        //if (dolog) console.log('File: gotDir');        
        // Get a directory reader
        var directoryReader = dirEntry.createReader();
        // Get a list of all the entries in the directory
        directoryReader.readEntries(gotReader,fsFail);        
    }
    
    function gotFS(fileSystem) {
        fileSystem.root.getDirectory(path, {create: true, exclusive: false}, gotDir, fsFail);
    }
    
    getFileSystem(gotFS, fsFail);
}


function saveLocalData(path, fname, data, success, fail, noReplace, noSync) {


    //if (dolog) console.log('saveLocalData('+path+','+fname+')');
    
    function fsFail(error) {
        
        
        error = updateError(error);
        console.log("Logging error: " + error);
        
        if (error.code != 12) if (dolog) console.log('fsFail: ' + error.message);
        if (fail != undefined) {
            fail(error);
        }
    }               
    
    function doneWrite(evt) {
        //if (dolog) console.log('doneWrite: ' + path + '/' + fname);                
        navigator.sendBroadcastPlugin.mediaScanFile(path + '/' + fname);

        if (!noSync) dropBoxAddFile(path, fname, function() {
            // do something here?
        });
        

        if (success != undefined) {
            success(evt.target.result);
        }
    }
    
    function gotFileWriter(writer) {
        //if (dolog) console.log('gotFileWriter');
        
        writer.onwrite = doneWrite;
        writer.onerror = fsFail;
        
        writer.write(data);
    }
    
    function gotFile(fileEntry) {
        //if (dolog) console.log('gotFile');
        
        
        fileEntry.createWriter(gotFileWriter, fsFail);        
    }

    function gotDir(dirEntry) {
        //if (dolog) console.log('gotDir');
        navigator.sendBroadcastPlugin.mediaScanFile(path);

        if (!noSync) dropBoxEnsureFolderExists(path, function() {
            // do something here?
        });
        
        
        if (noReplace) {
            dirEntry.getFile(fname, {create: true, exclusive: true}, gotFile, fsFail);
        } else {
            dirEntry.getFile(fname, {create: true, exclusive: false}, gotFile, fsFail);
        }
    }

    function gotFS(fileSystem) {
        fileSystem.root.getDirectory(path, {create: true, exclusive: false}, gotDir, fsFail);
    }
        
    getFileSystem(gotFS, fsFail);
}

function loadLocalData(path, fname, success, fail) {
    //if (dolog) console.log('loadLocalData: ' + path + '/' + fname);               

    function fsFail(error) {
        error = updateError(error);
        if (dolog) console.log('fsFail: ' + error.message);
        if (fail != undefined) {
            fail(error);
        }
    }               
    
    function doneRead(evt) {
        //if (dolog) console.log('doneRead');
        if (success != undefined) {
            success(evt.target.result);
        }
    }
        
    function readAsText(file) {
        //if (dolog) console.log('readAsText');
        var reader = new FileReader();
        reader.onload = doneRead;
        reader.onerror = fsFail;
        reader.readAsText(file);
    }    
    
    function gotFile(fileEntry) {
        //if (dolog) console.log('gotFile');
        //readAsText(fileEntry)
        fileEntry.file(readAsText, fsFail)
    }

    function gotDir(dirEntry) {
        //if (dolog) console.log('gotDir');
        dirEntry.getFile(fname, {create: false, exclusive: true}, gotFile, fsFail);
    }
    
    function gotFS(fileSystem) {
        fileSystem.root.getDirectory(path, {create: true, exclusive: false}, gotDir, fsFail);
    }
    
    getFileSystem(gotFS, fsFail);
}

function deleteLocalPath(path, success, fail) {

    function fsFail(error) {
        error = updateError(error);
        if (dolog) console.log('fsFail: ' + error.message);
        if (fail != undefined) {
            fail(error);
        }
    }               

    function doneDelete(parent) {
        //if (dolog) console.log('doneDelete: ' + path);                
        navigator.sendBroadcastPlugin.mediaScanFile(path);  
        
        dropBoxDeleteFolderOrFile(path, function() {
            // do something here?
        });        
        
        if (success != undefined) {
            success(parent);
        }        
    }
        
    function gotDir(dirEntry) {
        //if (dolog) console.log('gotDir');
        dirEntry.removeRecursively(doneDelete, fsFail);
    }
    
    function gotFS(fileSystem) {
        fileSystem.root.getDirectory(path, {create: true, exclusive: false}, gotDir, fsFail);
    }
    
    getFileSystem(gotFS, fsFail);
}

function deleteLocalData(path, fname, success, fail) {

    //if (dolog) console.log('deleteLocalData');
    
    function fsFail(error) {
        error = updateError(error);
        if (dolog) console.log('fsFail: ' + error.message);
        if (fail != undefined) {
            fail(error);
        }
    }               

    function doneDelete(entry) {
        //if (dolog) console.log('doneDelete: ' + path + '/' + fname);                
        navigator.sendBroadcastPlugin.mediaScanFile(path + '/' + fname);
        
        dropBoxDeleteFolderOrFile(path + '/' + fname, function() {
            // do something here?
        });        
        
        
        if (success != undefined) {
            success(entry);
        }        
    }    
    
    function gotFile(fileEntry) {
        //if (dolog) console.log('gotFile');
        fileEntry.remove(doneDelete, fsFail);
    }

    function gotDir(dirEntry) {
        //if (dolog) console.log('gotDir');
        dirEntry.getFile(fname, {create: false, exclusive: true}, gotFile, fsFail);
    }

    function gotFS(fileSystem) {
        fileSystem.root.getDirectory(path, {create: true, exclusive: false}, gotDir, fsFail);
    }

    getFileSystem(gotFS, fsFail);
}

function copyLocalPath(path, newPath, success, fail) {

    //if (dolog) console.log('copyLocalPath');           
    var newParent = newPath.substring(0, newPath.lastIndexOf('/'));
    var newName = newPath.substring(newPath.lastIndexOf('/')+1);    
    var oldDir;
    //if (dolog) console.log('path=' + path);        
    //if (dolog) console.log('newParent=' + newParent);        
    //if (dolog) console.log('newName=' + newName);             
    
    function fsFail(error) {
        error = updateError(error);
        if (dolog) console.log('fsFail: ' + error.message);
        if (fail != undefined) {
            fail(error);
        }
    }               

    function doneCopy(entry) {
        //if (dolog) console.log('doneCopy: ' + newPath);                
        navigator.sendBroadcastPlugin.mediaScanFile(newPath);
        
        if (entry.isFile) {        
            dropBoxAddFile(newParent, newName, function() {
                // do something here?
            });
        } else {
            dropBoxEnsureFolderExists(newPath, function() {                                                
                // recurse into contents in success callback
            });                    
        }
        
        if (success != undefined) {
            success(entry);
        }        
    }    
    
    function gotDstDir(dirEntry) {
        //if (dolog) console.log('gotDstDir');        
        navigator.sendBroadcastPlugin.mediaScanFile(newParent);

        dropBoxEnsureFolderExists(newParent, function() {
            // do something here?
        });        
        
        oldDir.copyTo(dirEntry, newName, doneCopy, fsFail)        
    }
    
    function gotSrcDir(dirEntry) {
        //if (dolog) console.log('gotSrcDir');
        oldDir = dirEntry;
        _fileSystem.root.getDirectory(newParent, {create: true, exclusive: false}, gotDstDir, fsFail);
    }
    
    function gotFS(fileSystem) {        
        _fileSystem.root.getDirectory(path, {create: true, exclusive: false}, gotSrcDir, fsFail);
    }    
    
    getFileSystem(gotFS, fsFail);
}


