var dataPath = '';
var app_name = 'MCM iBTVM';
var versionNumber = '';
var dataFolder = '';
var settings = {};

var voltageInterval;
var voltageTimeout;
var restartTimeout;
var logFreq, tmrFreq;
var timerDt;
var gpsDate = false;

var logData = [];
var logging = false;
var dtHtml = undefined;
var dtStatus = undefined;
var gotStatus = false;
var gpsSuffix = '';

//profiling data
var doProfiling = false;
var maxGRmillis = 0;
var minGRmillis = 10000;
var profileReset = true;
var profSamples = 0;
var profTotal = 0;


function loadVersionStrings(success, fail) {
    if (dolog) console.log('loadVersionStrings');
    var promises = []; var px = 0;

    var def1 = $.Deferred();
    navigator.versionInfo.versionName(null, 
        function(r) {
            if (dolog) console.log('versionNumber = ' + r);
            if (r) versionNumber = r;
            def1.resolve();
        }, function() {
            def1.reject();
        }
    );
    promises[px++] = def1.promise();

    var def2 = $.Deferred();
    navigator.versionInfo.appName(null, 
        function(r) {
            if (dolog) console.log('app_name = ' + r);
            if (r) app_name = r;
            def2.resolve();
        }, function() {
            def2.reject();
        }
    );
    promises[px++] = def2.promise();

    var def3 = $.Deferred();
    navigator.versionInfo.valueString("dataFolder", 
        function(r) {
            if (dolog) console.log('dataFolder = ' + r);
            if (r) dataFolder = r;
            def3.resolve();
        }, function() {
            def3.reject();
        }
    );
    promises[px++] = def3.promise();

    $.when.apply($, promises).done(function() {
        if (success) success();
    }).fail(function(error) {
        if (fail) fail(error);
    });
}

function tabPageChange(ActivePage) {
    
    console.log("Logging touching the page tab: " + ActivePage);
    
    if ((chartData.length > 1 || chartPending) && ActivePage === 'ChartPage') {
        updateChart(chartPending, tmrFreq, chartData, logData);
    }
}

var demoModeText = '*Demo Mode*'; 
function demoMode() {
    return (iBTVMaddress === demoModeText) || usingBrowser();    
}

function devSelect(auto) {
    if (usingBrowser()) {
        startTimerAndMeter();
        return;
    }

    if (auto && iBTVMdevs.length === 1) {
        iBTVMname = iBTVMdevs[0].name
        iBTVMaddress = iBTVMdevs[0].address;
        setLocalItem("iBTVMname", iBTVMname);
        setLocalItem("iBTVMaddress", iBTVMaddress);
        $('#iBTVMdevice .ui-btn-text').text(iBTVMname);
        if (!voltageInterval) {
            startTimerAndMeter();
        } else {
            iBTVMrestart();
        }
        return;
    }

    var devs = [];
    for (i = 0; i < iBTVMdevs.length; i++) {
        devs.push(iBTVMdevs[i].name);
    }
    devs.push(demoModeText);
    devs.push('Discover New Devices ...');

    function doStart(lpos) {
        if (lpos > iBTVMdevs.length-1) {
            iBTVMname = demoModeText;
            iBTVMaddress = demoModeText;            
        } else {
            iBTVMname = iBTVMdevs[lpos].name;
            iBTVMaddress = iBTVMdevs[lpos].address;
        }
        setLocalItem("iBTVMname", iBTVMname);
        setLocalItem("iBTVMaddress", iBTVMaddress);
        $('#iBTVMdevice .ui-btn-text').text(iBTVMname);        
        startTimerAndMeter();
    }

    navigator.listSelectPlugin.listSelect('Select iBTVM Device', devs, function(lpos) {
        if (lpos === devs.length - 1) {
            if (voltageInterval) {
                stopTimerAndMeter(true, function() {
                    activityStop(500);
                    window.setTimeout(function() {
                        iBTVMname = ''; iBTVMaddress = '';
                        $("#iBTVMdevice").buttonMarkup({theme : 'b'});
                        $('#iBTVMdevice .ui-btn-text').text('...');
                        iBTVMdiscover(undefined, function() {
                            devSelect();
                        }), 1000
                    });
                })
            } else {
                iBTVMdiscover(undefined, function() {
                    devSelect();
                });
            }
        } else if (lpos !== -1) {
            if (voltageInterval) {
                stopTimerAndMeter(true, function() {
                    activityStop(500);
                    window.setTimeout(function() {
                        doStart(lpos);
                    }, 1000);
                });
            } else {
                doStart(lpos);
            }
        }
    });
}


function initAfterFileSystem() {
    if (dolog) console.log('initAfterFileSystem');

    $('#iBTVMtitle').html(app_name);
    $('#iBTVMfooter').html(app_name + " v" + versionNumber + ' - Copyright (c) 2014 - M. C. Miller Co., Inc.');

    if (!usingBrowser()) {                
        navigator.dropbox_sync.init(function() {
            console.log('Logging BREAKPOINT iBTVMdropbox islinked: ' + navigator.dropbox_sync.isLinked());
//            $('#iBTVMdropbox').prop('checked', navigator.dropbox_sync.isLinked()).checkboxradio("refresh");
                                    
                                    navigator.dropbox_sync.isLinked(function(linked){
                                                                    
                                                                    $('#iBTVMdropbox').prop('checked', linked).checkboxradio("refresh");
                                                                    
                                                                    });
                                    
        }); // basically just checks to see if there's an account linked
    }
    iBTVMInitialize();

    hookupControls();
    
    window.setTimeout(function() {
        cordova.exec(null, null, "SplashScreen", "hide", []);
        navigator.screenOrientation.unlock();
    }, 2000); // wait a further 1 sec then show intro screen

    if (iBTVMaddress) {
        window.setTimeout(function() {
            startTimerAndMeter();
        }, 1000); // give user interface a chance to draw before starting
    } else if (!iBTVMdiscovery) {
        navigator.notification.confirm(
            'Make sure your iBTVM is switched on and Bluetooth is enabled.',
            function(buttonIndex) {
                if (buttonIndex == 1) {
                    iBTVMdiscover(undefined, function() {
                        devSelect(true);
                    });
                }
            }, 
            'Discover and Connect to your iBTVM device?',
            'Yes,No');
    }
}

function initAfterReady() {
    if (dolog) console.log('initAfterReady');

    if (usingBrowser() || (device.platform !== 'Android')) {
        // pretend we're iOS
        $('.ui-title').css("margin-top", "16px");
        $('#iBTVMexit').css("margin-top", "14px");
    }

    
    
    tabview_initialize('TabView', tabPageChange);

    if (usingBrowser()) {
        initAfterFileSystem();
        return;
    }

    loadVersionStrings(function() {
        useExternalSDCard = false;
        getFileSystem(function(fs) { // success get file system
//MSM            dataPath = root + '/' + dataFolder; // root defined in file.js
            dataPath = root;
            console.log('Logging BREAKPOINT dataPath: ' + dataPath);
                      
          navigator.dropbox_sync.isLinked(function(linked){
                                          
                                          console.log('Logging BREAKPOINT isLinked: ' + linked);
                                          $('#iBTVMdropbox').prop('checked', linked).checkboxradio("refresh");
                                          
                                          });
                      
                      
            getLocalSettings(function() {
                s = settings["useExternalSDCard"];
 //MSM               useExternalSDCard = (s !== false);
                useExternalSDCard = false;
                if (dolog) console.log('File useExternalSDCard = ' + useExternalSDCard);

                if (useExternalSDCard) {
                    resetFileSystem(); // so that useExternalSDCard can be used
                    getFileSystem(function(fs) {
                        dataPath = root + '/' + dataFolder; // update to external
                        if (dolog) console.log('dataPath: ' + dataPath);
                        initAfterFileSystem();
                    });
                } else {
                    initAfterFileSystem();
                }
            });
                      
        });
    }, function(error) {
        if (dolog) console.log("loadVersionStrings failed: " + error);
    });
}

function pad(num, size) {
    if (!size) size = 2
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}

function emailData(fname, subject, body) {
    var toRecipients = [];
    
    console.log("Logging BREAKPOINT code reached emailData");
    
    if (iBTVMrecipients) {
        var str = iBTVMrecipients;
        str = replaceAll(';', ',', str);
        str = replaceAll(' ', ',', str);
        str = replaceAll(',,', ',', str);
        toRecipients = str.split(',');
    }
    
    var ccRecipients = "";
    var bccRecipients = "";
    var bIsHTML = "";
    console.log("Logging Sending Email: " + subject + " " + body + " " + toRecipients + " " + ccRecipients + " " + bccRecipients + " " + bIsHTML);
    window.plugins.emailComposer.showEmailComposer(subject, body, toRecipients, ccRecipients, bccRecipients, false, [ dataPath + '/' + fname ]);

}

function emailLogData(fname) {
    console.log("Logging BREAKPOINT code reached emailLogData");

    return emailData(fname, 'MCM iBTVM Data', 'MCM iBTVM Data');
}

function emailDiagnostics(fname) {
    console.log("Logging BREAKPOINT code reached emailDiagnostics");
    return emailData(fname, 'MCM iBTVM Diagnostics', 'MCM iBTVM Diagnostics');
}

function saveDiagnostic(diag, email, ext) {

    console.log("Logging BREAKPOINT saveDiagnostic");
    
    var fname = 'iBTVM';
    var dt = new Date();
    fname += '-';
    fname += americanDate(dt, '-') + '-' + pad(dt.getHours()) + '-' + pad(dt.getMinutes()) + '-' + pad(dt.getSeconds());

    if (!ext) ext = 'txt';
    fname += '.' + ext;

    activityStart("Saving ...");

    saveLocalData(dataPath, fname, diag, function(res) {
        
        console.log("Logging BREAKPOINT reached saveDiagnostic -> saveLocalData -> callback");
                  
        activityStop(500);
        // Asa Andrew this line may have been what was messing everything up
        // its suppose to show a dialog box that says 'saved'
        // Toast.longshow('Saved');
        if (email) {
            emailDiagnostics(fname);
        }
    }, function(error) {
        activityStop(500);
        if (dolog) console.log('saveDiagnostic: fail');
        alert('Failed to save: ' + error.message);
    }, false, false); // allow dropbox
}

function saveDataFile(data, fname, email, callback) {

    console.log("Logging BREAKPOINT code reached saveDataFile");
    
    function convertToCSV(data) {
        var str = '';

        for ( var i = 0; i < data.length; i++) {
            if (data[i][0] == null) {
                str += 'null' + '\r\n'; // for now
                continue; // skip null points
            }

            var line = '';

            var dt = data[i][0];
            var v1 = data[i][1];
            var v2 = data[i][2];

            var fmt = americanDate(dt) + ' ' + pad(dt.getHours()) + ':' + pad(dt.getMinutes()) + ':' + pad(dt.getSeconds()) + '.' + pad(dt.getMilliseconds(), 3);

            line = fmt + ',' + v1;
            if (i == 0 && gpsSuffix !== '') {
                line += ',';
                if ($.isNumeric(v2)) line += v2;
                line += ',' + gpsSuffix;
            } else {
                if ($.isNumeric(v2)) line += ',' + v2;
            }

            str += line + '\r\n';
        }
        return str;
    }
    if (data.length == 0) {
        alert('No data logged to save!');
        return;
    }

    activityStart("Saving ...");

    var csv = convertToCSV(data);

    saveLocalData(dataPath, fname, csv, function(res) {
        console.log("Logging BREAKPOINT code reached saveDataFile -> saveLocalData");

        activityStop(500);
        // Toast.longshow('Saved');
        if (email) {
            emailLogData(fname);
        }
        if (callback) callback();

    }, function(error) {
        activityStop(500);
        alert('Failed to saveDataFile: ' + error.message);
        if (callback) callback();
    }, false, false); // allow dropbox
}

function stopTimerAndMeter(forceStop, callback) {
    if (voltageInterval) {
        clearInterval(voltageInterval);
    }
    if (voltageInterval || forceStop) {
        iBTVMstop(callback);
    } else {
        if (callback !== undefined) callback();
    }
    voltageInterval = undefined;
    setLogging(false);
    clearDisplay();
}

function setLogging(log, tag) {
    if (log === logging) return;
    if (tag != undefined) if (dolog) console.log('logging = ' + log + ' ' + tag);

    if (log) {
        if (!voltageInterval) {
            startTimerAndMeter(false, function() {
                setLogging(log, tag);
            });
            return;
        }
        $('.volts').css("color", "#003399");
        $('.volts-unit').css("color", "#003399");
        $("#iBTVMlogStart .ui-btn-text").text("Stop Recording");
        $("#iBTVMlogStart").buttonMarkup({theme : 'e'});
    } else {
        if (dolog) console.log('logging = ' + log + ' ' + tag);
        $('.volts').css("color", "black");
        $('.volts-unit').css("color", "black");
        $("#iBTVMlogStart .ui-btn-text").text("Start Recording");
        $("#iBTVMlogStart").buttonMarkup({theme : 'b'});
    }
    logging = log;
}

function chartDataPush(dt, vOn, vOff) {
    chartData.push([ dt, vOn, vOff ]);
    if (chartData.length > 300) {
        chartData.splice(0, chartData.length - 300);
    }
    if (chartData.length > 1 && ActivePage === 'ChartPage') {
        if (logging && logFreq < 500) {
            updateChart(false, tmrFreq, chartData, logData, 2000);
        } else {
            updateChart(false, tmrFreq, chartData, logData, 750);
        }
    }
}

function logDataPush(dt, vOn, vOff) {
    logData.push([ dt, vOn, vOff ]);
}

function logDataClear() {
    if (logData.length != 0) {
        logData.length = 0;
    }
}

function checkAndUpdateStatus() {
    // request status every 5 minutes, and check for prior response, but check
    // every time period until first response returns

    // A'sa Andrew this method is called which eventually sends
    // a message to the iBTVM
    
//    console.log("Logging get status called from javascript.")
    
    var intvl = 60 * 5;
    var batMin = 3.0;
    var batMax = 4.19;

    var dt = new Date();
    var updateStatus = (!gotStatus || (dt.getTime() - dtStatus.getTime() > intvl * 1000));

    if (updateStatus) {
        iBTVMgetStatus((gotStatus || !dtStatus), function(r) {
            // callback after we get the status

            gotStatus = true;
            
            var v = parseFloat(r.battery);
            if (isNaN(v)) {
                $('#iBTVMbattery').html(r.battery);
            } else {
                var p = (v - batMin) / (batMax - batMin) * 100;
                if (p < 0) p = 0;
                else if (p > 100) p = 100;
                $('#iBTVMbatteryPct').html("");
                $("#iBTVMbatteryPct").css("top", (100 - p).toFixed() + '%');
                $("#iBTVMbatteryPct").css("height", p.toFixed() + '%');
                if (p < 40) {
                    $("#iBTVMbatteryPct").css("background", "red");
                    $("#iBTVMbatteryPct").css("color", "black");
                } else {
                    $("#iBTVMbatteryPct").css("background", "green");
                    $("#iBTVMbatteryPct").css("color", "white");
                }
                
                $("#iBTVMbatteryPercentLabel").html(p.toFixed() + '%');
                
            }
            // if (dolog) console.log('Status: ' + JSON.stringify(r));
                $("#iBTVMmeterIdLabel").html(r.name);
                $("#iBTVMbattery").css("top", 59);
        });
        dtStatus = new Date();
    }
}

function clearDisplay() {
    $('#iBTVMvoltsOn').html('...');
    $('#iBTVMvoltsOff').html('');
    $('#iBTVMvoltsOnUnits').html('');
    $('#iBTVMvoltsOffUnits').html('');
    $('#iBTVMbatteryPercentLabel').html('')
}

var lastTime = 0;
function readingTimer() {
    
    if (!logging || logFreq >= 500) {
        // suspend status update when logging at high speed
        checkAndUpdateStatus();
    }

    var reqTime = 0;
    if (logging && lastTime !== 0 && iBTVMstyleVal === 'single') {
        reqTime = lastTime + logFreq;
    }

    function updateDisplay(r) {
        // Update voltage display
        var res = getVoltageText(r);
        
//        console.log("Logging updated display with text: " + res.vOn);
        
        // A'sa Andrew -> function that updates the volts label
        
//        console.log("Logging the voltage off reading: " + res.uOff)
        
        $('#iBTVMvoltsOn').html(res.vOn);
        $('#iBTVMvoltsOff').html(res.vOff);
        $('#iBTVMvoltsOnUnits').html(res.uOn);
        $('#iBTVMvoltsOffUnits').html(res.uOff);

        if (r.noComplex) {
            $('#iBTVMvoltsOn').addClass("no-complex");
            $('#iBTVMvoltsOff').addClass("no-complex");
        } else {
            $('#iBTVMvoltsOn').removeClass("no-complex");
            $('#iBTVMvoltsOff').removeClass("no-complex");
        }

        // Update GPS display
        var html = formatGPS(r);
        $('#iBTVMgps').html(html);
        dtHtml = new Date();

        // Capture GPS for inclusion in logged data
        html = formatGPS(r, true);
        if (html !== '') gpsSuffix = html;
    }

    function correctPriorData(r, dt) {
        if (dolog) console.log('Discarding prior logged and charted data');
        gpsDate = true;
       
        logData.length = 0;
        chartData.length = 0;
        
        // r.gpsDiff is difference between GPS second pulse (which has no MS componenent) and local platform time, and also includes any reading latency
        // using it to correct prior data as below is wrong. What we'd need is difference between *combined* gpsDate and relative ms, and platform time
        // but this would still contain latency error so safer to just dump prior data
/*        
        // Correct previously logged or charted data (before we had GPS time)
        if (dolog) console.log('Correcting logged and charted data by: ' + r.gpsDiff + 'ms');
        var lDt;
        var i;
        for (i = 0; i < logData.length; i++) {
            lDt = new Date(logData[i][0] - r.gpsDiff);
            logData[i][0] = lDt;
        }
        for (i = 0; i < chartData.length; i++) {
            lDt = new Date(chartData[i][0] - r.gpsDiff);
            chartData[i][0] = lDt;
        }
*/        
    }

    var b4millis = new Date().getTime();

    iBTVMgetReading(
        function(r) {
            // if (dolog) console.log(JSON.stringify(r));

            if (restartTimeout) {
                clearTimeout(restartTimeout); restartTimeout = undefined;
            }

            var dt;
            var updateHtml = true;

                    // is this code suppose to say if the temporaryFrequency is less than 200 make it 200 and if it's
                    // greater than 500 make it 500 -> Andrew A'sa
                    
//                    console.log("Logging the temporaryFrequency: " + tmrFreq + "; and logFrequency: " + logFreq);
                    
//            if (tmrFreq < 200) {
//                dt = new Date();
//                // maximum 1 update per 500ms
//                if ((dtHtml && dt.getTime() - dtHtml.getTime() < 200)) {
//                    updateHtml = false;
//                }
//            }
            
            dt = new Date();
                    
            var elapsedTime = (dtHtml ? ( dt.getTime() - dtHtml.getTime() ) : 0);
//            console.log("dtHtml.getTime(): " + (dtHtml ? dtHtml.getTime() : 0) + "; dt.getTime(): " + dt.getTime() + "; dt-dtHtml: " + elapsedTime );

//            var timeIsLessThanLoggedTime = ( elapsedTime > logFreq );
//            updateHtml = (elapsedTime == 0 ? true : timeIsLessThanLoggedTime);
//                    
//            console.log("Will update: " + updateHtml);
                    
            if (updateHtml) {
                    
                // A'sa Andrew -> this is the only place this is called to update the volts label
                updateDisplay(r);
            }

            var logIntervalElapsed = logFreq <= tmrFreq;
            if (!logIntervalElapsed) {
                dt = new Date();
                logIntervalElapsed = !timerDt || (dt.getTime() >= timerDt.getTime());
                if (logIntervalElapsed) {
                    if (timerDt) { // experimental
                        timerDt.setTime(timerDt.getTime() + logFreq);
                    } else {
                        timerDt = new Date(dt.getTime() + logFreq);
                    }
                }
            }

            if (r.onTime) {
                dt = new Date(r.onTime);

                // Correct previously logged or charted data
                if (r.gpsDate != undefined && !gpsDate) {
                    correctPriorData(r, dt);
                }
            } else {
                // should never happen
                dt = new Date();
            }

            if (r.onTime && logIntervalElapsed) {
                var vOn = formatVoltage(r.voltsOn, undefined, true);
                var vOff = formatVoltage(r.voltsOff, undefined, true);

//vOff = r.gpsDiff;                
                
                lastTime = r.onTime;
                if (logging) {
                    logDataPush(dt, vOn, vOff, r.gpsDiff);
                }
                if ($.isNumeric(vOn) || $.isNumeric(vOff)) {
                    chartDataPush(dt, vOn, vOff);
                }

                // catch-up if timer is too slow
                if (reqTime !== 0 && r.reads !== undefined && r.reads.length > 0) {
                    if (dolog) console.log('r.reads.length = ' + r.reads.length);
                    if (dolog) console.log('lastTime + logFreq = ' + lastTime + logFreq);
                    var i;
                    // look thru readings (oldest first)
                    for (i = 0; i < r.reads.length; i++) {
                        // array should now be restricted to logFreq increments but doesn't hurt to check
                        //if (r.reads[i].t >= lastTime + logFreq) {
                            // save data
                            if (dolog) console.log('r.reads[i].t = ' + r.reads[i].t);
                            vOn = formatVoltage(r.reads[i].v, undefined, true);
//vOn = -vOn; // used to pinpoint catch-up data
                            vOff = undefined;
                            lastTime = r.reads[i].t;
                            dt = new Date(lastTime);
                            logDataPush(dt, vOn, vOff);
                            chartDataPush(dt, vOn, vOff);
                        //}
                    }
                }

            } else {
                // if (dolog) console.log('!logIntervalElapsed');
            }
            
//            console.warn('profiling: doProfiling='+doProfiling+', dolog='+dolog);
            
            if (doProfiling) {
            	var grms = new Date().getTime() - b4millis;
            	if (profileReset) {
            		maxGRmillis = grms;
            		minGRmillis = grms;
            		profSamples = 1;
            		profTotal = grms;
            		profileReset = false;
            	} else {
            		if (grms > maxGRmillis) maxGRmillis = grms;
            		if (grms < minGRmillis) minGRmillis = grms;
            		profTotal += grms;
            		profSamples++;
            		if ((profSamples % 20) === 0) {
            			if (dolog) {
            				console.warn('getReading profile: after '+profSamples+' samples min='+minGRmillis+', max='+maxGRmillis+', avg='+(profTotal/profSamples));
            			}
            		}
            	}
            } else {
                //if (dolog) console.warn('Profiling off');
            }            
        },
        function(e) {
            // if (dolog) console.warn('no reading');
            // failed
            var dt;
            var updateHtml = true;
                    
            //lastTime = 0; // reset last time value received from meter

            if (tmrFreq < 200) {
                dt = new Date();
                // maximum 1 update per 500ms
                if ((dtHtml && dt.getTime() - dtHtml.getTime() < 200)) {
                    updateHtml = false;
                }
            }
            if (updateHtml) {
                if (dolog) console.log(e);
                dtHtml = new Date();

                if (e.indexOf('cannot compute complex reading') != -1) {
                    // do nothing
                } else if (e.indexOf('call for complex read while not started') != -1) {
                    // do nothing
                } else if (e.indexOf('call to get reading while not started') != -1) {
                    // do nothing
                } else if (e.indexOf('no reading yet') != -1) {
                    if (dolog) console.log(e);
                    if (!restartTimeout) {
                        restartTimeout = setTimeout(function() {
                            if (dolog) console.log('Restarting ... ');
                            iBTVMstart(undefined, undefined, undefined, true);
                        }, 1000);
                    }
                } else {
                    // Update voltage display
                    $('#iBTVMvoltsOn').html(e);
                    dtHtml = new Date();
                    if (dolog) console.log(e);
                }
            }
        }, reqTime, logFreq // playing with this still
    );
}

function restartTimerAndMeter() {
    iBTVMrestart();
    restartTimer();
    profileReset = true; //reset profiling
}

function restartMeter() {
    if (dolog) console.log('restartMeter');
    iBTVMrestart();
}

function initTimer() {
    logFreq = getFrequency();
    tmrFreq = logFreq;
    if (tmrFreq > 1000) {
        // so display & chart gets updated regualrly        
//        if (iBTVMstyleVal === 'single') {
//            tmrFreq = 200;
//        } else {
//            tmrFreq = 500;
//        }
    }
    else if (tmrFreq < 100) {
        // if meter is running at 50ms we dont want to poll every 50ms as we may be too early for reading 
        tmrFreq = 100;
    }
    timerDt = undefined;
    lastTime = 0; // reset last time value received from meter
    voltageInterval = setInterval(function() {
        readingTimer();
    }, tmrFreq);    
}

function restartTimer() {
    if (dolog) console.log('restartTimer');

    if (voltageInterval) {
        clearInterval(voltageInterval); voltageInterval = undefined;
    }
    if (voltageTimeout) {
        clearTimeout(voltageTimeout); voltageTimeout = undefined;
    }

    initTimer();
}

function startTimerAndMeter(spoof, success) {
    if (dolog) console.log('startTimerAndMeter');
    
    spoof = spoof || demoMode();    

    if (voltageInterval) {
        clearInterval(voltageInterval); voltageInterval = undefined;
    }
    if (voltageTimeout) {
        clearTimeout(voltageTimeout); voltageTimeout = undefined;
    }

    $("#iBTVMdevice").buttonMarkup({theme : 'c'});
    iBTVMstart(spoof, function() {
        initTimer();
        if (success !== undefined) success();
    }, function(err) {
        if (err === "Connect Error") {
            $("#iBTVMdevice").buttonMarkup({theme : 'b'});
            $('#iBTVMdevice .ui-btn-text').text('...');
        }
    });
}

function handleDate(elm, options) {
    event.stopPropagation();
    var currentField = $(elm);
    var opts = options || {};
    var minVal = opts.min || 0;
    var maxVal = opts.max || 0;

    var myNewDate = Date.parse(currentField.val()) || new Date();
    if (typeof myNewDate === "number") {
        myNewDate = new Date(myNewDate);
    }

    window.plugins.datePicker.show({
        date : myNewDate,
        mode : 'date',
        minDate : Date.parse(minVal),
        maxDate : Date.parse(maxVal)
    }, function(returnDate) {
        if (returnDate !== "") {
            var dt = new Date(returnDate);
            var txt = americanDate(dt);
            currentField.val(txt);

            if (dolog) console.log('elm.id: ' + elm.id);
            if (elm.id === 'iBTVMstartDate') {
                iBTVMstartDate = txt;
                setLocalItem(elm.id, txt);
            } else if (elm.id === 'iBTVMstopDate') {
                iBTVMstopDate = txt;
                setLocalItem(elm.id, txt);
            }
            loggingChange();
        }
    });
}
function handleTime(elm, reset) {
    var currentField = $(elm);
    var time = currentField.val();
    var myNewTime = new Date();

    if (time && !reset) {
        myNewTime.setHours(time.substr(0, 2));
        myNewTime.setMinutes(time.substr(3, 2));
    }
    plugins.datePicker.show({
        date : myNewTime,
        mode : 'time'
    }, function(returnDate) {
        if (returnDate !== "") {
            var newDate = new Date(returnDate);
            var hrs = newDate.getHours();
            var min = newDate.getMinutes();
            var txt = pad(hrs) + ':' + pad(min);
            currentField.val(txt);

            if (dolog) console.log('elm.id: ' + elm.id);
            if (elm.id === 'iBTVMstartTime') {
                iBTVMstartTime = txt;
                setLocalItem(elm.id, txt);
            } else if (elm.id === 'iBTVMstopTime') {
                iBTVMstopTime = txt;
                setLocalItem(elm.id, txt);
            }
            loggingChange();
        }
    });
}

function promptSaveData(callback) {
    setLogging(false);

    if (usingBrowser()) {
        logDataClear();
        if (callback) callback(true);
        return;
    }

    navigator.notification.prompt('Enter Filename:', function(r) {
                                  
        if (r.buttonIndex == 1) {
            // Yes
            var fname = $.trim(r.input1);
            if (validateFileName(fname)) {
                // setLogging(false);
                fname += '.csv';
                saveDataFile(logData, fname, iBTVMemail, function() {
                    $('#iBTVMfilename').html(fname);
                    logDataClear();
                    if (callback) callback(true);
                });
            } else {
                promptSaveData(callback);
            }
                                  
        } else {
            // No
            logDataClear();
            if (callback) callback(false);
        }
    }, 'Save Logged Data?', [ 'Save', 'Cancel' ], ' ');

}

function loggingChange() {
    if (iBTVMscheduled && !voltageInterval && iBTVMstartTime && iBTVMstopTime) {
        var startDate = dateAmerican(iBTVMstartDate);
        var stopDate = dateAmerican(iBTVMstopDate);
        var dtStart = new Date(Date.parse(startDate.toDateString() + ' ' + iBTVMstartTime));
        var dtStop = new Date(Date.parse(stopDate.toDateString() + ' ' + iBTVMstopTime));
        var dt = new Date();

        if (voltageTimeout) {
            clearTimeout(voltageTimeout); voltageTimeout = undefined;
        }

        if (dolog) console.log('** ' + dtStart + ' ' + dtStop + ' ' + dt);

        if (dtStart.getTime() >= dtStop.getTime()) {
            return;
        }

        var ms = dtStart.getTime() - dt.getTime();
        if (ms < 0) {
            // do nothing
        } else if (ms < 60 * 1000) {
            // start immediatately if less than 1 minute
            startTimerAndMeter();
        } else {
            // start 1 minute before due
            voltageTimeout = setTimeout(function() {
                startTimerAndMeter();
            }, ms - 60 * 1000);

        }
    } else {
        if (voltageTimeout) {
            console.log("Logging: Voltage Timeout")
            clearTimeout(voltageTimeout); voltageTimeout = undefined;
        }
    }
}

function hookupControls() {
    if (dolog) console.log("hookupControls");
/*
     if (iBTVMdiscovery) { 
         if (dolog) console.log('iBTVMdiscovery ' + iBTVMdiscovery);
         if (dolog) console.log('iBTVMname ' + iBTVMname);
         var txt = getDiscoveryHtmlFromString(iBTVMdiscovery);
         $('#iBTVMdevice').html(txt); 
         if (iBTVMaddress) {
             $('#iBTVMdevice').val(iBTVMaddress);
         } 
     }
*/
    if (iBTVMaddress && iBTVMname) {
        $('#iBTVMdevice .ui-btn-text').text(iBTVMname);
    }

    if (dolog) console.log('iBTVMrangeName ' + iBTVMrangeName);
    var txt = getSelectHtml(iBTVMrangeValues);
    $('#iBTVMrange').html(txt);
    if (iBTVMrangeVal) {
        $('#iBTVMrange').val(iBTVMrangeVal);
    }
    if (dolog) console.log('iBTVMacrejectName ' + iBTVMacrejectName);
    var txt = getSelectHtml(iBTVMacrejectValues);
    $('#iBTVMacreject').html(txt);
    if (iBTVMacrejectVal) {
        $('#iBTVMacreject').val(iBTVMacrejectVal);
    }
    if (dolog) console.log('iBTVMstyleName ' + iBTVMstyleName);
    var txt = getSelectHtml(iBTVMstyleValues);
    $('#iBTVMstyle').html(txt);
    if (iBTVMstyleVal) {
        $('#iBTVMstyle').val(iBTVMstyleVal);
    }

    $('#iBTVMfreq').val(iBTVMfreq);
    $('#iBTVMfreqUnits').val(iBTVMfreqUnits);

/*
     if (dolog) console.log('iBTVMscheduled ' + iBTVMscheduled);
     $('#iBTVMscheduled').prop('checked', iBTVMscheduled).checkboxradio("refresh");

    $('#iBTVMprefix').val(iBTVMprefix);

    if (dolog) console.log('iBTVMtimestamp ' + iBTVMtimestamp);
    $('#iBTVMtimestamp').prop('checked', iBTVMtimestamp).checkboxradio("refresh");

    if (dolog) console.log('iBTVMaddGPS ' + iBTVMaddGPS);
    $('#iBTVMaddGPS').prop('checked', iBTVMaddGPS).checkboxradio("refresh");
*/
    if (dolog) console.log('iBTVMemail ' + iBTVMemail);
    $('#iBTVMemail').prop('checked', iBTVMemail).checkboxradio("refresh");
    $('#iBTVMrecipients').val(iBTVMrecipients);
    if (iBTVMemail)
        $('#iBTVMrecipients').textinput('enable');
    else
        $('#iBTVMrecipients').textinput('disable');

    $('select').selectmenu('refresh', true);

/*
     $('#iBTVMstartTime').val(iBTVMstartTime);
     $('#iBTVMstopTime').val(iBTVMstopTime);
     $('#iBTVMstartDate').val(iBTVMstartDate);
     $('#iBTVMstopDate').val(iBTVMstopDate);
*/

    function styleChange() {
        if (iBTVMstyleVal === 'single') {
            $('#iBTVMfreq').textinput('enable');
            $('#iBTVMfreqUnits').selectmenu('enable');
            $('#iBTVMonoff').hide();
            $('iBTVMsinglepredom').hide();
            $('#iBTVMvoltsOffDiv').hide();
            $('#iBTVMvoltsOnLabel').html('Voltage:');
        } else if (iBTVMstyleVal === 'single/predom') {
            $('#iBTVMfreq').textinput('disable');
            $('#iBTVMfreqUnits').selectmenu('disable');
            $('#iBTVMonoff').hide();
            $('#iBTVMsinglepredom').show();
            $('#iBTVMvoltsOffDiv').hide();
            $('#iBTVMvoltsOnLabel').html('Voltage:');
        } else if (iBTVMstyleVal === 'on/off' || iBTVMstyleVal === 'max/min') {
            $('#iBTVMfreq').textinput('disable');
            $('#iBTVMfreqUnits').selectmenu('disable');
            $('#iBTVMonoff').show();
            $('#iBTVMsinglepredom').hide();
            $('#iBTVMvoltsOffDiv').show();
            $('#iBTVMvoltsOnLabel').html('On Voltage:');
        }
    }

    $('#iBTVMcycleOn').val(iBTVMcycleOn);
    $('#iBTVMcycleOff').val(iBTVMcycleOff);
    $('#iBTVMpredom').val(iBTVMpredom);

    styleChange();

    // ------------------------------------------------------------------------------------------------

    $("#iBTVMdevice").click(function() {
        devSelect();
        return false;
    });
/*
     $("#iBTVMdevices").click(function() { 
         if (voltageInterval) {
             stopTimerAndMeter(); 
             setTimeout(function() { 
                 iBTVMdiscover(undefined, function() { devSelect(); });
             }, 1000)
         } else { 
             iBTVMdiscover(undefined, function() { devSelect(); }); 
         }
         return false; 
     });
     $("#iBTVMdevice").change(function() { 
         if (dolog) console.log('iBTVMdevice.change()');
         iBTVMname = $('#iBTVMdevice option:selected').text(); 
         iBTVMaddress = $('#iBTVMdevice').val();
         setLocalItem("iBTVMname", iBTVMname); 
         setLocalItem("iBTVMaddress", iBTVMaddress); 
         if (!voltageInterval) { 
             startTimerAndMeter(); 
         } else {
             iBTVMrestart(); 
         }
         return false; 
     });
*/
    $("#iBTVMstyle").change(function() {
        iBTVMstyleName = $('#iBTVMstyle option:selected').text();
        iBTVMstyleVal = $('#iBTVMstyle').val();
        setLocalItem("iBTVMstyleName", iBTVMstyleName);
        setLocalItem("iBTVMstyleVal", iBTVMstyleVal);

        styleChange();

        if (voltageInterval) {
            // restart timer and meter as logging frequency *has* changed
            restartTimerAndMeter();
        }
        return false;
    });
    $("#iBTVMrange").change(function() {
        iBTVMrangeName = $('#iBTVMrange option:selected').text();
        iBTVMrangeVal = $('#iBTVMrange').val();
        setLocalItem("iBTVMrangeName", iBTVMrangeName);
        setLocalItem("iBTVMrangeVal", iBTVMrangeVal);
        if (voltageInterval) {
            // just restart meter as logging frequency is unchanged
            restartMeter();
        }
        $("#iBTVMfreq").trigger("change");
        return false;
    });
    $("#iBTVMacreject").change(function() {
        iBTVMacrejectName = $('#iBTVMacreject option:selected').text();
        iBTVMacrejectVal = $('#iBTVMacreject').val();
        setLocalItem("iBTVMacrejectName", iBTVMacrejectName);
        setLocalItem("iBTVMacrejectVal", iBTVMacrejectVal);
        if (voltageInterval) {
            // just restart meter as logging frequency is unchanged
            restartMeter();
        }
        $("#iBTVMfreq").trigger("change");
        return false;
    });

    $('#iBTVMcycleOn').change(function() {
        iBTVMcycleOn = $('#iBTVMcycleOn').val();
        setLocalItem("iBTVMcycleOn", iBTVMcycleOn);
        if (voltageInterval) {
            // restart timer and meter as logging frequency *has* changed
            restartTimerAndMeter();
        }
    });
    $('#iBTVMcycleOff').change(function() {
        iBTVMcycleOff = $('#iBTVMcycleOff').val();
        setLocalItem("iBTVMcycleOff", iBTVMcycleOff);
        if (voltageInterval) {
            // restart timer and meter as logging frequency *has* changed
            restartTimerAndMeter();
        }
    });
    $('#iBTVMpredom').change(function() {
        iBTVMpredom = $('#iBTVMpredom').val();
        setLocalItem("iBTVMpredom", iBTVMpredom);
        if (voltageInterval) {
            // just restart meter as logging frequency is unchanged
            restartMeter();
        }
    });

    $("#iBTVMfreq").change(function() {
        iBTVMfreq = $('#iBTVMfreq').val();
        if (dolog) console.log('iBTVMfreqUnits: ' + iBTVMfreqUnits);
        if (dolog) console.log('iBTVMfreq: ' + iBTVMfreq);

        var rangeItem = getRangeItem(iBTVMrangeVal);
        var acrejectItem = getACrejectItem(iBTVMacrejectVal);
        var min = rangeItem.freq;
        if (acrejectItem.freq) {
            min = Math.max(acrejectItem.freq, min);
        }
        var freq = getFrequency();
        // restrict frequency to minimum for range/acrejection selected
        if (!iBTVMfreq || !$.isNumeric(iBTVMfreq) || getFrequency() < min) {            
            Toast.longshow('Recording Interval set to ' + min + 'ms minimum');            
            iBTVMfreq = min.toString();
            $('#iBTVMfreq').val(iBTVMfreq);
            iBTVMfreqUnits = 'ms';
            $('#iBTVMfreqUnits').val(iBTVMfreqUnits)
            $('#iBTVMfreqUnits').selectmenu('refresh', true);
            setLocalItem("iBTVMfreqUnits", iBTVMfreqUnits);
        }
        setLocalItem("iBTVMfreq", iBTVMfreq);

        if (voltageInterval) {
            if (meterFreq !== undefined && meterFreq != getMeterFrequency()) {
                // this isn't quite working - getting a double start for some
                // reason ... left freq restricted to 100ms for now
                if (dolog) console.log('restartTimerAndMeter');
                restartTimerAndMeter(); // allow for switching from 20 to 25ms delay
            } else {
                // just restart timer as meter is already running at (correct) top speed
                if (dolog) console.log('restartTimer');
                restartTimer();
            }
        }
        return false;
    });
    $("#iBTVMfreqUnits").change(function() {
        iBTVMfreqUnits = $('#iBTVMfreqUnits').val();
        setLocalItem("iBTVMfreqUnits", iBTVMfreqUnits);
        $("#iBTVMfreq").trigger("change");
        return false;
    });
/*
    $('#iBTVMprefix').change(function() {
        var prefix = $('#iBTVMprefix').val();
        if (prefix !== '' && !validateFileName(prefix)) {
            $('#iBTVMprefix').val(iBTVMprefix);
        } else {
            iBTVMprefix = prefix;
            setLocalItem("iBTVMprefix", iBTVMprefix);
        }
        return false;
    })

    $("#iBTVMtimestamp").click(function() {
        if (dolog) console.log('iBTVMtimestamp click event');
        iBTVMtimestamp = this.checked;
        setLocalItem("iBTVMtimestamp", iBTVMtimestamp);
        if (iBTVMaddGPS) {
            iBTVMaddGPS = false;
            $('#iBTVMaddGPS').prop('checked', iBTVMaddGPS).checkboxradio("refresh");
            setLocalItem("iBTVMaddGPS", iBTVMaddGPS);
        }
        return false;
    });

    $("#iBTVMaddGPS").click(function() {
        if (dolog) console.log('iBTVMaddGPS click event');
        iBTVMaddGPS = this.checked;
        setLocalItem("iBTVMaddGPS", iBTVMaddGPS);
        if (iBTVMtimestamp) {
            iBTVMtimestamp = false;
            $('#iBTVMtimestamp').prop('checked', iBTVMtimestamp).checkboxradio("refresh");
            setLocalItem("iBTVMtimestamp", iBTVMtimestamp);
        }
        return false;
    });
*/
    $('#iBTVMemail').click(function() {
        if (dolog) console.log('iBTVMemail click event');
        iBTVMemail = this.checked;
        setLocalItem("iBTVMemail", iBTVMemail);
        if (iBTVMemail)
            $('#iBTVMrecipients').textinput('enable');
        else
            $('#iBTVMrecipients').textinput('disable');
        return false;
    });

    $('#iBTVMrecipients').change(function() {
        iBTVMrecipients = $('#iBTVMrecipients').val();
        setLocalItem("iBTVMrecipients", iBTVMrecipients);
        return false;
    });

    $("#iBTVMscheduled").click(function() {
        if (dolog) console.log('iBTVMscheduled click event');
        iBTVMscheduled = this.checked;
        if (dolog) console.log('iBTVMscheduled ' + iBTVMscheduled);

        if (iBTVMscheduled && logging) {
            // turn off scheduling if in the past and already logging
            var stopDate = dateAmerican(iBTVMstopDate);
            var dt = new Date();
            if (stopDate.getTime() < dt.getTime()) {
                iBTVMscheduled = false;
                $('#iBTVMscheduled').prop('checked', iBTVMscheduled).checkboxradio("refresh");
                setLocalItem("iBTVMscheduled", iBTVMscheduled);
            } else {
                setLocalItem("iBTVMscheduled", iBTVMscheduled);
                loggingChange();
            }
        } else {
            setLocalItem("iBTVMscheduled", iBTVMscheduled);
            loggingChange();
        }
    });

    $('#iBTVMdropbox').click(function() {
                             
        if (this.checked) {
                             
             function dropBoxSuccess(obj) {
                    
                             
                             
                 if (obj != "OK") {
                    
                    $('#iBTVMdropbox').prop('checked', false).checkboxradio("refresh");
                 }
             }
             
             function dropBoxFailure(err) {
                             
                             console.log("Logging BREAKPOINT touched the dropbox FAILURE");

                             
//                 if (err.res === undefined || err.res !== 0) {
//                    alert(err.error); // e.g. Link Activity returned error 0
//                 }
                 $('#iBTVMdropbox').prop('checked', false).checkboxradio("refresh");
             }
                             
            navigator.dropbox_sync.linkAccount( dropBoxSuccess, dropBoxFailure);
                             
        } else {
            navigator.dropbox_sync.unlinkAccount();
            $('#iBTVMdropbox').prop('checked', false).checkboxradio("refresh");
        }
        return false;
    });

    $("#iBTVMlogStart").click(function() {
        if (logging) {
            // do stop stuff
            console.log('iBTVMlogStop click event');
            promptSaveData();
        } else {
            console.log('iBTVMlogStart click event');
            setLogging(true);
        }
        return false;

/*
        if (iBTVMscheduled) { // turn off scheduling if in the past 
            var stopDate = dateAmerican(iBTVMstopDate); 
            var dt = new Date(); 
            if (stopDate.getTime() < dt.getTime()) { 
                iBTVMscheduled = false;
                $('#iBTVMscheduled').prop('checked', iBTVMscheduled).checkboxradio("refresh");
                setLocalItem("iBTVMscheduled", iBTVMscheduled); 
            } 
        }         
        if (iBTVMprefix) filename = iBTVMprefix; 
        else filename = '';
         
        if (iBTVMtimestamp) { 
            var dt = new Date(); 
            if (filename !== '') filename += ' '; 
            filename += americanDate(dt,'-') + ' ' + pad(dt.getHours()) + '-' + pad(dt.getMinutes()) + '-' + pad(dt.getSeconds()); 
            if (dolog) console.log(filename);
        } 
        else if (iBTVMaddGPS && gpsSuffix) {
            if (filename !== '') filename += ' ';
            filename += gpsSuffix; 
        }
        filename += '.csv';
         
        navigator.notification.prompt('', function(r) { 
            if (r.buttonIndex == 1) { // OK 
                if (validateFileName(filename)) {
                    $('#iBTVMfilename').html(filename); 
                    setLogging(true);
                }
            } else { // Cancel 
                filename = ''; 
                $('#iBTVMfilename').html(filename); 
            } 
        }, 'Enter Data Filename:', ['OK','Cancel'], filename ); 
        return false;         
*/
    });
    
/*
    $("#iBTVMstartTime").click(function() { 
        handleTime(this); 
        return false;
    }); 
    $("#iBTVMstartTime").taphold(function() { 
        handleTime(this, true);
        return false; 
    });
    
    $("#iBTVMstopTime").click(function() {
        handleTime(this); 
        return false;
    }); 
    $("#iBTVMstopTime").taphold(function() {
        handleTime(this, true);
        return false; 
    });
    
    $("#iBTVMstartDate").click(function() {
        handleDate(this); 
        return false;
    }); 
    $("#iBTVMstopDate").click(function() { 
        handleDate(this); 
        return false; 
    });
*/

    // ------------------------------------------------------------------------------------------------
/*
    $("#iBTVMstart").click(function() { 
        if (!voltageInterval) {
            startTimerAndMeter(); 
        } else { 
            iBTVMrestart(); 
        }
        return false; 
    });
    $("#iBTVMstart").taphold(function() { 
        if (!voltageInterval) {
            startTimerAndMeter(true); // spoof voltages 
        } else { 
            iBTVMrestart();
        }
        return false; 
    });
    
    $("#iBTVMstop").click(function() { 
        stopTimerAndMeter(true); 
        return false;
    });
*/

    $("#iBTVMdiagnostics").click(function() {
                                 
    console.log("logging touch the iBTVMdiagnostics");
                                 
    function doDiagnostic() {
        if (dolog) console.log('doDiagnostic');
        iBTVMdiagnostic(
            function(json) {
                if (dolog) console.log('Diagnostic result: ' + JSON.stringify(json));
                var diag = 
                    'Device: ' + iBTVMname + '\r\n' +
                    'Version: ' + versionNumber + '\r\n' +
                    'Address: ' + iBTVMaddress + '\r\n' +
                    'Firmware: ' + json.firmware + '\r\n' +
                    'Serial #: ' + json.serial + '\r\n' +
                    'Battery: ' + json.battery + '\r\n' +
                    'GPS Power: ' + json.gps_power + '\r\n' +
                    'Sats: ' + json.sats + '\r\n' +
                    'VM Power: ' + json.vm_power + '\r\n' +
                    'Range: ' + ((json.vmRange !== undefined) ? 
                            iBTVMrangeValues[json.vmRange].val : '') + '\r\n' +
                    'AC Rejection: ' + ((json.vmACRejection !== undefined) ?
                            iBTVMacrejectValues[json.vmACRejection].name : '') + '\r\n' +
                    'Voltage: ' + ((json.vmRange !== undefined && json.voltsOn !== undefined) ?
                            formatVoltages(json, false, iBTVMrangeValues[json.vmRange]) : json.voltsOn);

                var diag2 = diag + '\r\n\r\n' +
                    'Raw Values:' + '\r\n' +
                    'Status: ' + json.status + '\r\n' + 
                    'GPS: ' + json.gps + '\r\n' + 
                    'Range: ' + json.range + '\r\n' +
                    'AC Rejection: ' + json.acreject + '\r\n' +
                    'Voltage: ' + json.voltage;

                startTimerAndMeter();

                navigator.notification.confirm(diag,
                    function(buttonIndex) {
                                               
                        console.log("Logging Button Index: " + buttonIndex);
                                               
                        if (buttonIndex == 1) {
                            saveDiagnostic(diag2, iBTVMemail);
                        }
                    }, 'Save Diagnostic Results?',
                    'Yes,No');
                }, function(err) {
                    if (dolog) console.log('Diagnostic err: ' + err);
                    // alert(err);
                    if (err === "Connect Error") {
                        $("#iBTVMdevice").buttonMarkup({theme : 'b'});
                        $('#iBTVMdevice .ui-btn-text').text('...');
                    } else {
                        startTimerAndMeter();
                    }
                });
        }

        stopTimerAndMeter(false, function() {
            activityStop(500);
            window.setTimeout(function() {
                doDiagnostic();
            }, 1000);
        });
        // return false;
    });

    $("#iBTVMclear").click(function() {
        /*
         * if (logging) { setLogging(false); logDataClear(); clearChart(true); }
         * else { clearChart(false); }
         */
        clearChart(false);
        if (ActivePage = 'ChartPage') {
            updateChart(true, tmrFreq, chartData, logData);
        } else {
            chartPending = true;
        }
        profileReset = true;
        return false;
    });

    function exitAppPopup() {
        navigator.notification.confirm(
            'Are you sure you want to exit?',
            function(buttonIndex) {
                if (buttonIndex == 1) {
                    if (logData.length > 0) {
                        // prompt to save
                        navigator.notification.confirm(
                            'Logged data has not been saved!',
                            function(buttonIndex) {
                                if (buttonIndex == 1) {
                                    promptSaveData(function() {
                                        stopTimerAndMeter(false, function() {
                                            navigator.app.exitApp();
                                        });
                                    });
                                } else if (buttonIndex == 2) {
                                    stopTimerAndMeter(false, function() {
                                        navigator.app.exitApp();
                                    });
                                }
                            }, 
                            'Save Logged Data?',
                            'Yes,No,Cancel'
                        );
                    } else {
                        stopTimerAndMeter(false, function() {
                            navigator.app.exitApp();
                        });
                    }
                }
            },
            'Leaving ' + app_name + '?', 
            'Yes,No'
        );
        return false;
    }

    function onBackKeyDown() {
        exitAppPopup();
        return false;
    }
    
    if (!usingBrowser() && device.platform === 'Android') {
        //Android SPECIFIC
        document.addEventListener("backbutton", onBackKeyDown, false);
    }    
    
    // button not visible now
    $("#iBTVMexit").click(function() {
        exitAppPopup();
        return false;
    });

    // ------------------------------------------------------------------------------------------------
}

function getLocalSettings(callback) {
    if (usingBrowser()) {
        if (callback) callback();
        return;
    }
    if (dolog) console.log('getLocalSettings() - dataPath: ' + dataPath);

    var path = internal_root + '/' + dataFolder;
    loadLocalData(path, "settings.json", function(res) {
        settings = JSON.parse(res);
        if (dolog) console.log('getLocalSettings(): success - settings: ' + JSON.stringify(settings));
        if (callback) callback();
    }, function() {
        if (dolog) console.log('getLocalSettings(): fail');
        settings = {};
        if (callback) callback();
    });
}

function setLocalSettings() {
    if (usingBrowser()) {
        return;
    }
    // if (dolog) console.log('setLocalSettings()');
    var json = JSON.stringify(settings, null, 2);
    var path = internal_root + '/' + dataFolder;
    saveLocalData(path, "settings.json", json, function(res) {
                  
        console.log("Logging BREAKPOINT code reached setLocalSettings -> saveLocalData");

                  
        // if (dolog) console.log('setLocalSettings(): success');
    }, function(error) {
        if (dolog) console.log('setLocalSettings(): fail');
        alert('Failed to setLocalSettings: ' + error.message);
    }, false, true); // noSynch = true
}

function setLocalItem(name, value) {

    if (settings !== undefined && settings[name] === value) {
        return;
    }

    if (dolog) console.log('setLocalItem(' + name + ', ' + value + ')');
    if (settings === undefined) {
        getLocalSettings(function() {
            settings[name] = value;
            setLocalSettings();
        });
    } else {
        settings[name] = value;
        setLocalSettings();
    }
}

function getLocalItem(name, callback) {
    if (dolog) console.log('getLocalItem(' + name + ')');
    if (settings === undefined) {
        getLocalSettings(function() {
            if (callback) callback(settings[name]);
        });
    } else {
        if (callback) callback(settings[name]);
    }
}
