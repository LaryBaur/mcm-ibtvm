var sendBroadcastPlugin = function () {
};

sendBroadcastPlugin.prototype.mediaScanFile = function(argument,cbSuccess,cbError) {
   //if (dolog) console.log('mediaScanFile: ' + argument);                    
   return cordova.exec(cbSuccess, cbError, 'SendBroadcastPlugin', 'MediaScanFile', [argument]);
};

navigator.sendBroadcastPlugin = new sendBroadcastPlugin();
