var dolog = false;
//var dolog = true;

function pow2(x) {
    return Math.pow(x, 2);
}
function pow4(x) {
    return Math.pow(x, 4);
}
function sqrt(x) {
    return Math.sqrt(x);
}

function usingBrowser() {
    return (!navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/) ||
            !document.URL.match(/(file:|android_asset)/));
};

function activityStart(title, message) {
    if (!message) message = '';
    if (!usingBrowser() && device.platform === 'Android') {        
        //if (dolog) console.log('activityStart: ' + title);
        navigator.notification.activityStart(title, message);
    } else {      
        // problems with 'spinner' not displaying properly so went with simple text html
        // doc: http://jquerymobile.com/demos/1.2.0/docs/pages/loader.html
        $.mobile.loading( 'show', {
            theme: 'b',
            textVisible: true,
            textonly: true,
          /*            
            text: title + ": " + message,
            html: ""
          */
            html: "<div class='ui-bar ui-overlay-c ui-corner-all'>" +
                  "<table>"+
                  "<tr><td><h3>"+ title + "</h3></td></tr>" +
                  "<tr><td>&nbsp</td></tr>" +
                  "<tr><td>"+ message + "</td></tr>" +
                  "</table>"
        });      
    }
}

function activityStop(timeout) {
    if (!usingBrowser() && device.platform === 'Android') {
        //if (dolog) console.log('activityStop: ' + timeout);
        if (timeout) {
            setTimeout(function () {
                navigator.notification.activityStop();
            }, timeout);
        } else {
            navigator.notification.activityStop();
        }
    } else {
        $.mobile.loading( 'hide');  
    }
}

function saveThePage(dataPath, fn) {
    
    if (!fn) {
        var dt = new Date();
        fname = americanDate(dt,'-') + '-' +    
            pad(dt.getHours()) + '-' + pad(dt.getMinutes()) + '-' + pad(dt.getSeconds());
        fname += '.html';        
    }

    var html = "<html>\r\n<head>"+$("head").html() + "\r\n</head>\r\n<body>\r\n" + $("body").html()+"\r\n</body>\r\n</html>";    
    
    saveLocalData(dataPath, fn, html, function (res) {
        console.log("Logging BREAKPOINT code reached saveThePage -> saveLocalData");

        if (dolog) console.log('saveThePage: success');
    }, function (error) {
        if (dolog) console.log('saveThePage: fail');               
    }, false, true);        // no dropbox                       
}                 


function validateFileName(name, quiet) {
    var res = true;
    var err = '';
    
    if (!name || name.length < 1) {
        res = false;
        err = 'Name is too short';
    }  else if (name.length > 127) {
        res = false;
        err = 'Name is too long';
    }  
    if (res) {
//        var illegal = /[|\?\/\\\*<>:+\[\]\."']/g;                     
        var illegal = /[|\?\/\\\*<>:+\[\]"']/g;     // allow '.'                     
        if (illegal.test(name)) {
            res = false;
            var m = name.match(illegal);
            if (m.length === 1)
                err = 'Name contains illegal character: ' + m;
            else
                err = 'Name contains illegal characters: ' + m;
        }         
    }
    if (res) {
        for (var i=0;i<name.length;i++) {
            if (name.charCodeAt(i) < 32) {
                res = false;
                err = 'Name contains illegal character(s)';
                break;
            }
        }
    }
    if (!res && !quiet) {        
        /*navigator.notification.*/alert(err);  // this is blocking - intended for use when inside a validation loop
    }
    return res;
}

function replaceAll(find, replace, str) {
    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }                        
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}                    


function dateAmerican(str, sep) {
    if (!sep) sep = '/';
    // split into month/day/year
    var ar = str.split(sep);
    var month = parseInt(ar[0], 10)-1;      // (0-based)
    var day = parseInt(ar[1], 10);
    var year = parseInt(ar[2], 10);
    var dt = new Date(year, month, day);
    return dt;        
} 

function americanDate(dtobj, sep) {
    if (dtobj === undefined)
        dtobj = new Date();
    var m = dtobj.getMonth() + 1;
    var d = dtobj.getDate();
    var y = dtobj.getFullYear();
    
    if (!sep) sep = '/';
    var ad = m + sep;
    if (d < 10)
        ad += '0';
    ad += d + sep + y;
    return ad;
}

function verInt(version) {
    var osver = version.split('.');
    var maj = parseInt(osver[0], 10);
    var min = parseInt(osver[1], 10);
    var rls = parseInt(osver[2], 10);
    if (isNaN(maj))
        maj = 0;
    if (isNaN(min))
        min = 0;
    if (isNaN(rls))
        rls = 0;

    return ((maj * 1000) + min) * 1000 + rls; // numbers are 64-bit so allow 3 digits each
    // e.g. 2.3.6 -> 2003006
    // e.g. 4.11.123 -> 400011123
}

Number.prototype.toHrsMin = function() {
    var h = Math.floor(Math.abs(this));
    var m = Math.round((Math.abs(this) - h) * 60);

    if (m == 60) {
        //if (dolog) console.log('h='+h + ', m='+m);
        h++;
        m = 0;
        //if (dolog) console.log('h:='+h + ', m:='+m);
    }

    var txt;
    if (h == 0 && m == 0) {
        txt = h.toString() + 'h ' + m.toFixed(0) + 'm';
    } else if (m == 0) {
        if (h == 1)
            txt = h.toString() + ' hour';
        else
            txt = h.toString() + ' hours';
    } else if (h == 0) {
        txt = m.toFixed(0) + ' min';
    } else {
        txt = h.toString() + 'h ' + m.toFixed(0) + 'm';
    }
    if (this < 0)
        txt = '-' + txt;
    return txt;
};

function prevMonth() {
    var thisMonth = this.getMonth();
    this.setMonth(thisMonth - 1);
    if (this.getMonth() != thisMonth - 1
            && (this.getMonth() != 11 || (thisMonth == 11 && this.getDate() == 1)))
        this.setDate(0);
}
function nextMonth() {
    var thisMonth = this.getMonth();
    this.setMonth(thisMonth + 1);
    if (this.getMonth() != thisMonth + 1 && this.getMonth() != 0)
        this.setDate(0);
}
Date.prototype.nextMonth = nextMonth;
Date.prototype.prevMonth = prevMonth;

function calcBusinessDays(dDate1, dDate2) { // input given as Date objects
    var iWeeks, iDays;

    if (dDate2 < dDate1) {
        iWeeks = dDate2; // dates transposed
        dDate2 = dDate1;
        dDate1 = iWeeks;
        iWeeks = 0;
    }

    var iWeekday1 = dDate1.getDay(); // day of week
    var iWeekday2 = dDate2.getDay();

    iWeeks = Math.floor((dDate2.getTime() - dDate1.getTime()) / 604800000);
    iDays = 0;
    while (iWeekday1 != iWeekday2) {
        if (iWeekday1 >= 1 && iWeekday1 <= 5)
            iDays++;
        iWeekday1++;
        if (iWeekday1 == 7)
            iWeekday1 = 0;
    }
    if (iWeekday2 >= 1 && iWeekday2 <= 5)
        iDays++;
    return iWeeks * 5 + iDays;
}

function workDaysBetween(next, prev, logCalc) {
    /*
     var days = (next.getTime() - prev.getTime())  / (1000*60*60*24); //total days remaining including weekend days
     if (logCalc !== undefined && logCalc == true) {
     if (dolog) console.log('There are '+days+' total days');
     }
     days = days - (2*Math.floor(days/7)); //remove 2 days for every full week
     var start_day = prev.getDay();
     var end_day = next.getDay();
     if (logCalc !== undefined && logCalc == true) {
     if (dolog) console.log('Start day '+start_day+' end day '+end_day);
     }
     if (start_day - end_day > 0) days -= 2;
     if (start_day == 0 && end_day != 6) days--;
     if (end_day == 6 && start_day != 0) days--;
     //RNR: if the period ends on a Sunday we have to add back one
     //because the ending day really isn't a part of the time span
     //I GUESS! All I know is that things don't pan out right without it.
     if (end_day == 0) days++;
     */
    days = calcBusinessDays(prev, next); //starting over with new algorithm!
    if (logCalc !== undefined && logCalc == true) {
        if (dolog) console.log('Work days ' + days);
    }
    return (days);
}

String.prototype.replaceAll = function(str1, str2, ignore) {
    return this.replace(new RegExp(str1.replace(
            /([\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, function(c) {
                return "\\" + c;
            }), "g" + (ignore ? "i" : "")), str2);
};

String.prototype.startsWith = function(prefix) {
    return this.indexOf(prefix) === 0;
}

String.prototype.endsWith = function(suffix) {
    return this.match(suffix+"$") == suffix;
};


//--------------------------------------------------------------------------------------------------------------------

//jQuery 1.7.x does not consider XHR result of 0 a success in Android 4.0.3 or better.
//So we need to do our own ajax routine
var PW_ajax = function() {
    this.init();
};

PW_ajax.prototype.data = {
    status : 'uninitialized',
    exception : {},
    // proxy: 'http://localhost:6518/apidebug.php?mode=native&url=', //cross-site scripting proxy        
    proxy : '', //debug in emulator or device
    useJQuery : true,
    flags : 0,
    bitAndroidCheck : 1
};

PW_ajax.prototype.init = function() {
    this.data.status = 'initialized';
};

/*
 PW_ajax.prototype.get = function(url,onSuccess,onError,timeout) {
 var self = this;
 var data = this.data;

 function massageUrl(url) {
 if (data.proxy.length == 0) return url;
 return data.proxy+encodeURIComponent(url);
 }

 if (timeout !== undefined) {
 timeout *= 1000;
 } else {
 timeout = 0;
 }    

 //can't do this in the init as the document has not been loaded yet
 if ((data.flags & data.bitAndroidCheck) == 0) {
 if (data.proxy.length == 0) { //no PhoneGap in Chrome/Firefox
 if (dolog) console.log('Platform = '+device.platform+' '+device.version);
 if (dolog) console.log('verInt = '+verInt(device.version));     
 if ((device.platform == 'Android') && (verInt(device.version) >= verInt('4.0.3'))) 
 data.useJQuery = false;
 else
 data.useJQuery = true;
 }
 if (dolog) console.log('UseJQuery = '+data.useJQuery);
 data.flags |= data.bitAndroidCheck;
 if (dolog) console.log('Hitting URL: '+massageUrl(url));
 }
 if (!data.useJQuery) {

 var req = new XMLHttpRequest();
 req.open('GET', massageUrl(url), true);

 req.setRequestHeader("Accept", "application/json");
 if (timeout != 0) {
 var requestTimer=setTimeout(function (){
 // override onreadystatechange so that abort doesn't trigger success! 
 req.onreadystatechange = function() {
 if (dolog) console.log('ajax req aborted: timeout');
 if (onError != undefined) onError('timeout');                    
 };
 req.abort();
 }, timeout);
 }                     
 req.onreadystatechange = function() {       
 if (req.readyState == 4) {          
 if (timeout != 0) {
 clearTimeout(requestTimer);
 }
 if ((req.status == 200) || (req.status == 0)) {
 if (dolog) console.log('ajax req succeeds: '+req.status);
 var res;
 try {
 res = JSON.parse(req.responseText); //safer than eval
 }
 catch(err) {
 if (dolog) console.log('JSON parsing error on successful return, response was: '+req.responseText);
 }
 if (onSuccess != undefined) onSuccess(res);
 } else {
 if (dolog) console.log('ajax req fails: '+req.status+' '+req.responseText);
 if (onError != undefined) onError(req.statusText);
 }
 }           
 };
 req.send(null);
 } else { //jQuery version
 $.ajax({
 type: 'GET',
 url: massageUrl(url),
 timeout: timeout,
 success: function(response) {
 if (dolog) console.log('success on ajax request:');
 if (onSuccess != undefined) onSuccess(response);
 },
 error: function(request, status, error) {
 if (dolog) console.log('failed ajax request ==> '+status+':'+error+'\n'+JSON.stringify(request));
 if (dolog) console.log(error);
 if (onError != undefined) onError(error);
 }
 });     
 }
 };
 */

PW_ajax.prototype.json = function(usr, pwd, method, url, dataset, datatype,
        onSuccess, onError, timeout) {
    var self = this;
    var data = this.data;

    function massageUrl(url) {
        if (data.proxy.length == 0)
            return url;
        return data.proxy + encodeURIComponent(url);
    }

    if (timeout !== undefined) {
        timeout *= 1000;
    } else {
        timeout = 0;
    }

    //can't do this in the init as the document has not been loaded yet
    if ((data.flags & data.bitAndroidCheck) == 0) {
        if (data.proxy.length == 0) { //no PhoneGap in Chrome/Firefox
            if (dolog) console.log('Platform = ' + device.platform + ' ' + device.version);
            if (dolog) console.log('verInt = ' + verInt(device.version));
            if ((device.platform == 'Android')
                    && (verInt(device.version) >= verInt('4.0.3')))
                data.useJQuery = false;
            else
                data.useJQuery = true;
        }
        if (dolog) console.log('UseJQuery = ' + data.useJQuery);
        data.flags |= data.bitAndroidCheck;
        if (dolog) console.log('Hitting URL: ' + massageUrl(url));
    }
    if (!data.useJQuery) {

        var authstr = "Basic " + window.btoa(usr + ":" + pwd);
        var req = new XMLHttpRequest();
        req.open(method, massageUrl(url), true);
        req.setRequestHeader("Authorization", authstr);
        req.setRequestHeader("Accept", "application/json");
        if (method == "POST") {
            req.setRequestHeader("Content-Type", datatype + ";charset=UTF-8");
            req.setRequestHeader("Content-Length", dataset.length);
            req.setRequestHeader("Connection", "close");
        }
        if (timeout != 0) {
            var requestTimer = setTimeout(function() {
                // override onreadystatechange so that abort doesn't trigger success! 
                req.onreadystatechange = function() {
                    if (dolog) console.log('ajax req aborted: timeout');
                    if (onError != undefined)
                        onError('timeout');
                };
                req.abort();
            }, timeout);
        }
        req.onreadystatechange = function() {
            if (req.readyState == 4) {
                if (timeout != 0) {
                    clearTimeout(requestTimer);
                }
                if ((req.status == 200) || (req.status == 0)) {
                    if (dolog) console.log('ajax req succeeds: ' + req.status);
                    var res;
                    try {
                        res = JSON.parse(req.responseText); //safer than eval
                    } catch (err) {
                        console
                                .log('JSON parsing error on successful return, response was: '
                                        + req.responseText);
                    }
                    if (onSuccess != undefined)
                        onSuccess(res);
                } else {
                    if (dolog) console.log('ajax req fails: ' + req.status + ' '
                            + req.responseText);
                    if (onError != undefined)
                        onError(req.statusText);
                }
            }
        };
        if (method == "POST") {
            req.send(dataset);
        } else {
            req.send(null);
        }
    } else { //jQuery version
        $.ajax({
            type : method,
            url : massageUrl(url),
            dataType : "json",
            contentType : datatype,
            data : dataset,
            headers : {
                "Accept" : "application/json"
            },
            username : usr,
            password : pwd,
            timeout : timeout,
            success : function(response) {
                if (dolog) console.log('success on ajax request:'); //\n'+JSON.stringify(response).replaceAll(":", ": "));
                if (onSuccess != undefined)
                    onSuccess(response);
            },
            error : function(request, status, error) {
                if (dolog) console.log('failed ajax request ==> ' + status + ':' + error
                        + '\n' + JSON.stringify(request));
                self.data.status = 'error';
                self.data.exception = {
                    "method" : "init",
                    "result" : "error",
                    "request" : request,
                    "status" : status,
                    "error" : error
                };
                if (dolog) console.log(error);
                if (onError != undefined)
                    onError(error);
            }
        });
    }
};

var pw_ajax;

/*
 function ajaxget(url,onSuccess,onError,timeout) {    
 if (!pw_ajax) {
 pw_ajax = new PW_ajax(); 
 }
 pw_ajax.get(url,onSuccess,onError,timeout);   
 }
 */

function ajaxjson(usr, pwd, method, url, dataset, onSuccess, onError, timeout) {
    if (!pw_ajax) {
        pw_ajax = new PW_ajax();
    }
    pw_ajax.json(usr, pwd, method, url, dataset, "application/json", onSuccess,
            onError, timeout);
}

function ajaxjsontype(usr, pwd, method, url, dataset, datatype, onSuccess,
        onError, timeout) {
    if (!pw_ajax) {
        pw_ajax = new PW_ajax();
    }
    pw_ajax.json(usr, pwd, method, url, dataset, datatype, onSuccess, onError,
            timeout);
}
function CreateGUID() {
    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4()
            + S4() + S4());
}

function aesEncrypt(text, pass) {
    if (!pass) {
        if (device.platform == 'Android')
            pass = device.uuid;
        else
            pass = device.name; // iOS doesn't like uuid       
    }
    GibberishAES.size(128);
    var encrypted = GibberishAES.enc(text, pass);
    return encrypted;
}
function aesDecrypt(encrypted, pass) {
    /*
     if (dolog) console.log('Device Name: ' + device.name); 
     if (dolog) console.log('Device Cordova: '  + device.cordova); 
     if (dolog) console.log('Device Platform: ' + device.platform); 
     if (dolog) console.log('Device Version: '  + device.version);
     if (dolog) console.log('Device UUID: ' + device.uuid); 
     if (dolog) console.log('encrypted: ' + encrypted);
     */

    if (!pass) {
        if (device.platform == 'Android')
            pass = device.uuid;
        else
            pass = device.name; // iOS doesn't like uuid       
    }

    var decrypted;
    try {
        GibberishAES.size(128);
        decrypted = GibberishAES.dec(encrypted, pass);
    } catch (e) {
        decrypted = undefined;
    }

    //    if (dolog) console.log('decrypted: ' + decrypted);   
    return decrypted;
}

var screenOrientation = function() {
};
screenOrientation.prototype.set = function(str, success, fail) {
    // if (dolog) console.log('set screenOrientation = ' + str);
    if (device.platform == "Android") {
        cordova.exec(success, fail, "ScreenOrientation", "set", [ str ]);
    }
};
screenOrientation.prototype.unlock = function(success, fail) {
    this.set('unspecified', success, fail);
};
navigator.screenOrientation = new screenOrientation();
