

function dbxName(path) {  
    var i = path.indexOf(dataFolder); 
    var res = path.substring(i+dataFolder.length);
    return res;
}

function dropBoxEnsureFolderExists(path, callback) {
    
    var isLinked = navigator.dropbox_sync.isLinked();

    
    if (isLinked) {
        var s = dbxName(path);            
        navigator.dropbox_sync.ensureFolderExists(s, function() {
            if (dolog) console.log("ensureFolderExists: success: "+ s);
            if (callback) callback(true);
        },
        function(err) {
            console.error("ensureFolderExists: error: "+err);
            if (callback) callback(false);
        });  
    } else {
        if (callback) callback(true);
    }
}

function dropBoxAddFile(path, fname, callback) {
    
    var isLinked = navigator.dropbox_sync.isLinked(function(linked){
                                                   
                                                   if (linked == true) {
                                                   if (dolog) console.log('dropBoxAddFile: ' + path + ' ' +  fname);
                                                   var s = dbxName(path) + '/' + fname;
                                                   navigator.dropbox_sync.addFile(path+'/'+fname,true,function() {
                                                                                  if (dolog) console.log("addFile: success: "+ s);
                                                                                  if (callback) callback(true);
                                                                                  },
                                                                                  function(err) {
                                                                                  console.error("addFile: : error: "+err);
                                                                                  if (callback) callback(false);
                                                                                  }, s);
                                                   } else {
                                                         if (callback) callback(true);
                                                   }
                                                   
                                                   });
    
    
//    if (isLinked) {
//        if (dolog) console.log('dropBoxAddFile: ' + path + ' ' +  fname);
//        var s = dbxName(path) + '/' + fname;            
//        navigator.dropbox_sync.addFile(path+'/'+fname,true,function() {
//            if (dolog) console.log("addFile: success: "+ s);                
//            if (callback) callback(true);
//        },
//        function(err) {
//            console.error("addFile: : error: "+err);
//            if (callback) callback(false);
//        }, s);                        
//    } else {
//        if (callback) callback(true);
//    }
}







