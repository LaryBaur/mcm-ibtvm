var connectedTo = "";
var getPending = false;

var iBTVMinitialized = false;
var iBTVMname = "" 
var iBTVMaddress = "";
var iBTVMdiscovery = ""
var iBTVMdevs = [];   
    
var iBTVMrangeName = "";
var iBTVMrangeVal = ""; _rangeVal = ""; // value in use
var iBTVMrangeValues = [];

var iBTVMconfig;    // returned after successful config change

var iBTVMacrejectName = "";
var iBTVMacrejectVal = "";
var iBTVMacrejectValues = [];

var iBTVMstyleName = "";
var iBTVMstyleVal = "";
var iBTVMstyleValues = [];

var iBTVMgpsName = "";
var iBTVMgpsVal = "";
var iBTVMgpsValues = [];

var iBTVMfreq = "";
var iBTVMfreqUnits = "";
var iBTVMscheduled = false;

var iBTVMprefix = "";
var iBTVMtimestamp = false;
var iBTVMaddGPS = true;

var iBTVMemail = false;
var iBTVMrecipients = "";


var iBTVMstartTime = "";
var iBTVMstopTime = "";
var iBTVMstartDate = "";
var iBTVMstopDate = "";

var iBTVMcycleOn = "";
var iBTVMcycleOff = "";
var iBTVMpredom = "";

var meterFreq = undefined;


function getSelectHtml(items) {
    var txt = '';
    for(var i=0;i<items.length;i++) {
        txt += '<option value="' + items[i].val + '">' + items[i].name + '</option>';
    }                
    return txt;
}
    
function iBTVMInitialize() {
    if (iBTVMinitialized) return;

    function iBTVMinitializeResult(json){
        if (dolog) console.log(JSON.stringify(json));
    }    
    function iBTVMinitializeError(err){
        Toast.extendedshow(err); 
        if (dolog) console.log(err);
    }    
    
    if (!usingBrowser()) {
        navigator.iBTVM.initialize(null, iBTVMinitializeResult, iBTVMinitializeError);
    }
    
    iBTVMname = settings["iBTVMname"];        
    iBTVMaddress = settings["iBTVMaddress"];        
    iBTVMdiscovery = settings["iBTVMdiscovery"];
    
    if (iBTVMdiscovery)
        iBTVMdevs = eval(iBTVMdiscovery);

    if (iBTVMdiscovery && (!iBTVMname || !iBTVMaddress)) {
        if (iBTVMdevs.length > 0) {
            iBTVMname = iBTVMdevs[0].name;        
            iBTVMaddress = iBTVMdevs[0].address;
        }
    }
    iBTVMrangeName = settings["iBTVMrangeName"]; 
    iBTVMrangeVal = settings["iBTVMrangeVal"];     
    
    var ohm = '&#8486';
    
    iBTVMrangeValues = 
        [{name: '40mVDC, 10M'+ohm,  val: '40 mVDC 10 Mohm', dp: 2, lbl: 'mVDC', freq: 50, demo: -0.02},
         {name: '400mVDC, 10M'+ohm, val: '400 mVDC 10 Mohm', dp: 1, lbl: 'mVDC', freq: 50, demo: -0.2},            
         {name: '5.7VDC, 400M'+ohm, val: '5.7 VDC 400 Mohm', dp: 3, lbl: 'VDC', freq: 50, demo: -2},           
         {name: '40VDC, 75M'+ohm,   val: '40 VDC  75 Mohm', dp: 2, lbl: 'VDC', freq: 50, demo: -20.0},           
         {name: '57VDC, 75M'+ohm,   val: '57 VDC  75 Mohm', dp: 2, lbl: 'VDC', freq: 50, demo: -20.0},           
         {name: '57VDC, 400M'+ohm,  val: '57 VDC  400 Mohm', dp: 2, lbl: 'VDC', freq: 50, demo: -20.0},
         {name: '400VDC, 75M'+ohm,  val: '400 VDC 75 Mohm', dp: 1, lbl: 'VDC', freq: 50, demo: -200.0},           
         {name: '570VDC, 75M'+ohm,  val: '570 VDC 75 Mohm', dp: 1, lbl: 'VDC', freq: 50, demo: -200.0},           
         {name: '40VAC, 75M'+ohm,   val: '40 VAC  75 Mohm', dp: 2, lbl: 'VAC', freq: 1500, demo: 20.0},           
         {name: '400VAC, 75M'+ohm,  val: '400 VAC 75 Mohm', dp: 1, lbl: 'VAC', freq: 1500, demo: 200.0}];    
    
    if (!iBTVMrangeName || !iBTVMrangeVal) {
        iBTVMrangeName = iBTVMrangeValues[2].name;        
        iBTVMrangeVal = iBTVMrangeValues[2].val; 
    }
    iBTVMacrejectName = settings["iBTVMacrejectName"]; 
    iBTVMacrejectVal = settings["iBTVMacrejectVal"];     
    
    iBTVMacrejectValues = 
        [{name: '60Hz', val: '60Hz AC Rejection'},
         {name: '50Hz', val: '50Hz AC Rejection'},            
         {name: '30Hz', val: '30Hz AC Rejection'},           
         {name: '25Hz', val: '25Hz AC Rejection', freq: 120},           
         {name: '16.6Hz', val: '16.6Hz AC Rejection'},           
         {name: 'None', val: 'No AC Rejection'}];       
    
    if (!iBTVMacrejectName || !iBTVMacrejectVal) {
        iBTVMacrejectName = iBTVMacrejectValues[0].name;        
        iBTVMacrejectVal = iBTVMacrejectValues[0].val; 
    }    
    
    iBTVMstyleName = settings["iBTVMstyleName"];
    iBTVMstyleVal = settings["iBTVMstyleVal"];

    iBTVMstyleValues = 
        [{name: 'Single Read', val: 'single'},
         {name: 'On/Off Pairs (D.S.P)', val: 'on/off'},
         {name: 'On/Off Pairs (Max/Min)', val: 'max/min'}
//         ,{name: 'Single/Predominant', val: 'single/predom'}          MCM decided they don't want this
         ];    

    if (!iBTVMstyleName || !iBTVMstyleVal) {
        iBTVMstyleName = iBTVMstyleValues[0].name;        
        iBTVMstyleVal = iBTVMstyleValues[0].val; 
    }    

    iBTVMgpsName = settings["iBTVMgpsName"];
    iBTVMgpsVal = settings["iBTVMgpsVal"];

    iBTVMgpsValues = 
        [{name: 'G4,0', val: 'GPS Time & Date'},
         {name: 'G3,0', val: 'GPS Latitude & Longitude'},
         {name: 'G2,0', val: 'GPS All'},
         {name: 'G1',   val: 'GPS Sync relative time stamp'},
         {name: 'G0',   val: 'GPS Off'}];           

    if (!iBTVMgpsName || !iBTVMgpsVal) {
        iBTVMgpsName = iBTVMgpsValues[0].name;        
        iBTVMgpsVal = iBTVMgpsValues[0].val; 
    }               
    
    iBTVMfreq = settings["iBTVMfreq"];
    if (!iBTVMfreq) {
        iBTVMfreq = "500";
    }
    iBTVMfreqUnits = settings["iBTVMfreqUnits"];
    if (!iBTVMfreqUnits) {
        iBTVMfreqUnits = "msec";                    
    }    
    
    iBTVMscheduled = settings["iBTVMscheduled"];    

    iBTVMprefix = settings["iBTVMprefix"];
/*
    iBTVMtimestamp = settings["iBTVMtimestamp"];
    if (iBTVMtimestamp === undefined) iBTVMtimestamp = false;
    iBTVMaddGPS = settings["iBTVMaddGPS"];    
    if (iBTVMaddGPS === undefined) iBTVMaddGPS = true;
    if (iBTVMaddGPS) iBTVMtimestamp = false;               // either/or    
*/    
    iBTVMaddGPS = true; iBTVMtimestamp = false;
    
    iBTVMemail = settings["iBTVMemail"];
    if (iBTVMemail === undefined) iBTVMemail = false;     
    iBTVMrecipients = settings["iBTVMrecipients"];    
    
    iBTVMstartTime = settings["iBTVMstartTime"];
    iBTVMstopTime = settings["iBTVMstopTime"];    
    iBTVMstartDate = settings["iBTVMstartDate"];
    iBTVMstopDate = settings["iBTVMstopDate"];    
    
    iBTVMcycleOn = settings['iBTVMcycleOn'];
    iBTVMcycleOff = settings['iBTVMcycleOff'];
    if (iBTVMcycleOn === undefined) iBTVMcycleOn = "700";
    if (iBTVMcycleOff === undefined) iBTVMcycleOff = "300";
    
    iBTVMpredom = settings['iBTVMpredom'];
    if (iBTVMpredom === undefined) iBTVMpredom = "300";
    
    
    var dt = new Date();
    dt.setHours(0,0,0,0);
    if (dolog) console.log(dt);        
    
    if (!iBTVMstartDate) {
        iBTVMstartDate = americanDate(dt);
        iBTVMscheduled = false;        
    } else {        
        var startDate = dateAmerican(iBTVMstartDate);        
        if (dolog) console.log('startDate: ' + startDate);
        if (startDate.getTime() < dt.getTime()) {
            iBTVMstartDate = americanDate(dt);
            iBTVMscheduled = false;        
            if (dolog) console.log('iBTVMstartDate: ' + iBTVMstartDate);
        }        
    }
    if (!iBTVMstopDate) {
        iBTVMstopDate = americanDate(dt);
        iBTVMscheduled = false;        
    } else {
        var stopDate = dateAmerican(iBTVMstopDate);
        if (dolog) console.log('stopDate: ' + stopDate);
        if (stopDate.getTime() < dt.getTime()) {
            iBTVMstopDate = americanDate(dt);
            iBTVMscheduled = false;        
            if (dolog) console.log('iBTVMstopDate: ' + iBTVMstopDate);
        }        
    }        
    iBTVMinitialized = true;
}

//-----------------------------------------------------------------------------

function iBTVMdiscover(devicesId, callback) {
    if (!usingBrowser()) {
        activityStart("Discovering iBTVM devices","please wait...");
        
        function iBTVMdiscoveryResult(devs){
            
            if (devs === '') {
                // no point in updating result to a blank - leave prior result in place
                activityStop(500);                 
                Toast.extendedshow('No devices discovered');
                return;
            }
            
            iBTVMdiscovery = JSON.stringify(devs);
            if (iBTVMdiscovery) iBTVMdevs = eval(iBTVMdiscovery);
            
            if (dolog) console.log('iBTVMdiscovery: ' + iBTVMdiscovery);
            setLocalItem("iBTVMdiscovery", iBTVMdiscovery);
            activityStop(500);                 
            
            if (iBTVMdiscovery && (!iBTVMname || !iBTVMaddress)) {
                if (iBTVMdevs.length > 0) {
                    iBTVMname = iBTVMdevs[0].name;        
                    iBTVMaddress = iBTVMdevs[0].address;
                }
            }
            
            if (devicesId) {
                var txt = getDiscoveryHtml(devs);            
                $('#' + devicesId).html(txt);      
                $('#' + devicesId).val(iBTVMaddress);
                $('#' + devicesId).selectmenu('refresh', true);
                $('#' + devicesId).trigger("change");                
            }
            if (callback) callback();
        }

        function iBTVMdiscoveryError(err) {
            activityStop(500);                 
            Toast.extendedshow(err); 
            if (dolog) console.log(err);        
        }
        
        navigator.iBTVM.listDevices(null,            
            function(r){iBTVMdiscoveryResult(r)},
            function(e){iBTVMdiscoveryError(e)}
        );
    }
}

//-----------------------------------------------------------------------------


function iBTVMconnectError(err, fail) {
    activityStop(500);                 
    Toast.extendedshow(err); 
    if (dolog) console.log(err);
    connectedTo = "";        
    getPending = false;    
    if (dolog) console.log("Connect Error 1");
    if (fail !== undefined) {
        if (dolog) console.log("Connect Error 2");
        fail("Connect Error");                
    }
}        

function iBTVMConnect(device, fail) {
    if (device === connectedTo) return;
    if (!device) return;
    if (demoMode()) return;
    
    function iBTVMconnectResult(json) {
        activityStop(500);                 
        if (dolog) console.log(JSON.stringify(json));        
        connectedTo = device;                
    }
    activityStart("Connecting to iBTVM device","please wait...");                      
    navigator.iBTVM.connect(device,            
        function(r){iBTVMconnectResult(r)},
        function(e){iBTVMconnectError(e, fail)}
    );    
}

function getValuesItem(values, value) {
    var item = jQuery.grep(values, function( n, i ) {
        return ( n.val === value );
    })[0];
    return item;
}

function getACrejectItem(value) {
    return getValuesItem(iBTVMacrejectValues, value);    
}

function getRangeItem(value) {
    return getValuesItem(iBTVMrangeValues, value);    
}

function formatVoltage(val, item, raw) {

    var rez;
    if (val === '' || val === undefined) {
        rez = 'NONE'
        return rez;
    }
    
    if (!item) {
        item = getRangeItem(_rangeVal);
    }
    
    if (isNaN(val)) {
        //if (dolog) console.log('OVER: ' + val);
        rez = 'OVER';
    } else {
        rez = parseFloat(val);
        if (item.lbl == 'mVDC') {
            if (!raw) rez = (rez * 1000).toFixed(item.dp);
            else rez = rez.toFixed(item.dp+3); 
        } else {
            rez = rez.toFixed(item.dp);
        }                
    }
    return rez;
}

function getVoltageText(r) {
    var res = {};
    
    var item = getRangeItem(_rangeVal);

    res.vOn = '';
    res.vOff = '';
    res.uOn = '';
    res.uOff = '';
    var nbsp = '&#160';            
    
    res.vOn = formatVoltage(r.voltsOn);
    if ($.isNumeric(res.vOn)) res.uOn = item.lbl;
    res.vOn += nbsp; // add to end of text to clear occasional italic turd left on display
    
    if (r.voltsOff !== undefined) {
        res.vOff = formatVoltage(r.voltsOff);
        if ($.isNumeric(res.vOff)) res.uOff = item.lbl;
        res.vOff += nbsp;
    }
    return res;
}

function formatVoltages(r, raw, item) {

    if (!item) {
        item = getRangeItem(_rangeVal);
    }
    
    var html;
    var vOn = formatVoltage(r.voltsOn, item, raw);                       
    if (r.voltsOff !== undefined) {
        var vOff = formatVoltage(r.voltsOff, item, raw);
        html = vOn + ' | ' + vOff;
        if (!raw && ($.isNumeric(vOn) || $.isNumeric(vOff))) html += ' ' + item.lbl;
    } else {
        html = vOn;
        if (!raw && $.isNumeric(vOn)) html += ' ' + item.lbl;
    }
    return html;
}

function formatOnVoltage(r, raw, item) {

    if (!item) {
        item = getRangeItem(_rangeVal);
    }
    
    var html;
    var v = formatVoltage(r.voltsOn, item, raw);                           
    html = v;
    if (!raw && $.isNumeric(v)) html += ' ' + item.lbl;
   
    return html;
}

function formatOffVoltage(r, raw, item) {

    if (!item) {
        item = getRangeItem(_rangeVal);
    }
    
    var html;
    var v = formatVoltage(r.voltsOff, item, raw);                           
    html = v;
    if (!raw && $.isNumeric(v)) html += ' ' + item.lbl;
   
    return html;
}


function formatGPS(r, short) {
    var html;

    if (!r.gpsFixType || r.gpsFixType == 'none') {
        if (short) html = '';
        else html = 'No Fix';                
    } else {
        var dt = new Date(r.lastGPS);   
        var tm = new Date();                
        if (tm.getTime() - dt.getTime() > 1000 * 10) {
            r.gpsFixType = 'none';                    
        }        
        var ft = r.altitude * 3.28084;        
        var nbsp = '&#160';        
        
        if (short) {
            html = r.latitude.toFixed(6) + ',' + r.longitude.toFixed(6) + ',' + ft.toFixed(0) + ',' + r.gpsFixType;                                 
        }
        else if (r.gpsDate) {   
            var gpsDt = new Date(r.gpsDate)
            var gpsTms = gpsDt.toLocaleTimeString();    // time from GPS unit itself
            
            
//            html = 'Lat: ' + r.latitude.toFixed(6) + nbsp+nbsp + 'Lon: ' + r.longitude.toFixed(6) + nbsp+nbsp + 'Alt: ' + ft.toFixed(0) + ' ft' + '<br>' + 'Time: ' + gpsTms + nbsp+nbsp + 'Fix Quality: ' + r.gpsFixType;
            // A'sa Andrew the html went accross 4 verticle lines, the smallest i could make it was 3 so i added <br> tags appropriately to make the GPS data easier to read
            html = 'Lat: ' + r.latitude.toFixed(6) + nbsp+nbsp + 'Lon: ' + r.longitude.toFixed(6) + nbsp+nbsp + '<br>Alt: ' + ft.toFixed(0) + ' ft' + ' ' + 'Time: ' + gpsTms + nbsp+nbsp + '<br>Fix Quality: ' + r.gpsFixType;

        } else {
            var tms = dt.toLocaleTimeString();      // time from tablet when GPS was received
            html = 'Lat: ' + r.latitude.toFixed(6) + nbsp+nbsp + 'Lon: ' + r.longitude.toFixed(6) + nbsp+nbsp + 'Alt: ' + ft.toFixed(0) + ' ft' + '<br>' +
                   'Time: ' + tms + nbsp+nbsp + 'Fix Quality: ' + r.gpsFixType;
        }
                
    }
    return html;
}

var priorRead = -1.7;
var midVal = undefined;
function getRandomRead() {
    var r;
    if (!midVal) r = 0.2;
    else r = Math.abs(midVal) / 25; 
    
    priorRead = (priorRead + Math.random() * r - r/2); 
    return priorRead;    
}

function iBTVMgetReading(success, fail, reqTime, logFreq) {
    if (demoMode()) {
        var r = {voltsOn: getRandomRead()};
        if (iBTVMstyleVal === 'on/off' || iBTVMstyleVal === 'max/min') {        
            r.voltsOff = r.voltsOn * 0.8;
        }
        r.onTime = new Date().getTime();
        // r.noComplex = (iBTVMstyleVal === 'on/off') && ((new Date().getSeconds()) % 6 < 3);                
        r.noComplex = false; // dont need to fake this in demo mode now that display is working
        success(r);
        return;
    }
    navigator.iBTVM.getReading(reqTime, success, fail, logFreq);    
}

var priorBattery = 4.2;
function iBTVMgetStatus(doSendCmd, success, fail) {
    if (demoMode()) {
        if (success) {
            var r = {battery: priorBattery};
            priorBattery = priorBattery - 0.1;
            if (priorBattery < 0) priorBattery = 4.2;                
            success(r);
        }
    } else {
        navigator.iBTVM.getStatus(doSendCmd, success, fail);            
    }
}

function getCycles() {
   var cycleOn = 0, cycleOff = 0;
   if (iBTVMstyleVal === 'on/off' || iBTVMstyleVal === 'max/min') {
       cycleOn = iBTVMcycleOn;
       cycleOff = iBTVMcycleOff;            
   } else if (iBTVMstyleVal === 'single/predom') {
       cycleOn = iBTVMpredom * 1000;
   }
   return {CycleOn: cycleOn, CycleOff: cycleOff};
}


function _iBTVMgetVoltage(device, success, fail) {   
    if (demoMode()) return;
    if (!device) return;
    
    
    function _fail(err) {
        activityStop(500);                 
        Toast.extendedshow("Error getting voltage: " + err);
        if (err.indexOf("before connect") != -1) {
            connectedTo = "";                    
        }
    };
    
    function iBTVMconnectResult(json) {
        activityStop(500);                 
        if (dolog) console.log(JSON.stringify(json));        
        connectedTo = device;        
        if (getPending) {
            getPending = false;
            setTimeout(function(){
                _iBTVMgetVoltage(device, success, fail);
            }, 500);                                     
        }
    }    
    if (dolog) console.log("Getting iBTVM voltage . . .");
    if (connectedTo !== device) {
        getPending = true;       
        activityStart("Connecting to iBTVM device","please wait...");                      
        navigator.iBTVM.connect(device,            
            function(r){iBTVMconnectResult(r)},
            function(e){iBTVMconnectError(e, fail)}
        );                
    } else {
        activityStart("Getting iBTVM voltage","please wait...");
        if (fail === undefined) fail = _fail;    
        var cycles = getCycles();        
        navigator.iBTVM.updateConfig(
            {
                range: iBTVMrangeVal, 
                acreject: iBTVMacrejectVal, 
                readstyle: iBTVMstyleVal, 
                CycleOn: cycles.CycleOn, 
                CycleOff: cycles.CycleOff 
            },
            function(r) {
                _rangeVal = iBTVMrangeVal;
                iBTVMconfig = r;
                navigator.iBTVM.getVoltage(null, success, fail);                        
            }, fail
        );                                 
    };                   
};

function iBTVMgetVoltage(iBTVMid) {

    if (demoMode()) {
        var r = {voltsOn: 1.234};
        success(r);
        return;
    }
    
    function success (r) {
        activityStop(500);                 
        if (dolog) console.log("Got iBTVM voltage for " + iBTVMid + ": " + JSON.stringify(r));               
        var html;        
        if ($('#' + iBTVMid).is("input")) {
            html = formatVoltages(r, true);     // just show raw voltage without labels
            $('#' + iBTVMid).val(html);                            
        } else {
            html = formatVoltages(r, false);    // format according to range
            $('#' + iBTVMid).html(html);                                        
        }                        
        
        if (typeof(model) !== "undefined") {
            model[iBTVMid] = ko.observable(r.voltsOn);
            model.dirty = ko.observable(true);
        }                     
    };    
    if (!iBTVMaddress) {
        alert("No iBTVM device selected");
        return;
    } 
    _iBTVMgetVoltage(iBTVMaddress, success);    
}

function getFrequency() {
    var freq;
    if (iBTVMstyleVal === 'on/off' || iBTVMstyleVal === 'max/min') {
        freq = parseFloat(iBTVMcycleOn) + parseFloat(iBTVMcycleOff);                
    } else {
        freq = iBTVMfreq;
        if (iBTVMfreqUnits === "sec") freq *= 1000;
        else if (iBTVMfreqUnits === "min") freq *= 1000*60;
        else if (iBTVMfreqUnits === "hr") freq *= 1000*60*60;
    }       
    freq = Math.round(freq);       // should be safe to test for equality
    
//    console.log("this will be at the bottom: " + freq);
    return freq;
}

function getMeterFrequency() {
    var freq = getFrequency();
    
//    return freq;

    var twentyIsNotIdenticalToZero = (freq % 20 !== 0);
    var fiftyIsIdenticalToZero = (freq % 50 === 0);
    
    // Andrew Look at this bug. Causing issues on 500ms being interpreted at 50ms
    if ( twentyIsNotIdenticalToZero && fiftyIsIdenticalToZero ) {
        
        console.log("Logging the frequency: returning 50 when frequency is: " + freq);
        console.log("Logging the conditions; first: " + twentyIsNotIdenticalToZero + "; second: " + fiftyIsIdenticalToZero);
        return 50;
        
    } else {
        
        console.log("Logging the frequency: returning 20 when frequency is: " + freq);
        console.log("Logging the conditions; first: " + twentyIsNotIdenticalToZero + "; second: " + fiftyIsIdenticalToZero);
        return 20;
        
    }  
}

function _iBTVMstart(device, success, fail, quiet) {   
    if (!device) return;
    if (demoMode()) return;
    
    function _fail(err) {
        activityStop(500);                 
        Toast.extendedshow("Error starting: " + err);
        if (err.indexOf("before connect") != -1) {
            connectedTo = "";                    
        }
    };    
    function iBTVMconnectResult(json) {
        activityStop(500);                 
        if (dolog) console.log(JSON.stringify(json));        
        connectedTo = device;        
        if (getPending) {
            getPending = false;
            setTimeout(function(){
                _iBTVMstart(device, success, fail, quiet);
            }, 500);                                     
        }
    }    
    if (dolog) console.log("Starting iBTVM . . .");
    if (connectedTo !== device) {
        getPending = true;       
        if (!quiet) activityStart("Connecting to iBTVM device","please wait...");                      
        navigator.iBTVM.connect(device,            
            function(r){iBTVMconnectResult(r)},
            function(e){iBTVMconnectError(e, fail)},
            false   // don't get config as we're going to immdediately set it
        );                
    } else {
        if (!quiet) activityStart("Starting iBTVM","please wait...");
        if (fail === undefined) fail = _fail;    
        var cycles = getCycles();        
        navigator.iBTVM.updateConfig(
            {
                range: iBTVMrangeVal, 
                acreject: iBTVMacrejectVal, 
                readstyle: iBTVMstyleVal, 
                CycleOn: cycles.CycleOn, 
                CycleOff: cycles.CycleOff 
            },                
            function(r) {
                _rangeVal = iBTVMrangeVal;
                iBTVMconfig = r;
                // Might as well go as fast as possible in all modes, with either 20/25ms cycle depending on logging speed
                meterFreq = getMeterFrequency();
                                     if (dolog) console.log("Frequency is: " + meterFreq);
                if (dolog) console.log("navigator.iBTVM.start");
                navigator.iBTVM.start(meterFreq, success, fail, true);                 
            }, fail, true       // forceConfig
        );                                         
    };                   
};

function iBTVMstart(spoof, success, fail, quiet) {
       
    if (demoMode()) {
        _rangeVal = iBTVMrangeVal;
        // pick random value in middle of range
        var item = getRangeItem(_rangeVal);
        midVal = item.demo; 
        priorRead = item.demo;        
        
        if (success !== undefined) success();        
        return;
    }
    
    function _success (res) {
        activityStop(500);                 
        if (success !== undefined) success();        
    };    
    if (!iBTVMaddress) {
        alert("No iBTVM device selected");
        return;
    }
    
    _iBTVMstart(iBTVMaddress, _success, fail, quiet);    
}

function _iBTVMstop(success, fail) {   
    if (demoMode()) {
        if (success !== undefined) success();        
        return;
    }
    
    function _fail(err) {
        activityStop(500);                 
        Toast.extendedshow("Error stopping: " + err);
    };    
    if (dolog) console.log("Stopping iBTVM . . .");
    if (fail === undefined) fail = _fail;    
    if (connectedTo) {
        //activityStart("Stopping iBTVM","please wait...");     // this is annoying                
        navigator.iBTVM.stop(null, success, fail);            
    };                   
};

function iBTVMstop(success) {
    
    function _success (res) {
        activityStop(500);                 
        if (success !== undefined) success();        
    };        
    _iBTVMstop(_success);    
}

function iBTVMrestart() {   
    
    function fail(err) {
        activityStop(500);                 
        Toast.extendedshow("Error restarting: " + err);
    };
    function success (res) {        
        setTimeout(function(){
            _iBTVMstart(iBTVMaddress, function(r) {
                activityStop(500);
            }, fail, true);                    
        }, 500);       // meter seems to need about a second to settle else ignores start                                                     
    };    
    
    if (connectedTo && !demoMode()) {
        if (dolog) console.log("Restarting iBTVM . . .");
        activityStart("Restarting iBTVM","please wait...");                
        navigator.iBTVM.stop(null, success, fail);            
    } else {
        iBTVMstart();        
    }                   
};


function _iBTVMdiagnostic(device, success, fail) {   
    if (!device) return;
    if (demoMode()) {
        var json = {};
        success(json);
        return;
    }
    
    function _fail(err) {
        activityStop(500);                 
        Toast.extendedshow("Error with diagnostic: " + err);
        if (err.indexOf("before connect") != -1) {
            connectedTo = "";                    
        }
    };    
    function iBTVMconnectResult(json) {
        activityStop(500);                 
        connectedTo = device;        
        if (getPending) {
            getPending = false;
            setTimeout(function(){
                _iBTVMdiagnostic(device, success, fail);
            }, 500);                                     
        }
    }    
    if (dolog) console.log("Performing Diagnostics . . .");
    if (connectedTo !== device) {
        getPending = true;       
        activityStart("Connecting to iBTVM device","please wait...");                      
        navigator.iBTVM.connect(device,            
            function(r){iBTVMconnectResult(r)},
            function(e){iBTVMconnectError(e, fail)}
        );                
    } else {
        activityStart("Performing Diagnostics","please wait...");        
        if (fail === undefined) fail = _fail;    
        navigator.iBTVM.diagnostic(null, success, fail);                        
    };                   
};

function iBTVMdiagnostic(success, fail) {
    
    function _success (json) {
        activityStop(500);                 
        if (success !== undefined) success(json);        
    };    
    function _fail (err) {
        activityStop(500);                 
        if (fail !== undefined) fail(err);        
    };    
    if (!iBTVMaddress) {
        alert("No iBTVM device selected");
        return;
    }
    
    _iBTVMdiagnostic(iBTVMaddress, _success, _fail);    
};







