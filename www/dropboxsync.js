var dropbox_sync = function () {
};

dropbox_sync.prototype.init = function(callback) {
	self = this;
	if (dolog) console.log("dropbox_sync.init called");
	return this.checkIsLinked(function(obj) {
		if (dolog) console.log("checkIsLinked returned "+obj.isLinked);
		if (obj !== undefined) {
			self.linkedAccount = true;
		};
		if  (callback !== undefined) callback();
	});
};

dropbox_sync.prototype.isLinked = function(completed) {
    //if (dolog) console.log("dropbox_sync.isLinked called");
    self = this;
    
    return cordova.exec(function(obj) {
                        
                       if ( obj == "OK" ) {
                            this.linkedAccount = true;
                            completed(true);
                       } else {
                            this.linkedAccount = false;
                            completed(false);
                       }
                        }, function(obj) {
                        
                        if ( obj == "OK" ) {
                                this.linkedAccount = true;
                                completed(true);
                        } else {
                                this.linkedAccount = false;
                                completed(false);
                        }
                        
                        }, 'DropBoxSyncPlugin', 'isLinked', []);
    
//	return this.linkedAccount;
};

dropbox_sync.prototype.checkIsLinked = function(cbSuccess,cbError) {
	//success has object like { isLinked: true }
    return cordova.exec(cbSuccess, cbError, 'DropBoxSyncPlugin', 'isLinked?', []);
};

dropbox_sync.prototype.linkAccount = function(cbSuccess,cbError) {
    self = this;
	//success has object like { isLinked: true }
	//note that it is possible to get success with isLinked=false which is probably a user decline
    return cordova.exec(function(obj) {        
            // need to update internal link status else calls to isLinked() will return false until next init() or checkIsLinked()
            console.log("dropbox_sync.linkAccount returned: " + JSON.stringify(obj));
            if (obj !== undefined) {
                self.linkedAccount = true;
            };        
            if (cbSuccess !== undefined) {
                cbSuccess(obj);
            }
        }, cbError, 'DropBoxSyncPlugin', 'linkAccount', []);
};

dropbox_sync.prototype.unlinkAccount = function() {
	this.linkedAccount = false;
	return cordova.exec(null,null,'DropBoxSyncPlugin', 'unlinkAccount', []);
};

dropbox_sync.prototype.listFiles = function(cbSuccess,cbError,path,name) {
	//success returns an object like:
	//   {
	//      folder: "/",
	//      fileCount: 2,
	//      [
	//           {
	//               name: "Dig Folder #1",
	//               size: 0,
	//               modifiedDate: <datetime string>,
	//               isFolder: true
	//           },
	//           {
	//               name: "fancy.mpg",
	//               size: 1254321,
	//               modifiedDate: <datetime string>,
	//               isFolder: false
	//           }
	//       ]
	//   }
	if (path !== undefined) {
		//first char must be '/'
		return cordova.exec(cbSuccess, cbError, 'DropBoxSyncPlugin', 'listFiles', [path, name]);
	}
	//list root of application folder
	return cordova.exec(cbSuccess, cbError, 'DropBoxSyncPlugin', 'listFiles', ['/', name]);
};

dropbox_sync.prototype.createFolder = function(path,cbSuccess,cbError) {
   return cordova.exec(cbSuccess, cbError, 'DropBoxSyncPlugin', 'createFolder', [path]);
};

dropbox_sync.prototype.ensureFolderExists = function(path,cbSuccess,cbError) {
	   return cordova.exec(cbSuccess, cbError, 'DropBoxSyncPlugin', 'ensureFolderExists', [path]);
};

dropbox_sync.prototype.deleteFolderOrFile = function(path,cbSuccess,cbError) {
	//deleting a folder is recursive, will not return an error if file does not exist
    return cordova.exec(cbSuccess, cbError, 'DropBoxSyncPlugin', 'deleteFolderOrFile', [path]);
};

dropbox_sync.prototype.fileOrFolderExists = function(path,cbSuccess,cbError) {
	//deleting a folder is recursive
    return cordova.exec(cbSuccess, cbError, 'DropBoxSyncPlugin', 'fileOrFolderExists?', [path]);	
};

dropbox_sync.prototype.addFile = function(path,overwriteExisting,cbSuccess,cbError,dbxName) {
	//dbxName is optional - if left undefined, file will have same name on dropbox and source file will be removed
	//                      if defined, then file will have the name provided and source file will NOT be removed
	if (dbxName !== undefined) {
		return cordova.exec(cbSuccess, cbError, 'DropBoxSyncPlugin', 'addFile', [path, overwriteExisting, dbxName]);
	}
	return cordova.exec(cbSuccess, cbError, 'DropBoxSyncPlugin', 'addFile', [path, overwriteExisting]);
};

dropbox_sync.prototype.getFile = function(src,cbSuccess,cbError) {
	//NOTE: that this will return an error if a new version of the file is being downloaded... so keep trying if you get
	//      an error like "Synchronization of <thing> in progress: byte <x> of <y>".
	return cordova.exec(cbSuccess, cbError, 'DropBoxSyncPlugin', 'getFile', [src]);
};

navigator.dropbox_sync = new dropbox_sync();
