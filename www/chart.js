var plot = undefined;
var chartData = [];
var series = [];
var maxPoints;
var container;
var chartPending = false;
var chartFreq = undefined;
var logStart = undefined;
var logStop = undefined;

function clearChart(justMarkings) {
    if (dolog) console.log('clearChart(' + justMarkings + ')');
    if (!justMarkings) {
        chartData.length = 0;        
    }    
    logStart = undefined;
    logStop = undefined;            
}

function getSeriesData(chartData, offVal)
{
    //if (dolog) console.log("getSeriesData");

    maxPoints = Math.round(container.outerWidth() / 2) || 300;
    //if (dolog) console.log('maxPoints = ' + maxPoints);
    
    var res = [];    
    var idx = chartData.length-maxPoints < 0 ? 0 : chartData.length-maxPoints;    
    for (var i = idx; i < chartData.length; ++i) {
        if (chartData[i][0] == null) {
            res.push(null);                 
        } else {
            if (offVal) {
                res.push([chartData[i][0], chartData[i][2]]);     // [time, value]                
            } else {
                res.push([chartData[i][0], chartData[i][1]]);     // [time, value]
            }
        }
    }   
    return res;
}

var dtUpdate = undefined;

function updateChart(forceUpdate, freq, _chartData, logData, updateEvery) {
    var dt = new Date();
    // maximum 1 update per 3/4s
    if (!updateEvery) {
        updateEvery = 750;
    }
    if (!forceUpdate && (dtUpdate && dt.getTime() - dtUpdate.getTime() < updateEvery)) { 
        return;
    }    
    //if (dolog) console.log("updateChart");
    
    if (!_chartData) {
        _chartData = chartData;
    }    
    
    if (true || plot === undefined || (freq && chartFreq != freq)) {
        chartFreq = freq;
        initChart(freq, _chartData, logData);
    } else {    
        series[0].data = getSeriesData(_chartData);
        plot.setData(series);
        plot.resize();
        plot.setupGrid();
        plot.draw();
    }
    dtUpdate = dt; 
    
    chartPending = false;    
}

function getDataInfo(data) {
    var res = {
        neg: false,
        mv: false
    };
    if (data.length < 1) return res;
    
    var frstNeg = (data[0][1] < 0);
    var negCnt = 0;
    var posCnt = 0;
    
    res.mv = true;        
    for (var i = 0; i < data.length; i++) {
        if (data[i][1]) {
            if (data[i][1] < 0) negCnt++;
            else if (data[i][1] > 0) posCnt++;
            
            if (Math.abs(data[i][1]) > 0.1) res.mv = false;            
        }        
    }
    if (frstNeg) {
        res.neg = negCnt*3 > posCnt;
    } else {
        res.neg = negCnt > posCnt*3;
    }         
    
    return res;    
}

function valsIndicateNeg(data) {
    if (data.length < 1) return false;
    
    var frstNeg = (data[0][1] < 0);
    var negCnt = 0;
    var posCnt = 0;
    
    for (var i = 0; i < data.length; i++) {
        if (data[i][1]) {
            if (data[i][1] < 0) negCnt++;
            else if (data[i][1] > 0) posCnt++;
        }        
    }
    if (frstNeg) {
        return negCnt*3 > posCnt;
    } else {
        return negCnt > posCnt*3;
    }         
}

function initChart(freq, chartData, logData) {
    //if (dolog) console.log("initChart");     
        
    // Determine how many data points to keep based on the placeholder's initial size;
    // this gives us a nice high-res plot while avoiding more than one point per pixel.
    container = $("#placeholder");          
    maxPoints = Math.round(container.outerWidth() / 2) || 300;
    
    var data = getSeriesData(chartData);
    var dataOff = getSeriesData(chartData, true);
    
    var info = getDataInfo(data);
        
    if (info.neg || info.mv) {
        for (var i = 0; i < data.length; i++) {            
            if (data[i][1]) {
                if (info.neg) data[i][1] = -data[i][1];
                if (info.mv)  data[i][1] = data[i][1] * 1000;
            }
            if (dataOff[i][1]) {
                if (info.neg) dataOff[i][1] = -dataOff[i][1];
                if (info.mv)  dataOff[i][1] = dataOff[i][1] * 1000;
            }
        }
    }    
    
    series = [{
        data: data,
        lines: { show: true }, 
        points: { show: true, radius: 1 },
        shadowSize: 0,
        color: "#003399"
    }, 
    {
        data: dataOff,
        lines: { show: true }, 
        points: { show: true, radius: 1 },
        shadowSize: 0,
        color: "#009933"
    }];
    
    var atick, aunit;
    
    // use range of chart data rather than frequency of readings to determine tick size
    if (data.length < 2) {
        atick = undefined; aunit = "";                 
    } else {
        var dt0 = new Date(data[0][0]);
        var dt1 = new Date(data[data.length-1][0]);
        var sec = Math.round((dt1.getTime() - dt0.getTime()) / 1000);
                
        if (sec < 1) {
            atick = 500; aunit = "millisecond";                    
        } else {
            atick = Math.round(sec / 3); aunit = "second";                                            
        }        
        var w = container.outerWidth() / 300;
        if (w > 0) atick = Math.round(atick / w);        
    }
      
    
    var markings = [];
    if (logData && logData.length > 1) {
        logStart = logData[0][0];
        logStop = logData[logData.length-1][0];
    }

    if (logStop && data.length > 1 && logStop > data[0][0]) {
        
        if (logStart < data[0][0]) {
            markings = [{xaxis: {from: data[0][0], to: logStop}, color: "#FCEDA7"}];
        } else {
            markings = [{xaxis: {from: logStart, to: logStop}, color: "#FCEDA7"}];                
        } 
    }
        
    
    plot = $.plot(container, series,
        {
            grid: {
                borderWidth: 1,
                minBorderMargin: null,     // 20
                labelMargin: 10,
                backgroundColor: {
                    colors: ["#fff", "#e4f4f4"]
                },
                margin: {
                    top: 8,
                    bottom: 0,      // 20
                    left: 20
                },
                markings: markings
            },
            xaxis: {
                mode: "time",
                timeformat: "%H:%M:%S",
                timezone: "browser",
                minTickSize: [atick, aunit]
            },
            legend: {
                show: false
            }
            
        }
    );
    
    // Create the X and Y axis labels

    var label = "Voltage ";
    var units = "V";
    if (info.mv) units = "mV";    
    if (info.neg) units = "-" + units;
    label += "(" + units + ")";        
    
    var yaxisLabel = $("<div class='axisLabel yaxisLabel'></div>")
    .text(label)
    .appendTo(container);
    
    // Since CSS transforms use the top-left corner of the label as the transform origin,
    // we need to center the y-axis label by shifting it down by half its width.
    // Subtract 20 to factor the chart's bottom margin into the centering.    
    yaxisLabel.css("margin-top", yaxisLabel.width() / 2 - 20);

}
