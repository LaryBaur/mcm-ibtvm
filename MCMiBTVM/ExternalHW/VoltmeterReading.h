//
//  VoltmeterReading.h
//  ProCPNavPad
//
//  Created by Mark on 5/14/13.
//
//

#import <Foundation/Foundation.h>
#import "JSON.h"

@interface VoltmeterReading : NSObject
{
    NSString * _config;
    NSString * _gpsFixType;
      
    double _voltsOn;
	double _voltsOff;
	double _readTime;
	double _onTime;
	double _offTIme;
	double _latitude;
	double _longitude;
	double _altitude;
	int _pdop;
	int _hdop;
	int _vdop;
	int _satsUsed;
	
}

- (NSString *)signifDigits:(double)von voff:(double)voff digits:(int)digits;
- (NSString *)toDisplayString;

@end
