//
//  SignalLock.h
//  iProCPVolts
//
//  Created by Mark on 8/21/13.
//
//

#import <Foundation/Foundation.h>


// for synchronizing but not sure, we may not need these
/**
 * @category Utility Classes and Functions
 */



@interface jsignal : NSObject
{
    NSObject *Done;
    BOOL isDone;
    
}

- (void)doWait;
- (void)doNotify;

@end
