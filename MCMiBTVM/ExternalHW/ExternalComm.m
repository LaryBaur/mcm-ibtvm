//
//  ExternalComm.m
//  ProCPNavPad
//
//  Created by Mark on 5/3/13.
//
//

#import "ExternalComm.h"
#import "AppDelegate.h"


@implementation ExternalComm

@synthesize btFailed;
@synthesize btReconnect;

@synthesize eaSessionController;
@synthesize device;
@synthesize deviceProtocol;


#pragma mark ======== init method =========
/*
 ** --------------------------------------------------------
 **  init method
 ** --------------------------------------------------------
 */
- init
{
	if (self = [super init])
	{
//		CMDLOG;
        eaOpen = NO;
        eaSessionController = nil;
        btFailed = NO;
        incomingPackets = [[NSMutableArray alloc] initWithCapacity:20];
        outgoingPackets = [[NSMutableArray alloc] initWithCapacity:20];
        readMutex = [[NSRecursiveLock alloc] init];
        writeMutex = [[NSRecursiveLock alloc] init];
        btCommand = @"";
        
	}
	return self;
}

/*
 
 we cant get device class from External Accessory.
 
 connected:YES
 connectionID:520950272
 name: MCM-iBTVM-9999
 manufacturer: Roving Networks
 modelNumber: RN-42-APL-X
 serialNumber:
 firmwareRevision: 1.2.3
 hardwareRevision: 4.5.6
 macAddress: 00:06:66:08:BF:AC
 protocols: (
 "com.RovingNetworks.btdemo"
 
 */



- (NSArray*)listDevices:(NSString *)prefix :(NSInteger)param
{
//    CMDLOG;
    
    NSArray *versionCompatibility = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    int iOSversion = [[versionCompatibility objectAtIndex:0] intValue];
    
   
    NSArray *accessories = [[EAAccessoryManager sharedAccessoryManager] connectedAccessories];
    
    if (!accessories)
        return nil;
    
    NSMutableArray *devicesFound = [NSMutableArray array];
    
    for (EAAccessory *obj in accessories)
    {
        //cheating a bit here and assume its the first device found that we are using.
        device = obj;
        
        NSString *macAddress = @"Not available before iOS 6";

        if (iOSversion >= 6)
            macAddress = [obj valueForKey: @"macAddress"];
            
//        NSLog(@"mac address: %@", macAddress);
        
        NSString *name = [obj valueForKey:@"name"];
//        NSLog(@"name: %@", name);
        
        //    NSString *result = @"[ { 'name' : 'MCM-iBTVM-9999' ,'address' : '00:06:66:08:BF:AC' ,'class' : '1796' }] ";

//        NSLog([obj description],nil);
        
        /* what we actually get on the iOS side of things
         connected:YES
         connectionID:28313395
         name: MCM-iBTVM-6001
         manufacturer: Roving Networks
         modelNumber: RN-42-APL-X
         serialNumber:
         firmwareRevision: 1.2.3
         hardwareRevision: 4.5.6
         protocols: (
         "com.mcmiller.protocol.voltmeter"
         )
         */
        
        //and more cheating assuming the first protocol listed is the one we're using (to assume makes an ASS out of U and ME)
        deviceProtocol = [[device protocolStrings] objectAtIndex:0];
        
        NSMutableDictionary *dev = [NSMutableDictionary dictionary];
        
        [dev setObject:name             forKey:@"name"];
        [dev setObject:macAddress       forKey:@"address"];
        [dev setObject:deviceProtocol   forKey:@"class"];
        
        [devicesFound addObject:dev];
    
    }
    
    [self connectSocket:device];
    
    return devicesFound;
    
}


- (NSString *)sendCmd:(NSString *)address command:(NSString *)command
{
//    CMDLOG;
    
    if (![self connectSocket:device])       // device = EAAccessory iBTVM
    {
        return btError;
    }
    if (![self writeSocket:command])
    {
        return btError;
    }
    return  @"";
}

- (BOOL)send:(NSString *)command
{
//    CMDLOG;
    
    if (![self connectSocket:device])       // device = EAAccessory aka iBTVM
    {
        return NO;
    }
    
    return [self writeSocket:command];
}



- (NSString *)getResp:(NSString *)address command:(NSString *)command header:(NSString *)header timeout:(int)timeout
{
//    CMDLOG;
    
    btError = @"ERROR";
    
    NSString * result = @"";
    if (![self connectSocket:device])       // device = EAAccessory
    {
        return btError;
    }
      
    result = [self getIncoming:header timeout:timeout];
    return result;
}

- (NSString *)sendCmdGetResp:(NSString *)address command:(NSString *)command header:(NSString *)header timeout:(int)timeout
{
//    CMDLOG;
    
    btError = @"ERROR";
    
    NSString * result = @"";
    if (![self connectSocket:device])       // device = EAAccessory
    {
        return btError;
    }
    if (![self writeSocket:command])
    {
        return btError;
    }
    
    result = [self getIncoming:header timeout:timeout];
    return result;

}

- (BOOL)connectSocket:(EAAccessory *)accessory
{
//    CMDLOG;
    
//  if (btReconnect || (btSocket == null) || (btDevice == null) || (!btDevice.equals(device)) || (btUUID == null) || (!btUUID.toString().equalsIgnoreCase(uuid)))
    
    if (!eaOpen)
    {
        eaSessionController = [EADSessionController sharedController];
        [eaSessionController setupControllerForAccessory:accessory withProtocolString:deviceProtocol];
        eaOpen = [eaSessionController openSession];
    }
    
    return eaOpen;
}

- (NSString*)getCommand:(NSString *)command
{
//    CMDLOG;
    
    if ([command length] == 0) {
        command = btCommand;
    } else {
        btCommand = command;
    }
    return command;
}


- (BOOL)writeSocket:(NSString *)command
{
//    CMDLOG;
    
    btError = @"";
    
    command = [self getCommand:command];
    NSString *cmd = command;
    
//    NSLog(@"writeSocket: %@", cmd);
    
    if (!eaOpen) {
        btError = @"Write to bluetooth socket before its set up"; //need at least the device
        return false;
    }
    
    [writeMutex lock];
    @try {
        [outgoingPackets addObject:command];
        [eaSessionController _writeData];
    } @finally {
        [writeMutex unlock];
    }
    
    return true;
}

- (void)flushIncoming
{
//    CMDLOG;
    
    [readMutex lock];
    
    [incomingPackets removeAllObjects];
    
    [readMutex unlock];
}

/*
while (result.isEmpty() && System.currentTimeMillis() <= end) {
    readMutex.lock();
    try {
        if (!incomingPackets.empty() && (header.isEmpty() || incomingPackets.peek().startsWith(header))) {
            result = incomingPackets.pop();
            wait = false;
        } else {
            wait = true;
        }
    } finally {
        readMutex.unlock();
    }
    // wait *after* unlocking mutex
    if (wait) try {
        Thread.sleep(50);
    } catch(InterruptedException ex) {
        //
    }
}
 */

- (NSString *)getIncoming:(NSString *)header timeout:(int)timeout
{
//    CMDLOG;

    // NSTimeInterval is always specified in seconds; it yields sub-millisecond precision over a range of 10,000 years.
    
    NSTimeInterval ts = (NSTimeInterval)timeout / 1000.0;
    
    NSTimeInterval t = CACurrentMediaTime();
    NSTimeInterval end = t+ts;		// wait for result

    BOOL wait;
    
    gresult = @"";
    
    while (([gresult length] == 0) && (CACurrentMediaTime()  <= end))
    {
        [readMutex lock];
        @try {
//            if (!incomingPackets.empty() && (header.isEmpty() || incomingPackets.peek().startsWith(header))) {
            if ([incomingPackets count] && ( ([header length] == 0) || [[incomingPackets peek] hasPrefix:header])) {
                gresult = [incomingPackets pop];
                wait = false;
            } else {
                wait = true;
            }
        } @finally {
            [readMutex unlock];
        }
        // wait *after* unlocking mutex
        if (wait) @try {
//            [NSThread sleepForTimeInterval:0.050];           //Thread.sleep(50);
            [NSThread sleepForTimeInterval:0.300];           //Thread.sleep(300); A'sa Andrew

        } @catch (NSException *exception) {
//            NSLog(@"getIncoming exception name: %@ reason: %@", [exception name],[exception reason]);
        }

    }
    
    return gresult;
}

/*
 wait = true;
 
 if ([readMutex tryLock])
 {
 
 //      for (id object in array) {
 //        // do something with object
 //  }
 
 NSString *packet = [incomingPackets lastObject];
 NSLog(@"getincoming packet %@",packet);
 
 if ([packet hasPrefix:header])
 {
 gresult = [packet copy];
 [incomingPackets removeLastObject];
 wait = false;
 break;
 }
 
 [readMutex unlock];
 }
 
 if (wait)
 [NSThread sleepForTimeInterval:0.05];   // 50 milliseconds wait *after* unlocking mutex

- (NSString *)getIncoming:(NSString *)header timeout:(int)timeout
{
    CMDLOG;
 
    NSLog(@"header: %@, timeout %d",header,timeout);
 
    NSTimeInterval t = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval end = t+(timeout / 1000);		// wait for result

    NSString *result = @"";
    
    while (![result length] && ([[NSDate date] timeIntervalSince1970] <= end))
    {
    //    [readMutex lock];
        NSString *packet = [incomingPackets lastObject];
        NSLog(@"getincoming packet %@",packet);
        if ([packet hasPrefix:header])
            result = packet;
    }
      
    return result;
        
}
   */
    
- (void)disconnect
{
//    CMDLOG;
 
    if (eaOpen)
    {
        [[EADSessionController sharedController] closeSession];
        eaOpen = NO;
    }
}


@end
