/*
 
     File: EADSessionController.m
 Abstract: n/a
  Version: 1.1
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2010 Apple Inc. All Rights Reserved.
 
 
 */

#import "EADSessionController.h"
#import "AppDelegate.h"

#import "iBTVM.h"       //MSM TEMP!

NSString *EADSessionDataReceivedNotification = @"EADSessionDataReceivedNotification";

NSString *packetEnder = @"\r\n"; // documentation shows <CR> <LF>
NSString *packetBegin = @"@";


@implementation EADSessionController

@synthesize accessory = _accessory;
@synthesize protocolString = _protocolString;
@synthesize rawReadData;

#pragma mark Internal

// low level write method - write data to the accessory while there is space available and data to write
- (void)_writeData
{
//    CMDLOG;
//    NSLog(@"Sending packet data: %@", outgoingPackets);
    
    while ([outgoingPackets count])
    {
        NSString *s = [outgoingPackets objectAtIndex:0];
        [outgoingPackets removeObjectAtIndex:0];
        
        rawWriteData = [[NSMutableData alloc] initWithData:[s dataUsingEncoding:NSASCIIStringEncoding]];

        while (([[_session outputStream] hasSpaceAvailable]) && ([rawWriteData length] > 0))
        {
            NSInteger bytesWritten = [[_session outputStream] write:[rawWriteData bytes] maxLength:[rawWriteData length]];
            if (bytesWritten == -1)
            {
//                NSLog(@"write error");
                break;
            }
            else if (bytesWritten > 0)
            {
//                NSLog(@"rawWriteData bytesWritten %@ %d",rawWriteData, bytesWritten);
                [rawWriteData replaceBytesInRange:NSMakeRange(0, bytesWritten) withBytes:NULL length:0];
            }
        
        }
        
        [NSThread sleepForTimeInterval:1.0];
    }
    
//    NSLog(@"leaving _writeData");

}

// low level read method - read data while there is data and space available in the input buffer
- (void)_readData
{
//    CMDLOG;
    
    NSInteger bytesRead = 0;
    
#define EAD_INPUT_BUFFER_SIZE 512
    
    uint8_t buf[EAD_INPUT_BUFFER_SIZE];
    
    memset(buf, 0, sizeof(buf));    //for debugging clarity
    
//    while ([[_session inputStream] hasBytesAvailable])
    if ([[_session inputStream] hasBytesAvailable])
    {
         bytesRead = [[_session inputStream] read:buf maxLength:EAD_INPUT_BUFFER_SIZE];
        if (rawReadData == nil)
        {
            rawReadData = [[NSMutableData alloc] init];
        }
        
        [rawReadData appendBytes:(void *)buf length:bytesRead];
    }
    // Andrew Start Here

    NSString *s = [[NSString alloc] initWithData:rawReadData encoding:NSASCIIStringEncoding];

//    NSLog(@"_readData %d %@ ",bytesRead,s);
    
    while ([s contains:packetBegin])
    {
        NSString *packetCandidate = [s substringFromIndex:[s indexOf:packetBegin]];
        
        if ([packetCandidate contains:packetEnder])
        {
            NSString *r = [packetCandidate substringToIndex:[packetCandidate indexOf:packetEnder]];
            
        //  send this "up" to IBTVM somehow
        //  [self performSelectorOnMainThread:@selector(parseString:) withObject:[r copy] waitUntilDone:NO];

            [self parseString:r];
            
            [readMutex lock];
            
            [incomingPackets addObject:r];
       
            [readMutex unlock];
            
            s = [packetCandidate substringFromIndex:[packetCandidate indexOf:packetEnder]+[packetEnder length]];
            
        }
        else
            break;
    }
    
    [rawReadData release];
    rawReadData = [[NSMutableData alloc] initWithData:[s dataUsingEncoding:NSASCIIStringEncoding]];

    [NSThread sleepForTimeInterval:0.001];
}

// hopefully this runs on the UI thread and works.

- (void)parseString:(NSString*)packet
{
//    CMDLOG;
    
    NSString *str = [packet copy];
    
    [ibtvm ParseBTString:str];
}


#pragma mark Public Methods

+ (EADSessionController *)sharedController
{
//    CMDLOG;
    
    static EADSessionController *sessionController = nil;
    if (sessionController == nil) {
        sessionController = [[EADSessionController alloc] init];
    }

    return sessionController;
}

- (void)dealloc
{
//    CMDLOG;
    
    [self closeSession];
    [self setupControllerForAccessory:nil withProtocolString:nil];

    [super dealloc];
}

// initialize the accessory with the protocolString
- (void)setupControllerForAccessory:(EAAccessory *)accessory withProtocolString:(NSString *)protocolString
{
//    CMDLOG;
    
    [_accessory release];
    _accessory = [accessory retain];
    [_protocolString release];
    _protocolString = [protocolString copy];
}

// MSM ---------------------------------------------------------------------------------------

+ (NSThread *)socketReadThread {
    static NSThread *socketReadThread = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        socketReadThread =
        [[NSThread alloc] initWithTarget:self
                                selector:@selector(socketReadThreadMain:)
                                  object:nil];
        [socketReadThread start];
    });
    
    return socketReadThread;
}

+ (void)socketReadThreadMain:(id)unused {
    do {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] run];
        }
    } while (YES);
}

+ (NSThread *)socketWriteThread {
    static NSThread *socketWriteThread = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        socketWriteThread =
        [[NSThread alloc] initWithTarget:self
                                selector:@selector(socketWriteThreadMain:)
                                  object:nil];
        [socketWriteThread start];
    });
    
    return socketWriteThread;
}

+ (void)socketWriteThreadMain:(id)unused {
    do {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] run];
        }
    } while (YES);
}


- (void)scheduleInCurrentThread:(NSStream*)stream
{
    [stream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

// MSM ---------------------------------------------------------------------------------------

// open a session with the accessory and set up the input and output stream on the default run loop
- (BOOL)openSession
{
//    CMDLOG;
    
    NSLog(@"Open seesion with phone and iBTVM");
    
    [_accessory setDelegate:self];
    _session = [[EASession alloc] initWithAccessory:_accessory forProtocol:_protocolString];
    
    [EADSessionController socketReadThread];
    [EADSessionController socketWriteThread];
    
    if (_session)
    {
        [[_session inputStream] setDelegate:self];
        
      
        [self performSelector:@selector(scheduleInCurrentThread:)
                     onThread:[[self class] socketReadThread]
                   withObject:[_session inputStream]
                waitUntilDone:YES];
        
//        [[_session inputStream] scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [[_session inputStream] open];
        
        [[_session outputStream] setDelegate:self];
       
        [self performSelector:@selector(scheduleInCurrentThread:)
                     onThread:[[self class] socketWriteThread]
                   withObject:[_session outputStream]
                waitUntilDone:YES];
         
 //       [[_session outputStream] scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [[_session outputStream] open];
    }
    else
    {
//        NSLog(@"creating session failed");
    }
    
    return (_session != nil);
}


// close the session with the accessory.
- (void)closeSession
{
//    CMDLOG;
    NSLog(@"Close seesion with phone and iBTVM");
    
    [[_session inputStream] close];
    [[_session inputStream] removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [[_session inputStream] setDelegate:nil];
    [[_session outputStream] close];
    [[_session outputStream] removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [[_session outputStream] setDelegate:nil];

    [_session release];
    _session = nil;

    [rawWriteData release];
    rawWriteData = nil;
    [rawReadData release];
    rawReadData = nil;
}


#pragma mark EAAccessoryDelegate
- (void)accessoryDidDisconnect:(EAAccessory *)accessory
{
//    CMDLOG;
    
//    [self closeSession];
//    [self openSession];
    
    // do something ...
}

#pragma mark NSStreamDelegateEventExtensions

// asynchronous NSStream handleEvent method
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode
{
//    CMDLOG;
    
    switch (eventCode)
    {
        case NSStreamEventNone:
        {
//            NSLog(@"stream:NSStreamEventNone",nil);
            break;
        }
            
        case NSStreamEventOpenCompleted:
        {
//            NSLog(@"stream:NSStreamEventOpenCompleted",nil);
            break;
        }
     
        case NSStreamEventHasBytesAvailable:
        {
//            NSLog(@"stream:NSStreamEventHasBytesAvailable",nil);
            [self _readData];
           break;
        }
        
        case NSStreamEventHasSpaceAvailable:
        {
//            NSLog(@"stream:NSStreamEventHasSpaceAvailable",nil);
  //          [self _writeData];
            break;
        }

        case NSStreamEventErrorOccurred:
        {
//            NSLog(@"stream:NSStreamEventErrorOccurred",nil);
            break;
        }
    
        case NSStreamEventEndEncountered:
        {
//            NSLog(@"stream:NSStreamEventEndEncountered",nil);
            break;
        }
          
        default:
        {
//            NSLog(@"stream:default",nil);
            break;
        }
    }
}

@end
