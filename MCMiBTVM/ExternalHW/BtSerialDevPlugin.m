//
//  BtSerialDevPlugin.m
//  iProCPVolts
//
//  Created by Mark on 6/26/13.
//
//

#import "BtSerialDevPlugin.h"
#import "AppDelegate.h"

@implementation BtSerialDevPlugin

@synthesize bt;
@synthesize jsCallbackContext;

#pragma mark ======== init method =========
/*
 ** --------------------------------------------------------
 **  init method
 ** --------------------------------------------------------
 */

- (void)pluginInitialize
{
    CMDLOG;
    
    self.bt = nil;
    
    isInitialized = false;
    isConnected = false;
    
    devAddr = @"";
    self.jsCallbackContext = nil;
    
    sendThrottle = 300;
         

}

- (BOOL)doConnect:(NSString*)macaddress
{
    CMDLOG;
    
    if (isConnected)
    {
        if ([devAddr isEqualToString:macaddress])
            return YES;
        else
            [self doDisconnect];
    }
    
    devAddr = macaddress;
  
//  this.isConnected = bt.connectSerialSocket(this.devAddr);

    isConnected = YES;
    return isConnected;
}

- (void)doDisconnect
{
    CMDLOG;
    
    if (isInitialized)
    {
        [self.bt disconnect];
        isConnected = NO;
    }
}

- (BOOL)doInitialize
{
    CMDLOG;
    
    self.bt = [[ExternalComm alloc] init];
    
    if (self.bt)
    {
        [self.bt listDevices:nil :0];    //to set up global external accessory pointers, ignoring result here MSM
        isInitialized = YES;
        return YES;
    }
    
    return NO;
}

	
//default routine for reporting errors to javascript side - override if necessary
- (void)doErrorReport:(NSString *)s
{
    CMDLOG;
    
    NSString *err = s;
    
    if (self.jsCallbackContext)
    {
        CDVPluginResult* pluginResult;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:err];
        [pluginResult setKeepCallbackAsBool:true];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:self.jsCallbackContext];
    }
}

- (void)sendStr:(NSString *)cmd
{
    CMDLOG;
    
    [self.bt send:cmd];
}

- (void)onReset
{
    CMDLOG;
    
    [self doDisconnect];
    [super onReset];
}

//dispose is the closest I can find
- (void)onDestroy
{
    CMDLOG;
    
    [self doDisconnect];
//    bt.removeListener();
	[super dispose];
}



//---------------------------------------------------------------------------------------------------------------------------------------------------------------
// 'execute' routines
//	@Override
//	public boolean execute(String action, JSONArray args, CallbackContext cbContext) throws JSONException {

- (void)initSerialListener:(CDVInvokedUrlCommand*)command
{
    CMDLOG;

    NSString *name = @"";
    
    if ([command.arguments count] > 0)
        name  = [command.arguments objectAtIndex:0];
    
//not yet    [self.bt InitializeSerialListener:name];
    
   [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];

}

- (void)listDevices:(CDVInvokedUrlCommand*)command
{
    CMDLOG;
    
    NSString *namePrefix = @"";
    
    if ([command.arguments count] > 0)
        namePrefix  = [command.arguments objectAtIndex:0];
    
    NSArray *result = [self.bt listDevices:namePrefix:0];      //really wants JSONArray result
    
    CDVPluginResult* pluginResult = nil;
    if (result != nil && [result count] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:result];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}

- (void)connect:(CDVInvokedUrlCommand*)command
{
    CMDLOG;
    
    if (!isInitialized)
    {
        [self cbContextError:command:@"connect action before initialize"];
        return;
    }
    
    if ([self doConnect:[command.arguments objectAtIndex:0]])
        [self cbContextSuccess:command];
    else
         [self cbContextError:command:@"connect doConnect error"];
    
    
}

- (void)disconnect:(CDVInvokedUrlCommand*)command
{
    CMDLOG;
    
    if (!isInitialized)
    {
        [self cbContextError:command:@"disconnect action before initialize"];
        return;
    }
    
    [self doDisconnect];
    [self cbContextSuccess:command];
    
}

- (void)start:(CDVInvokedUrlCommand*)command
{
    CMDLOG;
    
    self.jsCallbackContext = nil;
    
    if (!isInitialized)
    {
        [self cbContextError:command:@"start action before initialize"];
        return;
    }
    
    if (!isConnected)
    {
        [self cbContextError:command:@"start action before connect"];
        return;
    }

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"live!"];
    
    [pluginResult setKeepCallbackAsBool:true];
 
    //ack the start
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
    self.jsCallbackContext = command.callbackId;
    
}

- (void)stop:(CDVInvokedUrlCommand*)command
{
    CMDLOG;
    
    if (!self.jsCallbackContext)
    {
        [self cbContextError:command:@"stop called while unstarted"];
        return;
    }
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"!stop!"];
    
    [pluginResult setKeepCallbackAsBool:false];
    
    //ack the stop
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
  
    self.jsCallbackContext = nil;
}


//----------------------------------------------------------------------------------------------------------------------------------------------------------------

//derivative of a deprecated port

-(void)cbContextSuccess:(CDVInvokedUrlCommand*)command
{
    CMDLOG;
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)cbContextSuccessMsg:(CDVInvokedUrlCommand*)command :(NSString *)okmsg
{
//    NSLog(@"cbContextSucces: %@",okmsg);
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:okmsg];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}



-(void)cbContextError:(CDVInvokedUrlCommand*)command :(NSString *)errmsg
{
    NSLog(@"cbContextError: %@",errmsg);
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errmsg];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


@end
