//
//  iBTVM.h
//  ProCPNavPad
//
//  Created by Mark on 5/1/13.
//
//

#import <Cordova/CDV.h>
#import "BtSerialDevPlugin.h"

#import "SignalLock.h"



//------------------------------------------------------------------------------------------------------------------------------------------------

@interface iBTVM : BtSerialDevPlugin
{
    
    double  maxFixInterval;		// 10 seconds between GPS fixes
    
	//Payload
	double volts;
	double voltsOn;
	double voltsOff;
	NSDate *onTime;
	NSDate *offTime;
	NSDate *sampleTime;
    NSDate *priorSample;
	NSString *gpsFixType;
	double latitude;
	double longitude;
	double altitude;
	int sats;
	double pdop;
	double hdop;
	double vdop;
	BOOL button1;
	BOOL button2;
	NSDate *lastGPS;
	NSDate *lastSimpleGPS;
	NSDate *lastFullGPS;
	
	//Status
	BOOL isStarted;
	NSString *firmware;
	NSString *serial;
	NSString *battery;
	BOOL gps_power;
	BOOL vm_power;
	BOOL vm_continuous;
	BOOL gpsUsePPS;
    BOOL gpsSynchOnOff;		// indicates user selection requiring GPS Synch On/Off
	NSDate *lastStatusUpdate;
	NSString *configError;
    BOOL    forceConfig;
	
	//Configuration
	NSMutableDictionary *cfg;
	int vmRange;
	int vmACRejection;
	int vmReadStyle; //0=Single, 1=Max/Min, 2=On/Off, 3=Single/predominant
    BOOL simpleCallback;
	int msOn;
	int msOff;
	int onDelay;
	int offDelay;
	BOOL cycleStartsWithOn;
	//downBeatTime, time of downbeat expressed as receprical of the time as a fraction of 1 day.
    // 1 = downbeat at  0000hr
    // 24 = downbeat at top of the hour
    // 24*60=1440 = downbeat at top of the minute
    long downbeatTime; //cycles are guaranteed to start on a downbeat
	NSString *btRange;
	NSString *btStyle;
	NSString *btACreject;
	NSMutableArray *rangeList;
	
	//Work variables
	int    gpsLeapSecondCorrection; 	  //ublox can be wrong on startup, value to be pulled from config
	double    downbeatInterval;          //millisecs between downbeats
    double  downbeatLatest;
	double   cycleStart;                //latest cycle start
//	NSDate *platformDate;
//  double   platformMS;				  // using platformMS rather than constantly converting to/from ms to date
//	double   lastPlatformMS;

	NSDate   *gpsDate;
	double   gpsDiff;                   //diff between platform time and gps time in ms such that gps+diff = platform
	double   readMS;
	double   lastMS;
	double   gpsMS;
    int     readFreq;
    
    double  gpsPulse;			// MS version of gpsDate which may be incremented without receiving @G2
	NSDate  *lastPulse;         // last time pulse was updated from @G2 or otherwise
    BOOL    resetPulse;         // force a reset when we 'start' continuous mode
	NSDate	*priorDt;			// for debugging
    
    
	//Raw Waveform
	NSMutableArray *rawWave;
    NSObject    *rawWaveMutex;
	
	/**
	 * @category Utility Classes and Functions
	 */
	id asyncContext;
    
    //maybe
    jsignal *rangeListDone;
    
    NSDate *now;
    NSDate *dtt;
    NSArray *splitArray;
    
}

@property (nonatomic, retain) NSDate *onTime;
@property (nonatomic, retain) NSDate *offTime;
@property (nonatomic, retain) NSDate *sampleTime;
@property (nonatomic, retain) NSDate *priorSample;
@property (nonatomic, retain) NSString *gpsFixType;
@property (nonatomic, retain) NSDate *lastGPS;
@property (nonatomic, retain) NSDate *lastSimpleGPS;
@property (nonatomic, retain) NSDate *lastFullGPS;
@property (nonatomic, retain) NSString *firmware;
@property (nonatomic, retain) NSString *serial;
@property (nonatomic, retain) NSString *battery;
@property (nonatomic, retain) NSDate *lastStatusUpdate;
@property (nonatomic, retain) NSString *configError;
@property (nonatomic, retain) NSMutableDictionary *cfg;
@property (nonatomic, retain) NSString *btRange;
@property (nonatomic, retain) NSString *btStyle;
@property (nonatomic, retain) NSString *btACreject;
@property (nonatomic, retain) NSMutableArray *rangeList;
@property (nonatomic, retain) NSDate *gpsDate;
@property (nonatomic, retain) NSDate *lastPulse;
@property (nonatomic, retain) NSMutableArray *rawWave;
@property (nonatomic, retain) NSObject *rawWaveMutex;


//------------------------------------------------------------------------------------------------------------------------------------------------------------------
// discrete routines

- (NSString *)getCRC:(NSString *)command;
- (NSString *)formatCommand:(NSString *)command;
- (BOOL)SameStr:(NSString *)str1 str2:(NSString *)str2;
- (NSMutableDictionary*)createReadingObj:(BOOL)withGPS :(BOOL)noComplex :(double)reqTime :(long)logFreq;
- (NSMutableDictionary*)createSampleObj;
- (void)addSample;
- (NSMutableArray*)createRangeListObj;
- (NSString*)MassageRange:(NSString *)range;
- (NSString*)MassageStyle:(NSString *)style;
- (NSString*)MassageACreject:(NSString *)acreject;
- (double)parseVolts:(NSString *)s;
- (NSDate*)parseVoltsTime:(NSString *)tm;
- (double)getMidnightMillisecs;
- (double)getMillisecsSinceMidnight:(NSDate*)dt;
- (NSDate*)parseGPSTime:(NSString*)hms :(NSString*)dmy;
- (double)parseLatLong:(NSString*)s :(NSString*)hemi;
- (BOOL)updateComplexReading;
- (NSMutableDictionary*)createWaveFormObj;
- (NSMutableDictionary*)formalWaveFormObj;
- (BOOL)parseStatement:(NSString*)result;
- (NSString *)ParseBTString:(NSString *)s;
- (void)doStop;

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
// 'execute' routines


- (void)listDevices:(CDVInvokedUrlCommand*)command;
- (void)listRanges:(CDVInvokedUrlCommand*)command;
- (void)initialize:(CDVInvokedUrlCommand*)command;
- (void)getRange:(CDVInvokedUrlCommand*)command;
- (void)getStatus:(CDVInvokedUrlCommand*)command;
- (void)diagnostic:(CDVInvokedUrlCommand*)command;
- (void)getVoltage:(CDVInvokedUrlCommand*)command;
- (void)getReading:(CDVInvokedUrlCommand*)command;
- (void)getWaveForm:(CDVInvokedUrlCommand*)command;
- (void)getConfig:(CDVInvokedUrlCommand*)command;
- (void)setConfig:(CDVInvokedUrlCommand*)command;
- (void)updateConfig:(CDVInvokedUrlCommand*)command;
- (void)connect:(CDVInvokedUrlCommand*)command;
- (void)start:(CDVInvokedUrlCommand*)command;
- (void)stop:(CDVInvokedUrlCommand*)command;
- (NSMutableDictionary*)createDefaultConfig;
- (void)downloadConfig;
- (BOOL)updateConfig2:(NSMutableDictionary*)ucfg :(int)vmr :(int)vmACR :(int)vmRd;
- (int)getReadStyle:(NSString*)readstyle;
- (int)getReadStyle2:(NSMutableDictionary*)scfg;
- (int)getRange1:(NSMutableDictionary*)rcfg;
- (int)getACRejection1:(NSMutableDictionary*)gcfg;
- (int)getRange2:(NSString*)range;
- (int)getACRejection2:(NSString *)acreject;
- (BOOL)updateConfig3:(NSMutableDictionary*)ucfg :(NSMutableDictionary*)json;
- (BOOL)updateConfig4:(NSMutableDictionary*)json;
- (BOOL)applyConfig:(NSMutableDictionary*)new_cfg;
- (void)fillDefaultConfig:(NSMutableDictionary*)json;
- (BOOL)configsDiffer:(NSMutableDictionary*)new_cfg :(NSMutableDictionary*)acfg :(NSString*)token;


@end

iBTVM *ibtvm;   //MSM TEMP?

//-------------------------------------------------------------------------------------------------------------------------------------------------------------
//Cocoa category extensions

@interface NSString (util)

- (int)indexOf:(NSString *)text;
- (BOOL)contains:(NSString *)string;
- (BOOL)isEmpty;

@end



//-------------------------------------------------------------------------------------------------------------------------------------------------------------



