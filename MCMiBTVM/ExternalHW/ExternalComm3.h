//
//  ExternalComm.h
//  ProCPNavPad
//
//  Created by Mark on 5/3/13.
//
//

#import <Cordova/CDV.h>

#import "EADSessionController.h"

#import <ExternalAccessory/ExternalAccessory.h>


@interface ExternalComm : CDVPlugin
{
    
    NSMutableArray  *found_devices;
    NSString        *discover_prefix;
    NSInteger       discover_class;
    BOOL            discovering;
    NSString        *btError;
    BOOL            btReconnect;
    NSString        *btAddress;
    BOOL            btFailed;
    NSString        *btCommand;
    NSInteger       minCommandMs;
    NSInteger       lastCommandMs;
    
    EADSessionController *eaSessionController;
    EAAccessory     *device;
    NSString        *deviceProtocol;
    BOOL            eaOpen;
    
    NSString *gresult;
    
}

@property (nonatomic, retain) EADSessionController *eaSessionController;
@property (nonatomic, retain) EAAccessory *device;
@property (nonatomic, retain) NSString *deviceProtocol;

//providing the abstraction layer between Android code and iOS external accessory framework

- (NSArray*)listDevices:(NSString *)prefix :(NSInteger)param;
- (NSString *)sendCmd:(NSString *)address command:(NSString *)command;
- (BOOL)send:(NSString *)command;
- (NSString *)getResp:(NSString *)address command:(NSString *)command header:(NSString *)header timeout:(int)timeout;
- (NSString *)sendCmdGetResp:(NSString *)address command:(NSString *)command header:(NSString *)header timeout:(int)timeout;

- (BOOL)connectSocket:(EAAccessory *)accessory;
- (BOOL)writeSocket:(NSString *)command;
- (NSString *)getIncoming:(NSString *)header timeout:(int)timeout;
- (void)flushIncoming;

- (void)disconnect;

@property (nonatomic, assign) BOOL btFailed;
@property (nonatomic, assign) BOOL btReconnect;

@end
