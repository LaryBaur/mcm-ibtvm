//
//  ExternalComm.m
//  ProCPNavPad
//
//  Created by Mark on 5/3/13.
//
//

#import "ExternalComm.h"
#import "AppDelegate.h"


@implementation ExternalComm

@synthesize btFailed;
@synthesize btReconnect;

@synthesize eaSessionController;
@synthesize device;
@synthesize deviceProtocol;


#pragma mark ======== init method =========
/*
 ** --------------------------------------------------------
 **  init method
 ** --------------------------------------------------------
 */
- init
{
	if (self = [super init])
	{
		CMDLOG;
        eaOpen = NO;
        eaSessionController = nil;
        btFailed = NO;
       readMutex = [[NSRecursiveLock alloc] init];
        writeMutex = [[NSRecursiveLock alloc] init];
        
	}
	return self;
}

/*
 
 we cant get device class from External Accessory.
 
 connected:YES
 connectionID:520950272
 name: MCM-iBTVM-9999
 manufacturer: Roving Networks
 modelNumber: RN-42-APL-X
 serialNumber:
 firmwareRevision: 1.2.3
 hardwareRevision: 4.5.6
 macAddress: 00:06:66:08:BF:AC
 protocols: (
 "com.RovingNetworks.btdemo"
 
 */



- (NSArray*)listDevices:(NSString *)prefix :(NSInteger)param
{
    CMDLOG;
    
    NSArray *versionCompatibility = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    int iOSversion = [[versionCompatibility objectAtIndex:0] intValue];
    
   
    NSArray *accessories = [[EAAccessoryManager sharedAccessoryManager] connectedAccessories];
    
    if (!accessories)
        return nil;
    
    NSMutableArray *devicesFound = [NSMutableArray array];
    
    for (EAAccessory *obj in accessories)
    {
        //cheating a bit here and assume its the first device found that we are using.
        device = obj;
        
        NSString *macAddress = @"Not available before iOS 6";

        if (iOSversion >= 6)
            macAddress = [obj valueForKey: @"macAddress"];
            
        NSLog(@"mac address: %@", macAddress);
        
        NSString *name = [obj valueForKey:@"name"];
        NSLog(@"name: %@", name);
        
        //    NSString *result = @"[ { 'name' : 'MCM-iBTVM-9999' ,'address' : '00:06:66:08:BF:AC' ,'class' : '1796' }] ";

        NSLog([obj description],nil);
        
        /* what we actually get on the iOS side of things
         connected:YES
         connectionID:28313395
         name: MCM-iBTVM-6001
         manufacturer: Roving Networks
         modelNumber: RN-42-APL-X
         serialNumber:
         firmwareRevision: 1.2.3
         hardwareRevision: 4.5.6
         protocols: (
         "com.mcmiller.protocol.voltmeter"
         )
         */
        
        //and more cheating assuming the first protocol listed is the one we're using (to assume makes an ASS out of U and ME)
        deviceProtocol = [[device protocolStrings] objectAtIndex:0];
        
        NSMutableDictionary *dev = [NSMutableDictionary dictionary];
        
        [dev setObject:name             forKey:@"name"];
        [dev setObject:macAddress       forKey:@"address"];
        [dev setObject:deviceProtocol   forKey:@"class"];
        
        [devicesFound addObject:dev];
    
    }
    
    [self connectSocket:device];
    
    
   

    return devicesFound;
    
}


- (NSString *)sendCmd:(NSString *)address command:(NSString *)command
{
    CMDLOG;
    
    if (![self connectSocket:device])       // device = EAAccessory
    {
        return btError;
    }
    if (![self writeSocket:command])
    {
        return btError;
    }
    return  @"";
}

- (BOOL)send:(NSString *)command
{
    CMDLOG;
    
  
    if (![self connectSocket:device])       // device = EAAccessory
    {
        return NO;
    }
    
    return [self writeSocket:command];
}



- (NSString *)getResp:(NSString *)address command:(NSString *)command header:(NSString *)header timeout:(int)timeout
{
    CMDLOG;
    
    btError = @"ERROR";
    
    NSString * result = @"";
    if (![self connectSocket:device])       // device = EAAccessory
    {
        return btError;
    }
      
    result = [self getIncoming:header timeout:timeout];
    return result;
}

- (NSString *)sendCmdGetResp:(NSString *)address command:(NSString *)command header:(NSString *)header timeout:(int)timeout
{
    CMDLOG;
    
    btError = @"ERROR";
    
    NSString * result = @"";
    if (![self connectSocket:device])       // device = EAAccessory
    {
        return btError;
    }
    if (![self writeSocket:command])
    {
        return btError;
    }
    
    result = [self getIncoming:header timeout:timeout];
    return result;

}

- (BOOL)connectSocket:(EAAccessory *)accessory
{
    CMDLOG;
    
//  if (btReconnect || (btSocket == null) || (btDevice == null) || (!btDevice.equals(device)) || (btUUID == null) || (!btUUID.toString().equalsIgnoreCase(uuid)))
    
    if (!eaOpen)
    {
        eaSessionController = [EADSessionController sharedController];
        [eaSessionController setupControllerForAccessory:accessory withProtocolString:deviceProtocol];
        eaOpen = [eaSessionController openSession];
    }
    
    return eaOpen;
}

- (BOOL)writeSocket:(NSString *)command
{
    CMDLOG;
    
  //  if (![command hasPrefix:@"@C"])
    //    return NO;  //MSM TEMP only start continuous.
    
    BOOL bRet = NO;
    
    
    if (!eaOpen)
        return bRet;
    
   
    
    if (eaSessionController.outgoingPackets)
    {
        [eaSessionController.outgoingPackets addObject:command];
        if (eaOpen)
//            [eaSessionController performSelector:@selector(_writeData) onThread:eaSessionController.writeThread  withObject:nil waitUntilDone:NO];
            [eaSessionController _writeData];       //DOESNT HAPPEN ON THE WRITE TRHEAD IF CALLED FROMHERE
        bRet = YES;
        NSLog(@"writeSocket: %@ cmd %@", [eaSessionController.outgoingPackets description], command);

    }
    
    
    return bRet;
}

- (void)flushIncoming
{
    CMDLOG;
    
    [readMutex lock];
    
    [eaSessionController.incomingPackets removeAllObjects];
    
    [readMutex unlock];
}

/*
while (result.isEmpty() && System.currentTimeMillis() <= end) {
    readMutex.lock();
    try {
        if (!incomingPackets.empty() && (header.isEmpty() || incomingPackets.peek().startsWith(header))) {
            result = incomingPackets.pop();
            wait = false;
        } else {
            wait = true;
        }
    } finally {
        readMutex.unlock();
    }
    // wait *after* unlocking mutex
    if (wait) try {
        Thread.sleep(50);
    } catch(InterruptedException ex) {
        //
    }
}
 */

- (NSString *)getIncoming:(NSString *)header timeout:(int)timeout
{
    CMDLOG;
    
    
    NSTimeInterval t = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval ts = (NSTimeInterval)timeout / 1000.0;
    NSTimeInterval end = t+ts;		// wait for result

    BOOL wait = false;
    
    gresult = @"";
    
    while (![gresult length] && ([[NSDate date] timeIntervalSince1970] <= end))
    {
        wait = true;
        
 //       if ([readMutex tryLock])
        {
       
      //      for (id object in array) {
        //        // do something with object
          //  }
            [readMutex lock];
            
            NSString *packet = [eaSessionController.incomingPackets lastObject];
            NSLog(@"getincoming packet %@ want header %@",packet,header);
         
            if (packet && [packet hasPrefix:header])
            {
                gresult = [packet copy];
                [eaSessionController.incomingPackets removeLastObject];
                wait = false;
                break;
            }

            [readMutex unlock];
        }
        
        if (wait)
            [NSThread sleepForTimeInterval:0.05];   // 50 milliseconds wait *after* unlocking mutex
    }
    
    return gresult;
}

/*
- (NSString *)getIncoming:(NSString *)header timeout:(int)timeout
{
    CMDLOG;
    
    NSLog(@"header: %@, timeout %d",header,timeout);
    
    NSTimeInterval t = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval end = t+(timeout / 1000);		// wait for result

    NSString *result = @"";
    
    while (![result length] && ([[NSDate date] timeIntervalSince1970] <= end))
    {
    //    [readMutex lock];
        NSString *packet = [incomingPackets lastObject];
        NSLog(@"getincoming packet %@",packet);
        if ([packet hasPrefix:header])
            result = packet;
    }
      
    return result;
        
}
   */
    
- (void)disconnect
{
    CMDLOG;
 
    if (eaOpen)
    {
        [[EADSessionController sharedController] closeSession];
        eaOpen = NO;
    }
}


@end
