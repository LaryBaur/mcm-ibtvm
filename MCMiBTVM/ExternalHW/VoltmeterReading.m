//
//  VoltmeterReading.m
//  ProCPNavPad
//
//  Created by Mark on 5/14/13.
//
//


//
//  VoltmeterReading.m
//  ProCPNavPad
//
//  Created by Mark on 4/18/13.
//
//

#import "VoltmeterReading.h"
#import "AppDelegate.h"


// look! a category! http://uplink.to/7i7


@interface NSString (util)

- (int) indexOf:(NSString *)text;

@end

@implementation NSString (util)

- (int) indexOf:(NSString *)text {
    NSRange range = [self rangeOfString:text];
    if ( range.length > 0 ) {
        return range.location;
    } else {
        return -1;
    }
}

@end

@implementation VoltmeterReading

#pragma mark ======== init method =========
/*
 ** --------------------------------------------------------
 **  init method
 ** --------------------------------------------------------
 */
- init
{
	if (self = [super init])
	{
		CMDLOG;
	}
	return self;
}

- (NSString *)signifDigits:(double)von voff:(double)voff digits:(int)digits
{
    CMDLOG;
    
    NSString * sVon = @"";
    NSString * sVoff = @"";
   int intChars = 1;
    if (isnormal(von))  //is NOT nanNumber or posInfinity
    {
        sVon = [NSString stringWithFormat:@"%f", von];      // %lf is specified for doubles see how go staying true to the java for now
        if ([sVon indexOf:@"."] > intChars)
        {
            intChars = [sVon indexOf:@"."];
            if ([sVon hasPrefix:@"-"])
                intChars--;
        }
    }
    
    if (isnormal(voff))            //original java is (!Double.isNaN(von)) voff?
    {
        sVoff = [NSString stringWithFormat:@"%f", voff];
        if ([sVoff hasPrefix:@"-"])
            sVon = [sVoff substringFromIndex:1];
        if ([sVoff indexOf:@"."] > intChars)
        {
            intChars = [sVoff indexOf:@"."];
            if ([sVoff hasPrefix:@"-"])
                intChars--;
        }
    }
    
    int fracChars = digits - intChars;
    
    NSString *result = @"ERROR";
    NSString *format1 = @"%";
    NSString *format2 = @"%";        //Coca is choking on "%%s" I know there is another way this will get us through the night.
    
    NSString *format = [NSString stringWithFormat:@"%@.%@",[@(digits+2) description], [@(fracChars) description]];
    format1 = [format1 stringByAppendingString:format];
    format1 = [NSString stringWithFormat:format, von];
    
    if (isnormal(voff))
    {
        format2 = [format2 stringByAppendingString:format];
        format2 = [NSString stringWithFormat:format, voff];
        result = [NSString stringWithFormat:@"%@|%@",format1, format2];
    }
    else
    {
        result = format1;
    }
    
    return result;
}

- (NSString *) toDisplayString
{
    CMDLOG;
    
    NSString *result = @"ERROR";
    
    double valOn = _voltsOn;
    double valOff = _voltsOff;
    
    // JSON parsing
	SBJsonParser *parser = [SBJsonParser new];
	id jsonObject = [parser objectWithString:_config];      //lets assume _config is an NSString with a JSON object in it.
	// the result supports key-value coding
    
//	NSDictionary* values = [jsonObject valueForKey:@"responseData"];
//	NSArray *rez = [values objectForKey:@"results"];

     if (([jsonObject boolForKey:@"isCalculated"] && (([jsonObject doubleForKey:@"multiplier"] != 1.0) || ([jsonObject doubleForKey:@"addend"] != 0.0))))
    {
        valOn *= [jsonObject doubleForKey:@"multiplier"];
        valOn += [jsonObject doubleForKey:@"addend"];
        valOff *= [jsonObject doubleForKey:@"multiplier"];
        valOff += [jsonObject doubleForKey:@"addend"];
    }
    
    if ([jsonObject boolForKey:@"dispMillis"])
    {
        valOn *= 1000.0;
        valOff *= 1000.0;
    }
    
    if ([[jsonObject stringForKey:@"readStyle"] isEqualToString:@"single"])
    {
        result = [self signifDigits:valOn voff:NAN digits:5];
    }
    else
    {
        result = [self signifDigits:valOn voff:valOff digits:5];
    }
    
    return result;
}

- (void) dealloc
{
    CMDLOG;
    
    [_config release];
    [_gpsFixType release];
    [super dealloc];
}

@end
