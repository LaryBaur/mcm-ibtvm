/*
 
     File: EADSessionController.m
 Abstract: n/a
  Version: 1.1
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2010 Apple Inc. All Rights Reserved.
 
 
 */

#import "EADSessionController.h"
#import "AppDelegate.h"

#import "iBTVM.h"       //MSM TEMP!

NSString *EADSessionDataReceivedNotification = @"EADSessionDataReceivedNotification";

NSString *packetEnder = @"\r\n";
NSString *packetBegin = @"@";

int hascount = 0;

@implementation EADSessionController

@synthesize accessory = _accessory;
@synthesize protocolString = _protocolString;
@synthesize rawReadData;
@synthesize outgoingPackets;
@synthesize incomingPackets;
@synthesize readThread;
@synthesize writeThread;

#pragma mark ======== init method =========
/*
 ** --------------------------------------------------------
 **  init method
 ** --------------------------------------------------------
 */
- init
{
	if (self = [super init])
	{
		CMDLOG;
        
       self.incomingPackets = [NSMutableArray arrayWithCapacity:10];
       self.outgoingPackets = [NSMutableArray arrayWithCapacity:10];
        
	}
	return self;
}

#pragma mark Internal

// low level write method - write data to the accessory while there is space available and data to write
- (void)_writeData
{
    CMDLOG;

//    [writeMutex lock];
    
//    @autoreleasepool
    {
         NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
        
        int cut = 0;
        
        while ([self.outgoingPackets count])
        {
            cut++;
            
            sw = [self.outgoingPackets objectAtIndex:0];
            
            NSMutableData *rawWriteData = (NSMutableData *)[sw dataUsingEncoding:NSASCIIStringEncoding];
            
            int ln = [rawWriteData length];
            
            while (([[_session outputStream] hasSpaceAvailable]) && (ln > 0))
            {
                NSInteger bytesWritten = [[_session outputStream] write:[rawWriteData bytes] maxLength:ln];
                if (bytesWritten == -1)
                {
                    NSLog(@"write error");
                    break;
                }
                else if (bytesWritten > 0)
                {
                    NSLog(@"rawWriteData bytesWritten %@ %ld",rawWriteData, (long)bytesWritten);
                    [rawWriteData replaceBytesInRange:NSMakeRange(0, bytesWritten) withBytes:NULL length:0];
                }
                ln = [rawWriteData length];
            }
            
            if ((ln == 0) && ([self.outgoingPackets count]))
            {
                [self.outgoingPackets removeObjectAtIndex:0];
                [NSThread sleepForTimeInterval:0.05];       //except that we are not on a thread mostly.
            }
            
            // [rawWriteData release];
        }
        
        [pool drain];

    }
    
    
//   [writeMutex unlock];

    NSLog(@"leaving _writeData");

}

// low level read method - read data while there is data and space available in the input buffer
- (void)_readData
{
    CMDLOG;
    
    NSInteger bytesRead;
    
#define EAD_INPUT_BUFFER_SIZE 512
    
    uint8_t buf[EAD_INPUT_BUFFER_SIZE];
    
    memset(buf, 0, sizeof(buf));    //for debugging clarity
    
//    > If you don't call -read:maxLength:, the stream won't send you another NSStreamEventHasBytesAvailable event.

    
    while ([[_session inputStream] hasBytesAvailable])
    {
         bytesRead = [[_session inputStream] read:buf maxLength:EAD_INPUT_BUFFER_SIZE];
        if (rawReadData == nil)
        {
            rawReadData = [[NSMutableData alloc] init];
        }
        
        [rawReadData appendBytes:(void *)buf length:bytesRead];
    }
    

   
   sr = [[NSString alloc] initWithData:rawReadData encoding:NSASCIIStringEncoding];

    NSLog(@"_readData %d  ",[sr length]);
    
    while ([sr contains:packetBegin])
    {
        NSString *packetCandidate = [sr substringFromIndex:[sr indexOf:packetBegin]];
        
        if ([packetCandidate contains:packetEnder])
        {
            NSString *r = [packetCandidate substringToIndex:[packetCandidate indexOf:packetEnder]];
            
            //send this "up" to IBTVM somehow
          //  [self performSelectorOnMainThread:@selector(parseString:) withObject:[r copy] waitUntilDone:NO];
            
                [self parseString:r];
           
            
     //      if ([readMutex tryLock])
            {
                
                [readMutex lock];
            
                [self.incomingPackets addObject:r];
       
                [readMutex unlock];
            
                sr = [packetCandidate substringFromIndex:[packetCandidate indexOf:packetEnder]+[packetEnder length]];
            }
        }
        else
            break;
    }
    
    
    [rawReadData release];
    rawReadData = [[NSMutableData alloc] initWithData:[sr dataUsingEncoding:NSASCIIStringEncoding]];

 //   [NSThread sleepForTimeInterval:0.1];
}

// hopefully this runs on the UI thread and works.

- (void)parseString:(NSString*)packet
{
    CMDLOG;
    
    NSString *str = [packet copy];
    
    [ibtvm ParseBTString:str];
    
    [str release];
}


#pragma mark Public Methods

+ (EADSessionController *)sharedController
{
    CMDLOG;
    
    static EADSessionController *sessionController = nil;
    if (sessionController == nil) {
        sessionController = [[EADSessionController alloc] init];
    }

    return sessionController;
}

- (void)dealloc
{
    CMDLOG;
    
    [self closeSession];
    [self setupControllerForAccessory:nil withProtocolString:nil];

    [super dealloc];
}

// initialize the accessory with the protocolString
- (void)setupControllerForAccessory:(EAAccessory *)accessory withProtocolString:(NSString *)protocolString
{
    CMDLOG;
    
    [_accessory release];
    _accessory = [accessory retain];
    [_protocolString release];
    _protocolString = [protocolString copy];
}

// MSM ---------------------------------------------------------------------------------------
//   CFStreamCreateBoundPair
// void CFReadStreamSetDispatchQueue(CFReadStreamRef stream, dispatch_queue_t q) CF_AVAILABLE(10_9, 7_0);

+ (NSThread *)socketReadThread {
    static NSThread *socketReadThread = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        socketReadThread =
        [[NSThread alloc] initWithTarget:self
                                selector:@selector(socketReadThreadMain:)
                                  object:nil];
        [socketReadThread start];
    });
    
    return socketReadThread;
}

+ (void)socketReadThreadMain:(id)unused {
    do {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] run];
        }
    } while (YES);
}

+ (NSThread *)socketWriteThread {
    static NSThread *socketWriteThread = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        socketWriteThread =
        [[NSThread alloc] initWithTarget:self
                                selector:@selector(socketWriteThreadMain:)
                                  object:nil];
        [socketWriteThread start];
    });
    
    return socketWriteThread;
}

+ (void)socketWriteThreadMain:(id)unused {
    do {
        @autoreleasepool {
            
 //           [[EADSessionController sharedController]  _writeData];
   //         [NSThread sleepForTimeInterval:0.5];
           
            [[NSRunLoop currentRunLoop] run];
        }
    } while (YES);
}


- (void)scheduleInCurrentThread:(NSStream*)stream
{
    [stream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

// MSM ---------------------------------------------------------------------------------------

// open a session with the accessory and set up the input and output stream on the default run loop
- (BOOL)openSession
{
    CMDLOG;
    
    
    [_accessory setDelegate:self];
    _session = [[EASession alloc] initWithAccessory:_accessory forProtocol:_protocolString];
    
//    self.readThread = [EADSessionController socketReadThread];
//    self.writeThread = [EADSessionController socketWriteThread];
    
    if (_session)
    {
     
        
      
//        [self performSelector:@selector(scheduleInCurrentThread:)
//                     onThread:[[self class] socketReadThread]
//                   withObject:[_session inputStream]
//                waitUntilDone:NO];
        
        [[_session inputStream] setDelegate:self];
        [[_session inputStream] scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [[_session inputStream] open];
        
       
        
//        [self performSelector:@selector(scheduleInCurrentThread:)
//                     onThread:[[self class] socketWriteThread]
//                   withObject:[_session outputStream]
//                waitUntilDone:NO];
         
        [[_session outputStream] setDelegate:self];
        [[_session outputStream] scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [[_session outputStream] open];
    }
    else
    {
        NSLog(@"creating session failed");
    }
    
    return (_session != nil);
}


// close the session with the accessory.
- (void)closeSession
{
    CMDLOG;
    
    [[_session inputStream] close];
    [[_session inputStream] removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [[_session inputStream] setDelegate:nil];
    [[_session outputStream] close];
    [[_session outputStream] removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [[_session outputStream] setDelegate:nil];

    [_session release];
    _session = nil;

  //  [rawWriteData release];
    //rawWriteData = nil;
    [rawReadData release];
    rawReadData = nil;
}


#pragma mark EAAccessoryDelegate
- (void)accessoryDidDisconnect:(EAAccessory *)accessory
{
    CMDLOG;
    
    // do something ...
}

#pragma mark NSStreamDelegateEventExtensions

// asynchronous NSStream handleEvent method
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode
{
    CMDLOG;
    
    NSLog(@"Stream status: %d", aStream.streamStatus);
    NSLog(@"Stream error: %@", [aStream.streamError description]);

    
    switch (eventCode)
    {
        case NSStreamEventNone:
        {
            NSLog(@"stream:NSStreamEventNone",nil);
            break;
        }
            
        case NSStreamEventOpenCompleted:
        {
            NSLog(@"stream:NSStreamEventOpenCompleted",nil);
            break;
        }
     
        case NSStreamEventHasBytesAvailable:
        {
            NSLog(@"stream:NSStreamEventHasBytesAvailable %d",hascount,nil);
            hascount++;
            [self _readData];
           break;
        }
        
        case NSStreamEventHasSpaceAvailable:
        {
            NSLog(@"stream:NSStreamEventHasSpaceAvailable",nil);
           [self _writeData];
            break;
        }

        case NSStreamEventErrorOccurred:
        {
            NSLog(@"stream:NSStreamEventErrorOccurred",nil);
            break;
        }
    
        case NSStreamEventEndEncountered:
        {
            NSLog(@"stream:NSStreamEventEndEncountered",nil);
            break;
        }
          
        default:
        {
            NSLog(@"stream:default",nil);
            break;
        }
    }
}

@end
