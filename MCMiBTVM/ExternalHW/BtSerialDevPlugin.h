//
//  BtSerialDevPlugin.h
//  iProCPVolts
//
//  Created by Mark on 6/26/13.
//
//

#import <Cordova/CDV.h>
#import "ExternalComm.h"

@interface BtSerialDevPlugin : CDVPlugin
{
    BOOL isInitialized;
	BOOL isConnected;
    
    ExternalComm * bt;
    
    NSString *devAddr;
    
    NSString *jsCallbackContext;
    
    int sendThrottle;
}

@property (nonatomic, retain) ExternalComm * bt;
@property (nonatomic, retain) NSString *jsCallbackContext;

- (BOOL)doConnect:(NSString*)macaddress;
- (void)doDisconnect;
- (BOOL)doInitialize;
- (void)doErrorReport:(NSString *)s;    ////default routine for reporting errors to javascript side - override if necessary
- (void)sendStr:(NSString *)cmd;
- (void)onReset;
- (void)onDestroy;

//----------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void)initSerialListener:(CDVInvokedUrlCommand*)command;
- (void)listDevices:(CDVInvokedUrlCommand*)command;
- (void)connect:(CDVInvokedUrlCommand*)command;
- (void)disconnect:(CDVInvokedUrlCommand*)command;
- (void)start:(CDVInvokedUrlCommand*)command;
- (void)stop:(CDVInvokedUrlCommand*)command;

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void)cbContextSuccess:(CDVInvokedUrlCommand*)command;
- (void)cbContextSuccessMsg:(CDVInvokedUrlCommand*)command :(NSString *)okmsg;
- (void)cbContextError:(CDVInvokedUrlCommand*)command :(NSString*)errmsg;

@end
