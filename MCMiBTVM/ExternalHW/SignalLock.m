//
//  SignalLock.m
//  iProCPVolts
//
//  Created by Mark on 8/21/13.
//
//

#import "SignalLock.h"
#import "AppDelegate.h"

/**
 * @category Utility Classes and Functions
 */

// these may never be used on iOS
// https://bitbucket.org/ronroberts/procpdig/src/36ffff9f16bd8f1535c78baa9f8299f2f29f3831/src/com/mcmiller/plugins/iBTVM.java?at=default#cl-106

@implementation jsignal

- init
{
    CMDLOG;
    
    self = [super init];
    if (self)
    {
        Done = [[NSObject alloc] init];
        isDone = false;
    }
    return self;
    
}

-(void)doWait
{
    @synchronized(Done)
    {
        while(!isDone)
        {
            //http://developer.android.com/reference/java/lang/Object.html#wait()
            //[Done wait];
            //https://developer.apple.com/library/mac/documentation/cocoa/reference/foundation/Classes/NSLock_Class/Reference/Reference.html
            isDone = false;
        }
    }
}

-(void)doNotify
{
    @synchronized(Done)
    {
        isDone = true;
    }
}

@end
