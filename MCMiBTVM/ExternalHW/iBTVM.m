//
//  iBTVM.m
//  ProCPNavPad
//
//  Created by Mark on 5/1/13.
//
//

#import <math.h>
#import "iBTVM.h"
#import "AppDelegate.h"

#import <JavaScriptCore/JavaScriptCore.h>  //MSM TEMP

/**
 * @category data items
 */

#define msPerDay 86400000.0
#define PREF_FILE_NAME @"iBtvmPrefs"

#define VNAN    9999.0

@implementation iBTVM

@synthesize onTime;
@synthesize offTime;
@synthesize sampleTime;
@synthesize priorSample;
@synthesize gpsFixType;
@synthesize lastGPS;
@synthesize lastSimpleGPS;
@synthesize lastFullGPS;
@synthesize firmware;
@synthesize serial;
@synthesize battery;
@synthesize lastStatusUpdate;
@synthesize configError;
@synthesize cfg;
@synthesize btRange;
@synthesize btStyle;
@synthesize btACreject;
@synthesize rangeList;
@synthesize gpsDate;
@synthesize lastPulse;
@synthesize rawWave;
@synthesize rawWaveMutex;

static NSInteger numberOfUpdates = 0;

#pragma mark ======== init method =========
/*
 ** --------------------------------------------------------
 **  init method
 ** --------------------------------------------------------
 */
- (void)pluginInitialize
{
//    CMDLOG;
    
    maxFixInterval = 10 * 1000L;    // 10 seconds between GPS fixes
    
    // http://stackoverflow.com/a/990826
    
    volts = INFINITY;
    voltsOn = INFINITY;             // [-]OVER -> Double.NaN -> JSON obj: "NaN"
    voltsOff = INFINITY;            // Missing/Invalid -> Double.POSITIVE_INFINITY -> JSON obj: ""
    
    //Status
    isStarted = false;
    self.firmware = @"";
    self.serial = @"";
    self.battery = @"";
    gps_power = false;
    vm_power = false;
    vm_continuous = false;
    gpsUsePPS = true;
    gpsSynchOnOff = false;		// indicates user selection requiring GPS Synch On/Off
    configError = @"";
 	forceConfig = YES;
    
    //Configuration
    vmReadStyle = 0;                //0=Single, 1=Max/Min, 2=On/Off, 3=Single/predominant
    simpleCallback = NO;
    cycleStartsWithOn = false;
    //downBeatTime, time of downbeat expressed as receprical of the time as a fraction of 1 day.
    // 1 = downbeat at  0000hr
    // 24 = downbeat at top of the hour
    // 24*60=1440 = downbeat at top of the minute
    downbeatTime = 1;               //cycles are guaranteed to start on a downbeat
    btRange = @"";
    btStyle = @"";
    btACreject = @"";
    
    //Work variables
    downbeatInterval = 0;          //millisecs between downbeats
    downbeatLatest = 0;
    cycleStart = 0;                //latest cycle start
//  platformMS = 0;                // using platformMS rather than constantly converting to/from ms to date
//  lastPlatformMS = 0;
    
    self.gpsDate = nil;
    gpsDiff = 0;                   //diff between platform time and gps time in ms such that gps+diff = platform
    readMS = 0;
    lastMS = 0;
    gpsMS = 0;
    readFreq = 0;
    
	gpsPulse = 0;                   // MS version of gpsDate which may be incremented without receiving @G2
	self.lastPulse = nil;                // last time pulse was updated from @G2 or otherwise
    resetPulse = NO;
    priorDt = nil;
    
    self.gpsFixType = @"none";       //MSM TEMP
    self.lastFullGPS = [NSDate date]; //MSM TEMP
    self.lastGPS = [NSDate date]; //MSM TEMP
    
    
    asyncContext = nil;
    
    rangeListDone = [[jsignal alloc] init];
    
    ibtvm = self;           //so External Accessory class can get to us MSM TEMP?
    
    self.cfg = nil;
    self.jsCallbackContext = nil;
    
    
    self.rawWave = [NSMutableArray arrayWithCapacity:1000];
    self.rawWaveMutex = [[NSObject alloc] init];
    
    
    
}

- (void)sendEmail:(CDVInvokedUrlCommand*)command {
    
}

- (NSString *)getCRC:(NSString *)command
{
//    CMDLOG;
    
    if ([command length] == 0) {
        return @"empty";
    }
    
    @try
    {
        int s = [command indexOf:@"@"];
        if (s == -1) s = 0; else s++;
        int e = [command indexOf:@"*"];
        if (e == -1) e = [command length] - 1; else e--;

        UTF8Char *bytes = (UTF8Char *)[command UTF8String];
        int lngChecksum = 0;
        for (int i = s; i <= e; i++) {
            lngChecksum += bytes[i];
        }
        
        NSString *strChecksum = [[NSString stringWithFormat:@"%x", lngChecksum] uppercaseString];
        int l = [strChecksum length];
        strChecksum = [strChecksum substringFromIndex:l-2];
        return strChecksum;
    }
    @catch (NSException *exception)
    {
//        NSLog(@"getCRC exception name: %@ reason: %@", [exception name],[exception reason]);
        return @"empty";
    }
    
}

- (NSString *)formatCommand:(NSString *)command
{
//    CMDLOG;
    
//    NSLog(@"Logging format command: %@", command);
    
    if ([command indexOf:@"@"] != -1)
    {
        return [command stringByAppendingString:@"\r\n"];
    }
    else
    {
        return [NSString stringWithFormat: @"@%@*%@\r\n",command,[self getCRC:command]];
    }
}

- (BOOL)SameStr:(NSString *)str1 str2:(NSString *)str2
{
//    CMDLOG;
    
    str1 = [str1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    str2 = [str2 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSComparisonResult cr = [str1 caseInsensitiveCompare:str2];
    
    if (cr == NSOrderedSame) return YES;
    
    return NO;
}

/**
 * Generate voltmeter reading object
 */

- (NSMutableDictionary*)createReadingObj:(BOOL)withGPS :(BOOL)noComplex :(double)reqTime :(long)logFreq
{
    //create object to report voltmeter state back to JS side
    
//    CMDLOG;
    
    NSMutableDictionary *obj = [NSMutableDictionary dictionaryWithCapacity:10];      //empty dictionary == new JSONObject()
    
    //- everything has to be converted to an NSObject to be stored in a dictionary
    
    [obj setObject:[NSNumber numberWithBool:noComplex] forKey:@"noComplex"];
    
    @try
    {
        if (vmReadStyle == 0 && reqTime != 0) {
            if (logFreq == 0) logFreq = 1;	// don't want logFreq to be 0 as would include reqTime in 'future' catch-up values
            
            NSMutableArray *reads = [NSMutableArray array];
            NSMutableArray *wave;
            @synchronized(self.rawWaveMutex)
            {
                wave = [NSMutableArray arrayWithArray:self.rawWave]; //copy waveform
            }
            
            double rvolts = INFINITY;    //defining just 'volts' here instead 'rvolts' brings warning "Local Declaration of 'volts' hides instance variable"
            double tm = 0;
            
            if (logFreq == 50 && readFreq == 50) {
                // special case where we want all since last time
                for (int i = 0; i < [wave count]; i++) {				// oldest first
                    double v = [[[wave objectAtIndex:i] objectForKey:@"volts"] doubleValue];
                    if ((v != VNAN) && (v != INFINITY)) {
                        double t = [[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue];
                        if (t > reqTime-50) {		// after last logged reading
                            // JS code expects single value, plus array of catch-up values
                            if (tm == 0) {
                                tm = t;
                                rvolts = v;
                            } else {
                                NSMutableDictionary *o = [NSMutableDictionary dictionary];
                                [o setObject:[NSNumber numberWithDouble:v] forKey:@"v"];
                                [o setObject:[NSNumber numberWithDouble:t] forKey:@"t"];
                                [reads addObject:o];
                            }
                        }
                    }
                }
            } else {
                // find reading closest to required time
                int idx = -1;
                double minDif = 0;
                for (int i = [wave count]-1; i >= 0; i--) {			// newest first
                    double v = [[[wave objectAtIndex:i] objectForKey:@"volts"] doubleValue];
                    if ((v != VNAN) && (v != INFINITY)) {
                        double t = [[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue];
                        double dif = fabs(t - reqTime);
                        if ((rvolts == INFINITY) || dif < minDif) {
                            minDif = dif;			// closer than previous
                            rvolts = v;
                            tm = t;
                            idx = i;
                        } else if ((v != INFINITY) && dif > minDif) {
                            break;					// further than previous
                        }
                    }
                }
                if (idx == 0 && tm < reqTime && minDif > readFreq/2) {
                    // exclude closest reading if earlier and not close enough
                    // so we don't use value at 80ms, instead of one at 103ms which is yet to be read
                    rvolts = INFINITY;
                    tm = 0;
                    idx = -1;
                }
                if (idx > 0) {
                    double nextTime = tm + logFreq;	// send back catch-up readings matching log frequency
                    
                    // add catch-up array
                    for (int i = idx+1; i < [wave count]; i++) {		// oldest first (later than closest)
                        double v = [[[wave objectAtIndex:i] objectForKey:@"volts"] doubleValue];
                        if ((v != VNAN) && (v != INFINITY)) {
                            double t = [[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue];
                            // add values *after* required time to reads array (at logFreq increments)
                            if (t >= nextTime - readFreq/2) {
                                NSMutableDictionary *o = [NSMutableDictionary dictionary];
                                [o setObject:[NSNumber numberWithDouble:v] forKey:@"v"];
                                [o setObject:[NSNumber numberWithDouble:t] forKey:@"t"];
                                [reads addObject:o];
                                nextTime = t + logFreq;
                            }
                        }
                    }
                }
            }
            
            [obj setObject:reads forKey:@"reads"];
            
            if (rvolts == VNAN) {
                [obj setObject:@"NaN" forKey:@"voltsOn"];
            } else if (rvolts == INFINITY) {
                [obj setObject:@"" forKey:@"voltsOn"];
            } else {
                [obj setObject:[NSNumber numberWithDouble:rvolts] forKey:@"voltsOn"];
            }
            if (tm != 0) {
                [obj setObject:[NSNumber numberWithDouble:tm] forKey:@"onTime"];
            }
            
        } else {                         //sudden use of this. in the Java begins here...
            if (voltsOn == VNAN) {
                [obj setObject:@"NaN" forKey:@"voltsOn"];
            } else if (voltsOn == INFINITY) {
                [obj setObject:@"" forKey:@"voltsOn"];
            } else {
                [obj setObject:[NSNumber numberWithDouble:voltsOn] forKey:@"voltsOn"];
            }
            if (self.onTime != nil) {
                [obj setObject:[NSNumber numberWithDouble:[self.onTime getTime]] forKey:@"onTime"];
            }
        }
        
        if (([[self.cfg objectForKey:@"ReadStyle"] isEqualToString:@"on/off"]) || ([[self.cfg objectForKey:@"ReadStyle"] isEqualToString:@"max/min"])) {
            if (voltsOff == VNAN) {
                [obj setObject:@"NaN" forKey:@"voltsOff"];
            } else if (voltsOff == INFINITY) {
                [obj setObject:@"" forKey:@"voltsOff"];
            } else {
                [obj setObject:[NSNumber numberWithDouble:voltsOff] forKey:@"voltsOff"];
            }
            if (self.offTime != nil) {
                [obj setObject:[NSNumber numberWithDouble:[self.offTime getTime]] forKey:@"offTime"];
            }
        }
        if (withGPS) {
            [obj setObject:self.gpsFixType forKey:@"gpsFixType"];
            if ((self.gpsFixType != nil) && (![self.gpsFixType isEqualToString:@"none"])) {
                [obj setObject:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
                [obj setObject:[NSNumber numberWithDouble:longitude] forKey:@"longitude"];
                [obj setObject:[NSNumber numberWithDouble:altitude] forKey:@"altitude"];
                [obj setObject:[NSNumber numberWithInt:pdop] forKey:@"pdop"];
                [obj setObject:[NSNumber numberWithInt:hdop] forKey:@"hdop"];
                [obj setObject:[NSNumber numberWithInt:vdop] forKey:@"vdop"];
                [obj setObject:[NSNumber numberWithDouble:[self.lastGPS getTime]] forKey:@"lastGPS"];
                [obj setObject:[NSNumber numberWithDouble:[self.gpsDate getTime]] forKey:@"gpsDate"];
                [obj setObject:[NSNumber numberWithDouble:gpsDiff] forKey:@"gpsDiff"];      // used to correct earlier logged/charted data
            }
        }
        
        
        return obj;
    }
    @catch (NSException *exception)
    {
//        NSLog(@"createReadingObj exception name: %@ reason: %@", [exception name],[exception reason]);
        return nil; //or just obj? off reservation here...
    }
    
}


/**
 * @return voltage sample from voltmeter (JSONObject)
 */

- (NSMutableDictionary*)createSampleObj
{
//    CMDLOG;
    
    NSMutableDictionary *samp = [NSMutableDictionary dictionaryWithCapacity:10];      //empty dictionary == new JSONObject()
    
    //everything has to be converted to an NSObject to be stored in a dictionary
    
    if (volts == VNAN)
        [samp setObject:@"NaN" forKey:@"volts"];           // "NaN" will CRASH the cordova json parser so we have to go with something else...
    else if (volts == INFINITY)
        [samp setObject:@"" forKey:@"volts"];
    else
        [samp setObject:[NSNumber numberWithDouble:volts] forKey:@"volts"];
    
    [samp setObject:[NSNumber numberWithDouble:[self.sampleTime getTime]] forKey:@"sampleTime"];
    
    return samp;
}

/*
- (void)addSample
{
    CMDLOG;
    
    [self.rawWave addLast:[self createSampleObj]];
    long ago = [self.sampleTime timeIntervalSince1970] - (msOn + msOff)*2;   //keep 2 cycles worth
    
    //using my custom NSMutableArray (ArrayDeque) category
    
    NSMutableDictionary *samp = (NSMutableDictionary*)[self.rawWave peekFirst];
    
    if (!samp)
        return;  //null, don't know what to do any more
    
    NSNumber *snum = [samp objectForKey:@"sampleTime"];       //MSM TEMP
    
    if (!snum)
        return;  //null, don't know what to do any more

    while ((samp != nil) && ([snum longValue] < ago))           // (snum holds a double tho?)
    {
        [self.rawWave removeFirst]; //remove obsolete sample
        samp = (NSMutableDictionary*)[self.rawWave peekFirst];
    }
}
 */
- (void)addSample
{
//    CMDLOG;
    
    @try
    {
        @synchronized(self.rawWaveMutex)
        {
            [self.rawWave addLast:[self createSampleObj]];
            double ago;
            if (vmReadStyle == 0)
                ago = [self.sampleTime getTime] - 2000;     // keep 1 seconds worth (says the comment, but the Java code says 2000)
            else
                ago = [self.sampleTime getTime] - (msOn + msOff)*2;   //keep 2 cycles worth
            
            //using my custom NSMutableArray (ArrayDeque) category
            
            NSMutableDictionary *samp = (NSMutableDictionary*)[self.rawWave peekFirst];
            
            while ((samp != nil) && ([[samp objectForKey:@"sampleTime"] doubleValue] < ago))           // (snum holds a double tho?)
            {
                [self.rawWave removeFirst]; //remove obsolete sample
                samp = (NSMutableDictionary*)[self.rawWave peekFirst];
            }
        }
        
    }
    @catch (NSException *exception)
    {
//        NSLog(@"addSample exception name: %@ reason: %@", [exception name],[exception reason]);
        // just eat it I guess
    }
}


- (NSMutableArray*)createRangeListObj
{
//    CMDLOG;
    
    NSMutableArray *rl = [NSMutableArray arrayWithArray:rangeList];
    
    /* not needed over here...
     NSMutableArray *rl = [NSMutableArray arrayWithCapacity:10];      //empty mutable array == new JSONArray()
     for (int i=0; i < [rangeList count]; i++)
     {
        [rl addObject:[rangeList objectAtIndex:i]];
     }     
     */
    
     return rl;

}

- (NSString*)MassageRange:(NSString *)range
{
//    CMDLOG;
    
    if ([self SameStr:range str2:@"40 mVDC 10Mohm"])
        range = @"R0,0";
    else if ([self SameStr:range str2:@"400 mVDC 10Mohm"])
        range = @"R0,1";
    else if ([self SameStr:range str2:@"5.7 VDC 400Mohm"])
        range = @"R0,2";
    else if ([self SameStr:range str2:@"40 VDC  75Mohm"])
        range = @"R0,3";
    else if ([self SameStr:range str2:@"57 VDC  75Mohm"])
        range = @"R0,4";
    else if ([self SameStr:range str2:@"57 VDC  400Mohm"])
        range = @"R0,5";
    else if ([self SameStr:range str2:@"400 VDC 75Mohm"])
        range = @"R0,6";
    else if ([self SameStr:range str2:@"570 VDC 75Mohm"])
        range = @"R0,7";
    else if ([self SameStr:range str2:@"40 VAC  75Mohm"])
        range = @"R0,8";
    else if ([self SameStr:range str2:@"400 VAC 75Mohm"])
        range = @"R0,9";
    return range;
}

- (NSString*)MassageStyle:(NSString *)style
{
//    CMDLOG;
    
    return style;
}

- (NSString*)MassageACreject:(NSString *)acreject
{
//    CMDLOG;
    
    if ([self SameStr:acreject str2:@"60Hz AC rejection"])
        acreject = @"A0,0";
    else if ([self SameStr:acreject str2:@"50Hz AC rejection"])
        acreject = @"A0,1";
    else if ([self SameStr:acreject str2:@"30Hz AC rejection"])
        acreject = @"A0,2";
    else if ([self SameStr:acreject str2:@"25Hz AC rejection"])
        acreject = @"A0,3";
    else if ([self SameStr:acreject str2:@"16.6Hz AC rejection"])
        acreject = @"A0,4";
    else if ([self SameStr:acreject str2:@"No AC rejection"])
        acreject = @"A0,5";
    return acreject;
}


- (double)parseVolts:(NSString *)s
{
//    CMDLOG;
    
    double rez;
    
    if ([s contains:@"OVER"])
        rez = VNAN;
    else
    {
        @try
        {
            NSArray *spl = [s componentsSeparatedByString:@" "];   //separate value from units string
            rez = [[spl objectAtIndex:0] doubleValue];
            if ([[spl objectAtIndex:1] hasPrefix:@"m"])
                rez /= 1000.0; //millivolts!
        }
        @catch (NSException *exception) {
//            NSLog(@"parseVolts exception name: %@ reason: %@", [exception name],[exception reason]);
            rez = INFINITY;
        }
    }
    return rez;
}

//Cocoa timeIntervalSince1970 is in seconds so need to convert to milliseconds http://stackoverflow.com/a/13891401
//could use CFAbsoluteTimeGetCurrent(), gettimeofday(), mach_absolute_time(), CACurrentMediaTime() https://developer.apple.com/library/mac/qa/qa1398/_index.html
//MSM except CACurrentMediaTime and some others purportedly do not update when screen blanked!

- (NSDate*)parseVoltsTime:(NSString *)tm
{
//    CMDLOG;
    
    if ([tm length] < 3)
        return nil;         //dont know what to do
    
    NSString *time = [tm substringToIndex:[tm length] - 3];	// remove " ms" //**\\ ??????
    double d = [time doubleValue];        // millisecond counter
    double ms = round(d);					// remove fractional part of ms
/**/
    now = [NSDate date];
    double nowMS = [now getTime];
    // if no pulse, or more than 20 seconds since pulse was updated (i.e at start, or after problem)
    if (resetPulse || gpsPulse == 0 || (self.lastPulse == nil) || (nowMS - [self.lastPulse getTime] > 20*1000L)) {
        // set either right now, or use prior gpsDiff to make best approximation (latency makes this inaccurate)
        gpsPulse = nowMS - gpsDiff - ms;
        self.lastPulse = now;
        gpsUsePPS = false;
        resetPulse = false;
        @synchronized (self.rawWaveMutex) {
            [self.rawWave removeAllObjects];	// otherwise we can have reading left from prior start
        }
    }
    else if (ms < lastMS) {
        // look for weird relative reading jumps
        if (lastMS < 1000L - 5*readFreq) {
            // probably an error in ms reading - so skip it
//            NSLog(@"error in MS! %f",ms);
            return nil;
        }
        // relative MS time has wrapped
        if (lastMS > 1000L) {					// strangely 1000 *is* acceptable
            // no pulse, cycle is 18204ms
            if (lastMS < 18204L - 5*readFreq) {
                // probably just got (initial) GPS synch, but @G2 will not arrive for 150ms - during which MS values are relative to unknown pulse
//                NSLog(@"GPS probably coming!");
                gpsPulse = nowMS - gpsDiff - ms;
                @synchronized(rawWaveMutex) {
                    [rawWave removeAllObjects];
                }
            } else {
                gpsPulse += 18204L;
                double t = nowMS - gpsDiff - ms;
                if (fabs(gpsPulse - t) > 1000L) {
                    // gpsPulse has drifted more than 1 second so correct - not ideal!
//                    NSLog(@"correcting drift!");
                    gpsPulse = t;
                }
            }
            self.lastPulse = now;
            gpsUsePPS = false;
        } else {
            // add 1 second to pulse - which will be read with @G2 (to match) in 150ms or so
            gpsPulse += 1000L;
        }
    }
    dtt = [[NSDate dateWithTimeIntervalSince1970:(gpsPulse + ms)/1000] copy];
  	if ((priorDt != nil) && [dtt getTime] < [priorDt getTime]) {
        // this is likely when first getting GPS pulse which is why prior data is discarded
//        NSLog(@"went backwards!");
    }
    //DebugOutput(dt, ms);
    priorDt = dtt;
    lastMS = ms;
    
/**/

/*
    if (ms >= 1000) {
        gpsUsePPS = false;           //GPS demonstrably off or without a single satellite if this happens
    }
    
    if (platformMS == 0 || ms < lastMS) {
        long t = new Date().getTime();
        if (true) {
            if (platformMS == 0 || lastPlatformMS == 0 || t - lastPlatformMS > 1500) {
                platformMS = t;
                readMS = ms;
            } else {
                platformMS += 1000;
            }
            lastPlatformMS = t;
        }
        else {
            // equivalent of old version - using ms instead of date
            platformMS = t;
            readMS = ms;
        }
    }
    lastMS = ms;
    Date dt = new Date(platformMS + ms - readMS - gpsDiff); //should usually be adequate except for GPS Sync purposes
 
    if (gpsUsePPS && (self.gpsDate != nil)) {
        //we need to be *very* exact here in order for the sync-on/off to work
        //during gps PPS sync, the ms is the EXACT ms of the reading within that reading's second
        //expectation for dt is that it will only potentially be a few ms off from the reading time
        //so algorithm would be to check if abs(dt.ms - ms) > 10 (perhaps?) and adjust +-1 second depending on which ms value is greater
        //and then force dt.ms = ms of course
        double ms_diff = fabs((fmod([dtt getTime], 1000L)) - ms);            //gulp...
        if (ms_diff > 10L)
        {
            if (ms < fmod([dtt getTime], 1000L))
            { // wrap to next second
                dtt = [dtt dateByAddingTimeInterval:1];
            }
            double ddt = [dtt getTime];
            dtt = [NSDate dateWithTimeIntervalSince1970:(ddt + ms)/1000];
        }
        // a cloud of questions hovers over this
    }
*/
    return dtt;
}


- (double)getMidnightMillisecs
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [calendar setTimeZone:timeZone];    //sets up for current date/time
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:[NSDate date]];
    
    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    //  cal.set(Calendar.MILLISECOND, 0); no equivalent here
    
    // Convert back
    NSDate *midnight = [calendar dateFromComponents:dateComps]; //beginningOfDay
    
    return [midnight getTime];
    
}

- (double)getMillisecsSinceMidnight:(NSDate*)dt
{
//    CMDLOG;
    
    return [dt getTime] - [self getMidnightMillisecs];
}


- (NSString*)EncodeISODate:(NSDate*)dt
{
//    CMDLOG;
    
     
//    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
//    return fmt.format(dt);
    
    NSDateFormatter *dft = [[[NSDateFormatter alloc] init] autorelease];
//    [dft setDateFormat:@"yyyy/MM-dd HH:mm:ss.SSSZZZZZ"];
    [dft setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
 
 //   [dft setDateFormat:@"yyyy/MM/dd HH:mm:ss"]; this one works
    
    NSString *isod = [dft stringFromDate:dt];
   
    
//    NSLog(@"EncodeISODate: %@",isod);
    
    /*
  //  isod = @"2001-01-31 12:00:00 AM";
    
    JSContext *context = [[JSContext alloc] initWithVirtualMachine:[[JSVirtualMachine alloc] init]];
//    context[@"a"] = isod;
    context[@"a"] = [NSNumber numberWithDouble:[dt timeIntervalSince1970]];
    [context evaluateScript:@"var square = function(x) {return new Date(x);}"];
    JSValue *squareFunction = context[@"square"];
    NSLog(@"%@", squareFunction);
    JSValue *aSquared = [squareFunction callWithArguments:@[context[@"a"]]];
    NSLog(@"new Date: %@", aSquared);
     */
    
    return isod;
}

- (NSDate*)parseGPSTime:(NSString*)hms :(NSString*)dmy
{
//    CMDLOG;
    
    NSMutableArray *tm = [[hms componentsSeparatedByString:@"."] mutableCopy];
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [cal setTimeZone:timeZone];    //sets up for current date/time
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps =
    [cal components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
    
    // Set the time components manually - all NSInteger
    //from http://uplink.to/7n4  set (int year, int month, int day, int hourOfDay, int minute, int second)
    
//    NSLog(@"parseGPSTime hms: %@ dmy :%@",hms,dmy);
    
    // Day, week, weekday, month, and year numbers are generally 1-based http://uplink.to/b0q
    
    [dateComps setYear:  [[dmy substringWithRange:NSMakeRange(4,2)] integerValue] + 2000];
    [dateComps setMonth: [[dmy substringWithRange:NSMakeRange(2,2)] integerValue]];
    [dateComps setDay:   [[dmy substringWithRange:NSMakeRange(0,2)] integerValue]];
    [dateComps setHour:  [[[tm objectAtIndex:0] substringWithRange:NSMakeRange(0,2)] integerValue]];
    [dateComps setMinute:[[[tm objectAtIndex:0] substringWithRange:NSMakeRange(2,2)] integerValue]];
    [dateComps setSecond:[[[tm objectAtIndex:0] substringWithRange:NSMakeRange(4,2)] integerValue]];
    
    int i = [[tm objectAtIndex:1] length]; int j = 1;
    
    while (i > 0) { j *= 10; i--; }
    
    // [dateComps setMillisecond:[[tm objectAtIndex:1] integerValue]*1000/j];       //except there is no setMillisecond, will revisit withtimeIntervalSince1970
    
    NSDate *gpsDt = [cal dateFromComponents:dateComps]; //no cal.set or cal.getTime equiv in Cocoa
    
    return gpsDt;
}

/*
- (double)parseLatLong:(NSString*)s :(NSString*)hemi
{
    CMDLOG;
    
    NSString *min;
    double i = 1.0;

    NSMutableArray *spl = [[s componentsSeparatedByString:@"."] mutableCopy];
    
    if (!spl) return INFINITY;      //Exception

    NSString *deg = [spl objectAtIndex:0];
    
    if ([hemi isEqualToString:@"W"] || [hemi isEqualToString:@"E"])
    {
        min = [deg substringFromIndex:3];
        deg = [deg substringWithRange:NSMakeRange(0,3)];
    }
    else
    {
        min = [deg substringFromIndex:2];
        deg = [deg substringWithRange:NSMakeRange(0,2)];
    }

    if ([hemi isEqualToString:@"W"] || [hemi isEqualToString:@"S"])
    {
        i = -1.0;
    }
    
    min = [NSString stringWithFormat:@"%@.%@",min, [spl lastObject]];
    
    return i * ([deg doubleValue] + ([min doubleValue]/60.0));
}
*/

- (double)parseLatLong:(NSString*)s :(NSString*)hemi
{
//    CMDLOG;
    
//    NSLog(@"parseLatLong:%@ %@",s,hemi);
    
    NSString *min;
    double i = 1.0;
    
    NSMutableArray *spl = [[s componentsSeparatedByString:@"."] mutableCopy];
    
    if ([spl count] < 2)
        return 0;   //don't know what to do...
    
    NSString *deg = [spl objectAtIndex:0];
    
    if ([hemi isEqualToString:@"W"] || [hemi isEqualToString:@"E"])
    {
        min = [deg substringFromIndex:3];
        deg = [deg substringWithRange:NSMakeRange(0,3)];
    }
    else
    {
        min = [deg substringFromIndex:2];
        deg = [deg substringWithRange:NSMakeRange(0,2)];
    }
    
    if ([hemi isEqualToString:@"W"] || [hemi isEqualToString:@"S"])
    {
        i = -1.0;
    }
    
    min = [NSString stringWithFormat:@"%@.%@",min, [spl objectAtIndex:1]];
    
    return i * ([deg doubleValue] + ([min doubleValue]/60.0));
}


/**
 * @category Complex Reading Types
 */
- (BOOL)updateComplexReading
{
//    CMDLOG;
    
    NSMutableArray *wave;
    
    @synchronized(self.rawWaveMutex)
    {
        if ([rawWave count] == 0) return NO;
        wave = [NSMutableArray arrayWithArray:self.rawWave]; //copy waveform
    }
    
    //update database for on/off or min/max reading styles
    
    int i = [wave count] - 1;
    
    double ago = [[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue] - (msOn + msOff); //1 cycle ago
    
    //Do some initial statistics
    int sample_count = 0;
    double min = INFINITY;	// indicating undefined - easier to detect when no values was not OVER
    double max = INFINITY;
    double v;
    do
    {
        sample_count++;
        v = [[[wave objectAtIndex:i] objectForKey:@"volts"] doubleValue];
        if (v == VNAN) {
            // capture any OVER as NaN
            max = VNAN;
        } else {
            if (!(max == VNAN)) {
                if ((max == INFINITY) || (fabs(v) > fabs(max))) {
                    max = v;
                }
            }
            if ((min == INFINITY) || (fabs(v) < fabs(min))) {
                min = v;
            }
        }
        i--;
    } while ((i >= 0) && ([[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue] >= ago));
    if (min == INFINITY) min = max;
    
    int first_sample = i + 1;
    voltsOn = max;
    voltsOff = min; //minmax read is default if on/off cannot be determined for any reason (i.e. return false).
    self.onTime = self.sampleTime;
    self.offTime = self.sampleTime;
    
    if ((voltsOn == INFINITY) || (voltsOff == INFINITY)) {
//        NSLog(@"MIN/MAX Infinite Values");
    }
    
    /*
     String prog;
     prog = String.format("%d: i=%d, j=%d, k=%d, n=%d, xoff=%d", 1, i, j, k, n, xoff);
     */
    
    if (vmReadStyle == 2)
    {
        //just want MinMax reading
        
        if ((min == INFINITY) || (max != INFINITY)) {
//            NSLog(@"MIN/MAX Infinite Min");
        }
    
        return YES;
    }
    
    if (vmReadStyle == 1)
    {
        if (max == VNAN) return NO;     // just use Max/Min
        
        //check to see if we have a full cycle's worth of readings
        if ((i < 0) && ([[[wave objectAtIndex:0] objectForKey:@"sampleTime"] doubleValue] > ago)) return NO; //not enough samples
        
        if (gpsSynchOnOff && gpsUsePPS && (self.gpsDate != nil))
        {
            //GPS-Synchronized on/off as indicated by desire to use pulse-per-second from the GPS
            if (self.gpsDate == nil)
                return NO; //default to min/max
            
            double tm = [[[wave objectAtIndex:[wave count]-1] objectForKey:@"sampleTime"] doubleValue]; //time of latest sample
            downbeatLatest = [self getMillisecsSinceMidnight:[NSDate date]] * 1000 / downbeatTime + [self getMidnightMillisecs];
            cycleStart = downbeatLatest;
            
            //find latest cycle
            while (cycleStart+msOn+msOff < tm)
            {
                cycleStart += msOn+msOff;
            }
            
            i = first_sample;
            
            if ((cycleStartsWithOn && (cycleStart+msOn+offDelay > tm)) || (!cycleStartsWithOn && (cycleStart+msOff+onDelay > tm)))
            {
                cycleStart -= msOn+msOff; //need to utilize previous cycle
                i = 0; //might as well start at the beginning of queue for lack of a better place
            }
            
            //find cycle start in the data
            double tgt_time = cycleStart+offDelay;
            if (cycleStartsWithOn)
                tgt_time = cycleStart+onDelay;
            
            while ((i < [wave count]) && ([[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue] < tgt_time)) i++;
            
            if (i == [wave count])
            {
                return NO; //should be impossible
            }
            
            if (cycleStartsWithOn)
            {
                voltsOn = [[[wave objectAtIndex:i] objectForKey:@"volts"] doubleValue];
                self.onTime =  [NSDate dateWithTimeIntervalSince1970:[[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue]/1000];
                tgt_time = tgt_time - onDelay + msOn + offDelay;
            }
            else
            {
                voltsOff = [[[wave objectAtIndex:i] objectForKey:@"volts"] doubleValue];
                self.offTime = [NSDate dateWithTimeIntervalSince1970:[[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue]/1000];
                tgt_time = tgt_time - offDelay + msOff + onDelay;
            }
            
            while ((i < [wave count]) && ([[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue] < tgt_time)) i++;
            
            if (cycleStartsWithOn)
            {
                voltsOff = [[[wave objectAtIndex:i] objectForKey:@"volts"] doubleValue];
                self.offTime = [NSDate dateWithTimeIntervalSince1970:[[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue]/1000];
            }
            else
            {
                voltsOn = [[[wave objectAtIndex:i] objectForKey:@"volts"] doubleValue];
                self.onTime = [NSDate dateWithTimeIntervalSince1970:[[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue]/1000];
            }
            
        }
        else
        {
            @try
            {
                //Bitlizard's world-famous Algorithmic on/off
                double enough_time = [[[wave objectAtIndex:[wave count]-1] objectForKey:@"sampleTime"] doubleValue] - round(1.8*(msOn+msOff)); //need this much time to use algorithm
                if ([[[wave objectAtIndex:0] objectForKey:@"sampleTime"] doubleValue] >= enough_time)
                {
                    return NO;  //need more samples
                }
                
//                NSLog(@"ALG-ONOFF Samples per cycle = %d", sample_count);
                
                double midway = (max + min) / 2.0; //pray for enough IR
                
                //compute inter-read delta and min/max for upper and lower values
                double *diff = malloc([wave count] * sizeof(double));
                double *vals = malloc([wave count] * sizeof(double));
                double v1;
                double v2;
                double max_vol = 0;
                int hi_count = 0; //the rule is that on time period has to be at least 2x the off period
                int lo_count = 0; //so we use these counts to check for voltage inversion
                i = [wave count] - 1;
                do
                {
                    v1 = [[[wave objectAtIndex:i] objectForKey:@"volts"] doubleValue];
                    v2 = [[[wave objectAtIndex:i-1] objectForKey:@"volts"] doubleValue];
                    diff[i] = fabs(v1-v2);
                    vals[i] = v1;
                    if (diff[i] > max_vol) max_vol = diff[i];
                    if (fabs(v1) > fabs(midway))
                    {
                        hi_count++;
                    }
                    else
                    {
                        lo_count++;
                    }
                    i--;
                } while ((i > 0) && (i >= first_sample));
                //theory of operation
                //  Since the rule is that the on period should be 2x the off period, there should be twice as
                //  many readings above midway as below. We can do a sanity check like this:
                BOOL inverted = (lo_count > hi_count); //yes <sigh> can become inverted sometimes
                
                if (inverted)
//                    NSLog(@"ALG-ONOFF cycle samples appear inverted");
                
                if ((inverted) && (hi_count > lo_count/2.0))
                    return NO;
                else
                    if ((!inverted) && (lo_count > hi_count/2.0))
                        return NO;
//                NSLog(@"ALG-ONOFF cycle sample pass the sanity test. Max volatity = %f", max_vol);
//                NSLog(@"ALG-ONOFF High count = %d Low count = %d",  hi_count, lo_count);
                //theory continued -- we have enough IR and enough confidence to go further. Look for 2 periods
                //  of high volatility after a relative long period of much lower volatility. These will be the
                //  transitions
                double vd = 6.0; //voodoo number = ratio between high volatility and low (started out 4-to-1)
                
                int xOff = -1;
                i = [wave count] - 1;
                while ((i >= 0) && (diff[i] > max_vol/vd)) i--; // find low volatility
                if (i == [wave count] - 1)
                {
//                    NSLog(@"ALG-ONOFF tail end of samples was in stable area");
                }
                int j = i - 1;
//not sure if we need or want this but keeping it here just to preserve the thought
//                while ((j >= first_sample) && (diff[j] > max_vol/4.0)) { i--; j--; } //skip 1-off glitches
//                if (j <= first_sample) return false; //too noisy
                while ((j > 0) && (diff[j] < max_vol/vd)) j--; // how long does it last;
                //found our first transition
                int k = j;
                j++;
                while ((k >= 0) && (diff[k] > max_vol/vd)) k--; // skip to transition start
//                NSLog(@"ALG-ONOFF Start of first transition at %i", k-[wave count]);
                int n = k-1; //k has read before start of latest transition
                while ((n >= 0) && (diff[n] < max_vol/vd)) n--; // find end of stable period
                while ((n >= 0) && (diff[n] > max_vol/vd)) n--; // find start of the transition
                if (n < 0) return NO; //don't think this will ever happen in real world, but...
                //should have enough info to determine where transitions are now
//                NSLog(@"ALG-ONOFF Start of prev transition at %i", n-[wave count]);
                //sanity check - make sure we didn't miss a transition in the middle
                if (k-n+1 >= sample_count-1)
                {
                    //detected nearly a full cycle between the 2 discovered transitions.
                    //Android version tries to compensate for this noisy data... think it might be better to just punt to min/max
                    //return NO;
//                    NSLog(@"ALG-ONOFF taking remedy for missed transition");
                    if (fabs([[[wave objectAtIndex:n] objectForKey:@"volts"] doubleValue]) > fabs(midway)) //unfortunately need to take a clue from the voltage
                    {
                        xOff = n;
                    }
                    else
                    {
                        //hunt forward for a sample after the transition time plus the msOn time
//                        NSLog(@"ALG-ONOFF - complicated remedy for mising transition");
                        double xoff_time = [[[wave objectAtIndex:n] objectForKey:@"sampleTime"] doubleValue] + msOn;
                        xOff = n+1;
                        while ((xOff < k) && ([[[wave objectAtIndex:xOff] objectForKey:@"sampleTime"] doubleValue] < xoff_time)) xOff++;
                        //now kludge things so a sample 150ms forward will get chosen
                        xoff_time += 150;
                        j = xOff+1;
                        while ((j < k) && ([[[wave objectAtIndex:j] objectForKey:@"sampleTime"] doubleValue] < xoff_time))
                        {
                            diff[j] = max_vol / (vd - 1.0);
                            j++;
                        }
                        diff[j] = 0.0;
                    }
                }
                else if ([[[wave objectAtIndex:k] objectForKey:@"sampleTime"] doubleValue] - [[[wave objectAtIndex:n] objectForKey:@"sampleTime"] doubleValue] < msOff/2)
                {
                    //failed sanity check for long enough inter-transition time
                    return NO;
                }
                else if (k-n+1 < sample_count/2)
                {
                    //n traversed off part of cycle
                    xOff = n; //mark last sample index before transition to off
                }
                else
                {
                    //n traversed on part of cycle
                    xOff = k;
                }
//                NSLog(@"ALG-ONOFF Transition to off starts at %i", xOff-[wave count]);
                //Yeehaw, gonna produce an on/off here
                j = xOff+1; //should point to first reading in transition to off
                while ((xOff > 0) && (diff[xOff] > diff[xOff-1]*2.0)) xOff--; //just in case pick prior read if substantially less volatile
                voltsOn = [[[wave objectAtIndex:xOff] objectForKey:@"volts"] doubleValue];
                self.onTime = [NSDate dateWithTimeIntervalSince1970:[[[wave objectAtIndex:xOff] objectForKey:@"sampleTime"] doubleValue]/1000];
                //forward from the xOff... find where volatility flattens out
                while ((j < [wave count]) && (diff[j] > max_vol/vd)) j++;
                if (j < [wave count] - 1)
                    while ((j < [wave count]-1) && (diff[j] > diff[j+1]*2.0)) j++; //just in case my 25% volatility threshold is too far up the curve
                if (j > [wave count] - 1)
                    j = [wave count] - 1;
                voltsOff = [[[wave objectAtIndex:j] objectForKey:@"volts"] doubleValue];
                self.offTime = [NSDate dateWithTimeIntervalSince1970:[[[wave objectAtIndex:j] objectForKey:@"sampleTime"] doubleValue]/1000];
                
                free(diff);     //prior early returns will leak these.
                free(vals);
                
                //Update cycle start
                if (cycleStartsWithOn)
                    cycleStart = [[[wave objectAtIndex:k+1] objectForKey:@"sampleTime"] doubleValue];
                else
                    cycleStart = [[[wave objectAtIndex:n+1] objectForKey:@"sampleTime"] doubleValue];
                return YES; //SUCCESS!
            }
            @catch (NSException *exception) {
//                NSLog(@"updateComplexReading exception name: %@ reason: %@", [exception name],[exception reason]);
                return NO;
            }
        }
    }
    
    if (vmReadStyle == 3)
    {
        //Predominant read
        double *vals = malloc(sample_count * sizeof(double));
        int *hsto = malloc(sample_count * sizeof(int));
        int distinct = 0;
        int j;
        double d;
        for (i=first_sample; i < [wave count]; i++)
        {   //build histo
            d = [[[wave objectAtIndex:i] objectForKey:@"volts"] doubleValue];
            j = distinct-1;
            while ((j>=0) && (vals[j] != d)) j--;
            if (j < 0)
            {
                vals[distinct] = d;
                hsto[distinct] = 1;
                distinct++;
            }
            else hsto[j]++;
        }
        //find largest count;
        int pd = 0;
        for (j=1; j<distinct; j++)
            if (hsto[j] >= hsto[pd]) pd = j; // yes, >= is intentional figuring later is better
        voltsOn = vals[pd];
        voltsOff = INFINITY;
        self.onTime  = self.sampleTime; //just use time from latest sample
        
        free(hsto);
        free(vals);
        return YES;
    }
    return false;
}

- (NSMutableDictionary*)createWaveFormObj
{
//    CMDLOG;
    
    NSMutableDictionary *wf = [NSMutableDictionary dictionaryWithCapacity:10];      //empty dictionary == new JSONObject()
    
    //see if we have a cycle's worth of data
    if (msOn+msOff < 1000)
        return nil; //not a valid cycle
    
    NSMutableArray *wave;
    
    @synchronized(self.rawWaveMutex)
    {
        if ([self.rawWave count] == 0) return nil;      //no samples
        wave = [NSMutableArray arrayWithArray:self.rawWave]; //copy waveform
    }
    
    int i = [wave count] - 1;
    
    @try
    {
        double ago = [[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue] - (msOn + msOff); //1 cycle ago
        while ((i>=0) && ([[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue] > ago)) i--;
        
        if (i < 0)
            return nil; //not enough data yet
        
        i++;
        
        NSMutableArray *samples = [NSMutableArray arrayWithCapacity:100];
        
        while (i < [wave count])
        {
            [samples addObject:[wave objectAtIndex:i]];
            i++;
        }
        
        [wf setObject:[NSNumber numberWithInt:msOn] forKey:@"msOn"];
        [wf setObject:[NSNumber numberWithInt:msOff] forKey:@"msOff"];
        [wf setObject:[NSNumber numberWithDouble:[[NSDate dateWithTimeIntervalSince1970:[[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue]/1000] getTime]] forKey:@"startTime"];
        [wf setObject:[NSNumber numberWithDouble:[[NSDate dateWithTimeIntervalSince1970:[[[wave objectAtIndex:[wave count]-1] objectForKey:@"sampleTime"] doubleValue]/1000] getTime]] forKey:@"stopTime"];
        [wf setObject:samples forKey:@"samples"];
    }
    @catch (NSException *exception)
    {
//        NSLog(@"createWaveFormObj exception name: %@ reason: %@", [exception name],[exception reason]);
        return nil;
    }
    
    return wf;
}

- (NSMutableDictionary*)formalWaveFormObj   //relies on cycle calculations done in UpdateComplexReading
{
//    CMDLOG;
    
    NSMutableDictionary *wf = [NSMutableDictionary dictionaryWithCapacity:10];      //empty dictionary == new JSONObject()
    
    //see if we have a waveform's worth of data
    if (msOn+msOff < 1000) return nil; //not a valid cycle
    double tgtTime = cycleStart;
    NSMutableArray *wave;
    @synchronized(self.rawWaveMutex)
    {
        if ([self.rawWave count] == 0) return nil;      //no samples
        wave = [NSMutableArray arrayWithArray:self.rawWave]; //copy waveform
    }
    int i = [wave count] - 1;
    
    @try
    {
        while ((i>=0) && ([[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue]  > tgtTime)) i--;
        if (i < 0) return nil; //not enough data yet
        i++;
        
        NSMutableArray *samples = [NSMutableArray arrayWithCapacity:100];
        
        while ((i < [wave count]) && ([[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue] < tgtTime+msOn+msOff))
        {
            [samples addObject:[wave objectAtIndex:i]];
            i++;
        }
        
        [wf setObject:[NSNumber numberWithInt:msOn] forKey:@"msOn"];
        [wf setObject:[NSNumber numberWithInt:msOff] forKey:@"msOff"];
        [wf setObject:[NSNumber numberWithDouble:[[NSDate dateWithTimeIntervalSince1970:[[[wave objectAtIndex:i] objectForKey:@"sampleTime"] doubleValue]/1000] getTime]] forKey:@"startTime"];
        [wf setObject:[NSNumber numberWithDouble:[[NSDate dateWithTimeIntervalSince1970:[[[wave objectAtIndex:[wave count]-1] objectForKey:@"sampleTime"] doubleValue]/1000] getTime]] forKey:@"stopTime"];
        [wf setObject:samples forKey:@"samples"];
    }
    @catch (NSException *exception)
    {
//        NSLog(@"formalWaveFormObj exception name: %@ reason: %@", [exception name],[exception reason]);
        return nil;
    }
    
    return wf;
}


/**
* @category Protocol Statement ParserBOOL
*/

- (BOOL)parseStatement:(NSString*)result
{
//    CMDLOG;
    
    if ([result isEmpty])
    {
//         NSLog(@"Parse Statement empty");
        return false;
    }
    
//    NSLog(@"parsing: %@",result);
    
    NSString *crc = [self getCRC:result];
    int c = [result indexOf:@"*"];
    NSString *crc2 = [result substringFromIndex:c+1];
    if (![crc isEqualToString:crc2]) {
//        NSLog(@"CRC error %@: - %@", result, crc);
        return false;
    }
    
    if ([result hasPrefix:@"@V"] || [result hasPrefix:@"@C"])
    {
        if ([result hasPrefix:@"@V"]) {
//            NSLog(@"iBTVM callback: retrieve volts function (V)");
        }
        
        if ([result hasPrefix:@"@C"]) {
            
//            NSLog(@"iBTVM callback: retrieve volts function continuously (C)");
//            NSLog(@"Logging number of updates for volts: %i", ++numberOfUpdates); // A'sa Andrew created this variable
        }
        
        
        int i = [result indexOf:@"*"];      //custom NSString extension
        splitArray = [[result substringWithRange:NSMakeRange(3,i)] componentsSeparatedByString:@","];
        volts       = [self parseVolts:[splitArray objectAtIndex:0]];
        self.sampleTime  = [self parseVoltsTime:[splitArray objectAtIndex:1]];
        button1     = [[splitArray objectAtIndex:2] isEqualToString:@"0"];
        button2     = [[splitArray objectAtIndex:3] isEqualToString:@"0"];
        if (vmReadStyle == 0)
        {
            voltsOn = volts;
            self.onTime  = self.sampleTime;
        }
        if (self.sampleTime != nil) [self addSample];
        return YES;
    }
    else if ([result hasPrefix:@"@S"])
    {
        NSLog(@"iBTVM callback: retrieve status function (Sx)");

        splitArray = [result componentsSeparatedByString:@","];
        self.firmware    = [splitArray objectAtIndex:1];
        self.serial      = [splitArray objectAtIndex:2];
        self.battery     = [splitArray objectAtIndex:4];
        gps_power   = [[splitArray objectAtIndex:6] hasSuffix:@" ON"];
        sats        = [[splitArray objectAtIndex:8] intValue];
        vm_power    = [[splitArray objectAtIndex:9] hasSuffix:@" ON"];
        vm_continuous = [[splitArray objectAtIndex:10] hasSuffix:@" CO"];
        self.lastStatusUpdate = [[NSDate date] retain];
        return YES;
    }
    else if ([result hasPrefix:@"@G3"])
    {
//        NSLog(@"iBTVM callback: manage GPS function (G3)");

        splitArray = [result componentsSeparatedByString:@","];
        latitude    = [self parseLatLong:[splitArray objectAtIndex:1] :[splitArray objectAtIndex:2]];
        longitude   = [self parseLatLong:[splitArray objectAtIndex:3] :[splitArray objectAtIndex:4]];
        self.lastSimpleGPS = [[NSDate date] retain];
        self.lastGPS = self.lastSimpleGPS;
        
        if (self.lastFullGPS == nil || [self.lastSimpleGPS getTime] - [self.lastFullGPS getTime] > maxFixInterval)
            self.gpsFixType = @"fix";
        
        return YES;
    }
    else if ([result hasPrefix:@"@G2"])
    {
//        NSLog(@"iBTVM callback: manage GPS function (G2)");

        splitArray = [[result componentsSeparatedByString:@","] mutableCopy];
        
        if (self.gpsDate == nil) {
            // we are going to get a jump, so discard prior waveform data
            @synchronized(rawWaveMutex) {
                [rawWave removeAllObjects];
            }
        }
        self.gpsDate = [self parseGPSTime:[splitArray objectAtIndex:1] :[splitArray objectAtIndex:2]];
        gpsPulse    = [self.gpsDate getTime];
        self.lastPulse   = [[NSDate date] retain];
        gpsDiff     = [self.lastPulse getTime] - gpsPulse;      
        gpsUsePPS = true;
        
        latitude    = [self parseLatLong:[splitArray objectAtIndex:3] :[splitArray objectAtIndex:4]];
        longitude   = [self parseLatLong:[splitArray objectAtIndex:5] :[splitArray objectAtIndex:6]];
        
        // these probably would resolve out to 0 ( http://nshipster.com/nil/ ) anyways but preserving the Android ways for clarity
        
        if ([(NSString*)[splitArray objectAtIndex:7] isEmpty])
            altitude = 0;
        else
            altitude = [[splitArray objectAtIndex:7] doubleValue];
        
        if ([(NSString*)[splitArray objectAtIndex:8] isEmpty])
            pdop = 0;
        else
            pdop = [[splitArray objectAtIndex:8] doubleValue];
        
        if ([(NSString*)[splitArray objectAtIndex:9] isEmpty])
            hdop = 0;
        else
            hdop = [[splitArray objectAtIndex:9] doubleValue];
        
        if (([(NSString*)[splitArray objectAtIndex:10] isEmpty]) || [(NSString*)[splitArray objectAtIndex:10] length] < 4)
            vdop = 0;
        else
            vdop  = [[[splitArray objectAtIndex:10] substringWithRange:NSMakeRange(0,4)] doubleValue];
        
        if (pdop > 0 && pdop <= 1) self.gpsFixType = @"ideal";
        else if (pdop <= 2) self.gpsFixType = @"excellent";
        else if (pdop <= 5) self.gpsFixType = @"good";
        else if (pdop <= 10) self.gpsFixType = @"moderate";
        else if (pdop <= 20) self.gpsFixType = @"fair";
        else if (pdop > 20)  self.gpsFixType = @"poor";
        else  self.gpsFixType = @"fix";
        
        self.lastFullGPS = [[NSDate date] retain];
        self.lastGPS     = self.lastFullGPS;
        return YES;
    }
    else if ([result hasPrefix:@"@R1"])
    {
        NSLog(@"iBTVM callback: set range function (R1)");
 
        splitArray = [[result substringFromIndex:4] componentsSeparatedByString:@":"];
        vmRange     = [[splitArray objectAtIndex:0] intValue];
        return YES;
    }
    else if ([result hasPrefix:@"@R2"])
    {
        NSLog(@"iBTVM callback: retrieve set range function (R2)");

        int i       = [result indexOf:@"*"];
        rangeList   = (NSMutableArray*)[[result substringWithRange:NSMakeRange(4,i)] componentsSeparatedByString:@","];
        [rangeListDone doNotify];       //drops to unhandled when done or semaphores off somewhere else?
    }
    else if ([result hasPrefix:@"@A1"])
    {
        NSLog(@"iBTVM callback: retrieve AC rejections (A1)");

        splitArray = [[result substringFromIndex:4] componentsSeparatedByString:@":"] ;
        vmACRejection = [[splitArray objectAtIndex:0] intValue];
        return YES;
    }
     
    return NO; //unhandled
}

// MAIN PARSER - Makes some adjustments before things go on the stack
- (NSString *)ParseBTString:(NSString *)s
{
//    CMDLOG;
    
    NSString *result = s;
    
    @try
    {
        //update database
        if (![self parseStatement:s])
        {
//        NSLog(@"Failure in ParseStatment %@",s);
            return @"";
        }
    }
    @catch (NSException *exception)
    {
//        NSLog(@"ParseBTString: Exception in parse statement %@ - name: %@ reason: %@", s, [exception name],[exception reason]);
        return result;
    }
    
    //hit the callback if this is a continuous reading report
    
    CDVPluginResult* pluginResult = nil;
    if (!simpleCallback && (self.jsCallbackContext != nil) && [result hasPrefix:@"@C"])
    {
        @try
        {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:(NSDictionary*)[self createReadingObj:YES:NO:0:0]];
            if (pluginResult)
            {
                [pluginResult setKeepCallbackAsBool:YES];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:self.jsCallbackContext];       //don't have command.callbackId right now..asyncContext?
            }
        }
        @catch (NSException *exception)
        {
//            NSLog(@"ParseBTString: Exception creating reading object - name: %@ reason: %@", [exception name],[exception reason]);
            return result;
        }
    }
    
    return result;
}

- (void)doStop
{
    
    if ((self.bt != nil) && isConnected)
    {
        [self.bt flushIncoming];
        [self.bt send:[self formatCommand:@"G0"]];  // stop GPS
        [self.bt send:[self formatCommand:@"X"]];   // stop Continuous
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// 'execute' routines aka
 /**
  * @category Cordova entry points
  */

- (void)listDevices:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;
    
    //command.array is static so we have to make a new one including our param already.
    
    NSArray *args = [NSArray arrayWithObject:@"MCM-iBTVM"];
    
    //and command.array is read only so unlike Android args.put(0, "MCM-iBTVM") we have to make a new command as its the only parameter
    
    CDVInvokedUrlCommand *mycommand = [[CDVInvokedUrlCommand alloc] initWithArguments:args callbackId:command.callbackId className:command.className methodName:command.methodName];

    [super listDevices:mycommand];
    
}

- (void)listRanges:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;
    
    //android does a bunch of thread stuff here - pending porting this until total picture evolves
    
    if (rangeList == nil)
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:(NSDictionary*)[self createRangeListObj]];
        [pluginResult setKeepCallbackAsBool:NO];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
        [self.bt send:[self formatCommand:@"R2"]];       //request range list
    }
    else 
    {   //range list already obtained
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:(NSDictionary*)[self createRangeListObj]];
        [pluginResult setKeepCallbackAsBool:NO];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)initialize:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;

    if (!isInitialized)
    {
        [super doInitialize];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:@"MCM-iBTVM" forKey:@"deviceNamePrefix"];
        [defaults setObject:[defaults objectForKey:@"lastBtvm"] forKey:@"rememberedDevice"];
        [defaults synchronize];
        
        NSMutableDictionary *obj = [NSMutableDictionary dictionaryWithCapacity:10];      //empty dictionary == new JSONObject()
        
        //default value http://uplink.to/bes
        
        NSString *pref = @"";
        
        if ([defaults objectForKey:@"rememberedDevice"])
            pref = [defaults objectForKey:@"rememberedDevice"];
        
        [obj setObject:[defaults objectForKey:@"deviceNamePrefix"]  forKey:@"deviceNamePrefix"];
        [obj setObject:pref forKey:@"rememberedDevice"];
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:obj];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
        isInitialized = YES;
    }
}

- (void)getRange:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;
    
    if (!isInitialized)
    {
        [self cbContextError:command:@"iBTVM getRange isInitialized = NO"];
        return;
    }
    
    if (!isConnected)
    {
        [self cbContextError:command:@"iBTVM getRange action before connect"];
        return;
    }

    NSString *cmd  = [self formatCommand:@"R1"];
    NSString *hdr = [cmd substringWithRange:NSMakeRange(0, 2)];
    
    NSString *rsp = [self.bt sendCmdGetResp:@"" command:cmd header:hdr timeout:1000];
    
    if ([self parseStatement:rsp])
    {
        NSMutableDictionary *obj = [NSMutableDictionary dictionaryWithCapacity:10];      //empty dictionary == new JSONObject()

        [obj setObject:rsp forKey:@"rsp"];
        [obj setObject:@"vmRange" forKey:@"vmRange"];
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:obj];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    else
        [self cbContextError:command:@"No range result"];
    
}

- (void)getACreject:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;
    
    if (!isInitialized)
    {
        [self cbContextError:command:@"iBTVM getACreject isInitialized = NO"];
        return;
    }
    
    if (!isConnected)
    {
        [self cbContextError:command:@"iBTVM getACreject action before connect"];
        return;
    }
    
    NSString *cmd  = [self formatCommand:@"A1"];
    NSString *hdr = [cmd substringWithRange:NSMakeRange(0, 2)];
    
    NSString *rsp = [self.bt sendCmdGetResp:@"" command:cmd header:hdr timeout:1000];
    
    if ([self parseStatement:rsp])
    {
        NSMutableDictionary *obj = [NSMutableDictionary dictionaryWithCapacity:10];      //empty dictionary == new JSONObject()
        
        [obj setObject:rsp forKey:@"rsp"];
        [obj setObject:@"vmACRejection" forKey:@"vmACRejection"];
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:obj];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    else
        [self cbContextError:command:@"No AC Rejection result"];
    
}

- (void)getStatus:(CDVInvokedUrlCommand*)command;
{
//    CMDLOG;
    
    if (!isInitialized)
    {
        [self cbContextError:command:@"iBTVM getStatus action before initialize"];
        return;
    }
    
    if (!isConnected)
    {
        [self cbContextError:command:@"iBTVM getStatus action before connect"];
        return;
    }
    
    BOOL doSendCmd = true;
    if (([command.arguments count] > 0) && ([command.arguments objectAtIndex:0] != [NSNull null])) {
        doSendCmd = [[command.arguments objectAtIndex:0] boolValue];
    }
    if (doSendCmd) {
        // we dont always send command - i.e. at start we keep checking for status to arrive, but only ask once
        NSString *cmd = [self formatCommand:@"S1"];
        [self.bt sendCmd:@"" command:cmd];		// not checking return value - assuming it works!
    }
    
    if (self.lastStatusUpdate != nil)
    {
        NSMutableDictionary *obj = [NSMutableDictionary dictionaryWithCapacity:10];      //empty dictionary == new JSONObject()
        
        [obj setObject:self.firmware forKey:@"firmware"];
        [obj setObject:self.serial forKey:@"serial"];
        [obj setObject:self.battery forKey:@"battery"];
        [obj setObject:[NSNumber numberWithBool:gps_power] forKey:@"gps_power"];
        [obj setObject:[NSNumber numberWithInt:sats] forKey:@"sats"];
        [obj setObject:[NSNumber numberWithBool:vm_power] forKey:@"vm_power"];
        [obj setObject:[NSNumber numberWithBool:vm_continuous] forKey:@"vm_continuous"];
        [obj setObject:[NSNumber numberWithDouble:[self.lastStatusUpdate getTime]] forKey:@"lastStatusUpdate"];       // Invalid type in JSON write (__NSDate)
        [obj setObject:self.bt.device.name forKey:@"name"];
        
//        NSDateFormatter *dft = [[[NSDateFormatter alloc] init] autorelease];
//        [dft setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSSZ"];
//       NSString *statd = [dft stringFromDate:self.lastStatusUpdate];
//        [obj setObject:statd forKey:@"lastStatusUpdate"];
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:obj];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    else
        [self cbContextError:command:@"No Status result"];
}


- (void)diagnostic:(CDVInvokedUrlCommand*)command;
{
//    CMDLOG;
    
    
    if (!isInitialized)
    {
        [self cbContextError:command:@"iBTVM diagnostic action before initialize"];
        return;
    }
    
    if (!isConnected)
    {
        [self cbContextError:command:@"iBTVM diagnostic action before connect"];
        return;
    }
    
    NSMutableDictionary *obj = [NSMutableDictionary dictionaryWithCapacity:10];      //empty dictionary == new JSONObject()
    NSString *rsp, *cmd, *hdr;
    
    cmd  = [self formatCommand:@"G2,0"];
    hdr = [cmd substringWithRange:NSMakeRange(0, 2)];
    rsp = [self.bt sendCmdGetResp:@"" command:cmd header:hdr timeout:5000];
    [obj setObject:rsp forKey:@"gps"];
    
    cmd = [self formatCommand:@"S1"];
    hdr = [cmd substringWithRange:NSMakeRange(0, 2)];
    rsp = [self.bt sendCmdGetResp:@"" command:cmd header:hdr timeout:1000];
    [obj setObject:rsp forKey:@"status"];
    
    if ([self parseStatement:rsp])
    {
        [obj setObject:self.firmware forKey:@"firmware"];
        [obj setObject:self.serial forKey:@"serial"];
        [obj setObject:self.battery forKey:@"battery"];
        [obj setObject:[NSNumber numberWithBool:gps_power] forKey:@"gps_power"];
        [obj setObject:[NSNumber numberWithInt:sats] forKey:@"sats"];
        [obj setObject:[NSNumber numberWithBool:vm_power] forKey:@"vm_power"];
        [obj setObject:[NSNumber numberWithBool:vm_continuous] forKey:@"vm_continuous"];
    }
   
    cmd = [self formatCommand:@"A1"];
    hdr = [cmd substringWithRange:NSMakeRange(0, 2)];
    rsp = [self.bt sendCmdGetResp:@"" command:cmd header:hdr timeout:1000];
    [obj setObject:rsp forKey:@"acreject"];
    
    if ([self parseStatement:rsp])
        [obj setObject:[NSNumber numberWithInt:vmACRejection] forKey:@"vmACRejection"];
    
    cmd = [self formatCommand:@"R1"];
    hdr = [cmd substringWithRange:NSMakeRange(0, 2)];
    rsp = [self.bt sendCmdGetResp:@"" command:cmd header:hdr timeout:1000];
    [obj setObject:rsp forKey:@"range"];
    
    if ([self parseStatement:rsp])
        [obj setObject:[NSNumber numberWithInt:vmRange] forKey:@"vmRange"];
    
    cmd = [self formatCommand:@"V"];
    hdr = [cmd substringWithRange:NSMakeRange(0, 2)];
    rsp = [self.bt sendCmdGetResp:@"" command:cmd header:hdr timeout:1000];
    [obj setObject:rsp forKey:@"voltage"];
    
    if ([self parseStatement:rsp])
    {
        NSMutableDictionary *vobj = [self createReadingObj:NO:NO:0:0];     //CreateReadingObj(false, false); need to update to latest JAVA
        [obj setObject:[vobj objectForKey:@"voltsOn"] forKey:@"voltsOn"];
    }
    
    if (bt.btFailed)
        [self cbContextError:command:@"No diagnostic result"];
    else
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:obj];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)getVoltage:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;
    
    if (!isInitialized)
    {
        [self cbContextError:command:@"iBTVM getVoltage isInitialized = NO"];
        return;
    }
    
    if (!isConnected)
    {
        [self cbContextError:command:@"iBTVM getVoltage before connect"];
        return;
    }

    
    NSString *cmd  = [self formatCommand:@"V"];
    NSString *hdr = [cmd substringWithRange:NSMakeRange(0, 2)];

    NSString *rsp = [self.bt sendCmdGetResp:@"" command:cmd header:hdr timeout:1000];
    
    if ([self parseStatement:rsp])
    {
        NSMutableDictionary *obj = [self createReadingObj:NO:NO:0:0];
        [obj setObject:rsp forKey:@"rsp"];
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:obj];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    else
        [self cbContextError:command:@"No Voltage result"];
}

- (void)getReading:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;
        
//    NSLog(@"Logging got reading from device");
    
    // A'sa Andrew Logging, this method gets called from
    // iBTVMgetReading -> navigator.iBTVM.getReading
    // that last call, calls into this selector from javascript
    
    if (!isInitialized)
    {
        [self cbContextError:command:@"iBTVM getReading isInitialized = NO"];
        return;
    }
    
    if (!isConnected)
    {
        [self cbContextError:command:@"iBTVM getReading before connect"];
        return;
    }

    // A'sa Andrew the min/max is never updated until the method updateComplexReading is called
    // this method is only called from right here... and it sets the vmReadStyle...
    // so this if statement will NEVER succeed....
    // so i put in the line -> "vmReadStyle = [self getReadStyle:[cfg objectForKey:@"ReadStyle"]]"
    
    vmReadStyle = [self getReadStyle:[cfg objectForKey:@"ReadStyle"]];
    if (vmReadStyle > 0)
    {
        if (self.jsCallbackContext == nil) { [self cbContextError:command:@"iBTVM getReading call for complex read while not started"]; return; }
        BOOL noComplex = ![self updateComplexReading];
        
        if (true)
        {
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:[self createReadingObj:YES:noComplex:0:0]];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
        else
        {
            [self cbContextError:command:@"iBTVM getReading cannot compute complex reading"];
        }
        return;
    }
    else
    { //simple reading
        double reqTime = 0; //NOTE - in Java this is a long, could be trouble here
        if (([command.arguments count] > 0) && ([command.arguments objectAtIndex:0] != [NSNull null]))
            reqTime = [[command.arguments objectAtIndex:0] doubleValue]; //NOTE - in Java this is a long, could be trouble here
        long logFreq = 0;
        if (([command.arguments count] > 1) && ([command.arguments objectAtIndex:1] != [NSNull null])) {
            logFreq = [[command.arguments objectAtIndex:1] longValue];
        }
        if (self.jsCallbackContext != nil)
        {
            if (self.onTime != nil)
            {
                CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:[self createReadingObj:YES:NO:reqTime:logFreq]];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }
            else
                [self cbContextError:command:@"iBTVM getReading no reading yet"];
            return;
        }
     	//Unstarted, could conceivably do a V command here and get a single voltage, but...
        [self cbContextError:command:@"iBTVM getReading call to get reading while not started"];
    }
}

- (void)getWaveForm:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;
    
    if (!isInitialized)
    {  
        [self cbContextError:command:@"iBTVM getWaveForm isInitialized = NO"];
        return;
    }
    if (!isConnected)
    {
        [self cbContextError:command:@"iBTVM getWaveForm action before connect"];
        return;
    }
    if (self.jsCallbackContext == nil)
    {
        [self cbContextError:command:@"iBTVM getWaveForm call for waveform while not started"];
        return;
    }
    
    [self updateComplexReading];    //ensure cycle calcs are done
    
    NSMutableDictionary *wf = [self formalWaveFormObj];
    
    if (wf != nil)
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:wf];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    else
    {
        [self cbContextError:command:@"iBTVM getWaveform waveform not available"];
    }
}

- (void)getConfig:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;
    
    if (!isInitialized)
    {
        [self cbContextError:command:@"iBTVM getConfig isInitialized = NO"];
        return;
    }
    
    if (!isConnected)
    {
        [self cbContextError:command:@"iBTVM getConfig before connect"];
        return;
    }

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:self.cfg];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)setConfig:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;
    
    if (!isInitialized)
    {
        [self cbContextError:command:@"iBTVM setConfig isInitialized = NO"];
        return;
    }
    
    if (!isConnected)
    {
        [self cbContextError:command:@"iBTVM setConfig before connect"];
        return;
    }
    
    if ((command.arguments == nil) || ([command.arguments count] == 0))
    {
        [self cbContextError:command:@"iBTVM setConfig with no config object"];
    }

    NSMutableDictionary *new_cfg = [NSMutableDictionary dictionaryWithDictionary:[command.arguments objectAtIndex:0]];
    BOOL res = [self applyConfig:new_cfg];
    
    if (res)
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:cfg];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    else
    {
        [self cbContextError:command:configError];
    }
}

- (void)updateConfig:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;
    
    if (!isInitialized)
    {
        [self cbContextError:command:@"iBTVM updateConfig isInitialized = NO"];
        return;
    }
    
    if (!isConnected)
    {
        [self cbContextError:command:@"iBTVM updateConfig before connect"];
        return;
    }

    
    if ((command.arguments == nil) || ([command.arguments count] == 0))
    {
        [self cbContextError:command:@"iBTVM updateConfig with no config object"];
    }
    
    if ([command.arguments count] > 1)
    {
        forceConfig = [[command.arguments objectAtIndex:1] boolValue];
    }
    
    NSMutableDictionary *json = [NSMutableDictionary dictionaryWithDictionary:[command.arguments objectAtIndex:0]];
    BOOL res = [self updateConfig4:json];
    
    if (res)
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:self.cfg];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    else
    {
        [self cbContextError:command:configError];
    }
}

- (void)connect:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;
    
    NSLog(@"Connecting to iBTVM");
    
    BOOL getConfig = true;
    
    if ((command.arguments != nil) && ([command.arguments count] > 1))
    {
        getConfig = [[command.arguments objectAtIndex:1] boolValue];
    }

    [super connect:command];
    
    if (isConnected && getConfig)
        [self downloadConfig];  //download config after connecting
    else
        forceConfig = YES;
}
    
- (void)start:(CDVInvokedUrlCommand*)command
{
    
    NSLog(@"Logging starting continueous call to get volts");
//    CMDLOG;

    // A'sa Andrew readfreq is used to calculate the timeout delay
    // increasing the readfreq to 50 instead of 20 made the device not disconnect
    // @"0.050" @"0.020" adding more characters to help find this faster
    
//    if (([command.arguments count] > 0) && ([command.arguments objectAtIndex:0] != [NSNull null]))
//        
//        readFreq = [[command.arguments objectAtIndex:0] intValue];
//    
//    else
//        readFreq = 20;		// default to 50ms (20/s) unless not allowed in particular AC range
    
    if (self.cfg == nil)
        self.cfg = [self createDefaultConfig];
    
    NSString *ACRejection = [[self.cfg objectForKey:@"ACRejection"] stringValue];    //an NSNumber inside
    
    
    
    readFreq = 50.0;
    
    if ([ACRejection isEqualToString:@"60"]) {
        if (readFreq < 35) readFreq = 20;
        else if (readFreq < 50) readFreq = 35;
        else if (readFreq < 71) readFreq = 50;
        else readFreq = (readFreq / 71) * 71;
    }
    else if ([ACRejection isEqualToString:@"50"]) {
        if (readFreq < 50) readFreq = 35;
        else if (readFreq < 71) readFreq = 50;
        else readFreq = (readFreq / 71) * 71;
    }
    else if ([ACRejection isEqualToString:@"30"] || [ACRejection isEqualToString:@"25"]) {
        if (readFreq < 71) readFreq = 50;
        else readFreq = (readFreq / 71) * 71;
    }
    else if ([ACRejection isEqualToString:@"16.67"]) {
        if (readFreq < 71) readFreq = 71;
        else readFreq = (readFreq / 71) * 71;
    }
    else {
        if (readFreq < 20)
            readFreq = 15;
        else if (readFreq < 35)
            readFreq = 20;
        else if (readFreq < 50)
            readFreq = 35;
        else if (readFreq < 71)
            readFreq = 50;
        else
            readFreq = (readFreq / 71) * 71;
    }
    if (readFreq > 9940) readFreq = 9940;	// maximum freq
    
    if (([command.arguments count] > 1) && ([command.arguments objectAtIndex:1] != [NSNull null]))
    {
        simpleCallback = [[command.arguments objectAtIndex:1] boolValue];
    }
   
    //can't use normal method because don't want start/stop pluginResult.

    if (!isInitialized)
    {
        [self cbContextError:command:@"iBTVM start action before initialize"];
        return;
    }
    
    if (!isConnected)
    {
        [self cbContextError:command:@"iBTVM start action before connect"];
        return;
    }
    
    if (self.jsCallbackContext != nil)
    {
        [self cbContextError:command:@"iBTVM start action when already started"];
        return;
    }

    [self.bt flushIncoming];
    
    @synchronized (self.rawWaveMutex)
    {
        [self.rawWave removeAllObjects];	// otherwise we can have reading left from prior start
    }
    // resetPulse - force refresh of 'pulse' with tablet clock or actual GPS time
    resetPulse = true;		// don't set gpsPulse=0, as used in background thread ParseVoltsTime() which could be part way through
	
    self.jsCallbackContext = [command.callbackId copy];

    
    NSString *cmd = [NSString stringWithFormat:@"C,%d", readFreq];
    [self.bt send:[self formatCommand:cmd]];       //start continuous reading mode
    [self.bt send:[self formatCommand:@"G2,1"]];      //start GPS 1 per second
    
    if (simpleCallback)
        [self cbContextSuccessMsg:command:@"started"];        // cbContext.success("started");

}

- (void)stop:(CDVInvokedUrlCommand*)command
{
//    CMDLOG;
    NSLog(@"Stop continuous call to get volts and stops GPS. Not done here but is done in the doStop method which is called from here.");
    
    if (self.jsCallbackContext == nil)
    {
        [self cbContextError:command:@"iBTVM stop action called when unstarted"];
        return;
    }
    
    [self doStop];
    
    self.jsCallbackContext = nil;
    
    if (simpleCallback)
        [self cbContextSuccessMsg:command:@"stopped"];        // cbContext.success("started");
    
}

// Voltmeter Configuration

- (NSMutableDictionary*)createDefaultConfig
{
//    CMDLOG;
    
    NSMutableDictionary *json = [NSMutableDictionary dictionaryWithCapacity:10];      //empty dictionary == new JSONObject()
    
    //everything has to be converted to an NSObject to be stored in a dictionary
    
    [json setObject:[NSNumber numberWithInt:5700] forKey:@"MeterRange"];
    [json setObject:@"DC" forKey:@"Current"]; //AC or DC
    [json setObject:[NSNumber numberWithInt:700] forKey:@"CycleOn"];
    [json setObject:[NSNumber numberWithInt:300] forKey:@"CycleOff"];
    [json setObject:[NSNumber numberWithInt:200] forKey:@"CycleIRFree"];
    [json setObject:[NSNumber numberWithInt:60] forKey:@"ACRejection"];
    [json setObject:@"single" forKey:@"ReadStyle"];
    [json setObject:[NSNumber numberWithBool:YES] forKey:@"HighImpedence"];
    [json setObject:[NSNumber numberWithBool:NO] forKey:@"GPSSynch"];
    
    return json;
}

- (void)fillDefaultConfig:(NSMutableDictionary*)json
{
//    CMDLOG;
    
    if (![json objectForKey:@"MeterRange"]) [json setObject:[NSNumber numberWithInt:5700] forKey:@"MeterRange"];
    if (![json objectForKey:@"Current"]) [json setObject:@"DC" forKey:@"Current"]; //AC or DC
    if (![json objectForKey:@"CycleOn"]) [json setObject:[NSNumber numberWithInt:700] forKey:@"CycleOn"];
    if (![json objectForKey:@"CycleOff"]) [json setObject:[NSNumber numberWithInt:300] forKey:@"CycleOff"];
    if (![json objectForKey:@"CycleIRFree"]) [json setObject:[NSNumber numberWithInt:200] forKey:@"CycleIRFree"];
    if (![json objectForKey:@"ACRejection"]) [json setObject:[NSNumber numberWithInt:60] forKey:@"ACRejection"];
    if (![json objectForKey:@"ReadStyle"]) [json setObject:@"single" forKey:@"ReadStyle"];
    if (![json objectForKey:@"HighImpedence"]) [json setObject:[NSNumber numberWithBool:YES] forKey:@"HighImpedence"];
    if (![json objectForKey:@"GPSSynch"]) [json setObject:[NSNumber numberWithBool:YES] forKey:@"GPSSynch"];
    
}


- (void)downloadConfig
{
//    CMDLOG;
    
    @try
    {
        if (self.cfg == nil)
            self.cfg = [self createDefaultConfig];
        
        [self.bt flushIncoming];
        
        NSString *cmd = [self formatCommand:@"R1"];
        NSString *hdr = [cmd substringToIndex:2];
        NSString *rsp = [self.bt sendCmdGetResp:@"" command:cmd header:hdr timeout:500];
        [self parseStatement:rsp];
        
        cmd = [self formatCommand:@"A1"];
        hdr = [cmd substringToIndex:2];
        NSString *acr = [self.bt sendCmdGetResp:@"" command:cmd header:hdr timeout:500];
        [self parseStatement:acr];
        
        forceConfig = NO;
        
        [self updateConfig2:self.cfg:vmRange:vmACRejection:vmReadStyle];
    }
    @catch (NSException *exception)
    {
//        NSLog(@"downloadConfig exception name: %@ reason: %@", [exception name],[exception reason]);
    }
}

- (BOOL)updateConfig2:(NSMutableDictionary*)ucfg :(int)vmr :(int)vmACR :(int)vmRd
{
//    CMDLOG;
    
    @try
    {
        switch (vmr)
        {
            case 0:
            {
                [ucfg setObject:[NSNumber numberWithInt:40] forKey:@"MeterRange"];
                [ucfg setObject:[NSNumber numberWithBool:NO] forKey:@"HighImpedence"];
                [ucfg setObject:@"DC" forKey:@"Current"];
                break;
            }
                
            case 1:
            {
                [ucfg setObject:[NSNumber numberWithInt:400] forKey:@"MeterRange"];
                [ucfg setObject:[NSNumber numberWithBool:NO] forKey:@"HighImpedence"];
                [ucfg setObject:@"DC" forKey:@"Current"];
                break;
            }
                
            case 2:
            {
                [ucfg setObject:[NSNumber numberWithInt:5700] forKey:@"MeterRange"];
                [ucfg setObject:[NSNumber numberWithBool:YES] forKey:@"HighImpedence"];
                [ucfg setObject:@"DC" forKey:@"Current"];
                break;
            }
                
            case 3:
            {
                [ucfg setObject:[NSNumber numberWithInt:40000] forKey:@"MeterRange"];
                [ucfg setObject:[NSNumber numberWithBool:NO] forKey:@"HighImpedence"];
                [ucfg setObject:@"DC" forKey:@"Current"];
                break;
            }
                
            case 4:
            {
                [ucfg setObject:[NSNumber numberWithInt:57000] forKey:@"MeterRange"];
                [ucfg setObject:[NSNumber numberWithBool:NO] forKey:@"HighImpedence"];
                [ucfg setObject:@"DC" forKey:@"Current"];
                break;
            }
                
            case 5:
            {
                [ucfg setObject:[NSNumber numberWithInt:57000] forKey:@"MeterRange"];
                [ucfg setObject:[NSNumber numberWithBool:YES] forKey:@"HighImpedence"];
                [ucfg setObject:@"DC" forKey:@"Current"];
                break;
            }
                
            case 6:
            {
                [ucfg setObject:[NSNumber numberWithInt:400000] forKey:@"MeterRange"];
                [ucfg setObject:[NSNumber numberWithBool:NO] forKey:@"HighImpedence"];
                [ucfg setObject:@"DC" forKey:@"Current"];
                break;
            }
                
            case 7:
            {
                [ucfg setObject:[NSNumber numberWithInt:570000] forKey:@"MeterRange"];
                [ucfg setObject:[NSNumber numberWithBool:NO] forKey:@"HighImpedence"];
                [ucfg setObject:@"DC" forKey:@"Current"];
                break;
            }
                
            case 8:
            {
                [ucfg setObject:[NSNumber numberWithInt:40000] forKey:@"MeterRange"];
                [ucfg setObject:[NSNumber numberWithBool:NO] forKey:@"HighImpedence"];
                [ucfg setObject:@"AC" forKey:@"Current"];
                break;
            }
                
            case 9:
            {
                [ucfg setObject:[NSNumber numberWithInt:400000] forKey:@"MeterRange"];
                [ucfg setObject:[NSNumber numberWithBool:NO] forKey:@"HighImpedence"];
                [ucfg setObject:@"AC" forKey:@"Current"];
                break;
            }
                
            default:
            {
                [ucfg setObject:[NSNumber numberWithInt:5700] forKey:@"MeterRange"];
                [ucfg setObject:[NSNumber numberWithBool:YES] forKey:@"HighImpedence"];
                [ucfg setObject:@"DC" forKey:@"Current"];
                break;
            }
        }
        
        switch (vmACR)
        {
            case 0: [ucfg setObject:[NSNumber numberWithInt:60] forKey:@"ACRejection"]; break;
            case 1: [ucfg setObject:[NSNumber numberWithInt:50] forKey:@"ACRejection"]; break;
            case 2: [ucfg setObject:[NSNumber numberWithInt:30] forKey:@"ACRejection"]; break;
            case 3: [ucfg setObject:[NSNumber numberWithInt:25] forKey:@"ACRejection"]; break;
            case 4: [ucfg setObject:[NSNumber numberWithInt:16.67] forKey:@"ACRejection"]; break;
            case 5:
            default:
                [ucfg setObject:[NSNumber numberWithInt:0] forKey:@"ACRejection"]; break;
        }
        
        switch (vmRd)
        {
            case 0: [ucfg setObject:@"single" forKey:@"ReadStyle"]; break;
            case 1: [ucfg setObject:@"on/off" forKey:@"ReadStyle"]; break;
            case 2: [ucfg setObject:@"max/min" forKey:@"ReadStyle"]; break;
            case 3: [ucfg setObject:@"single/predom" forKey:@"ReadStyle"]; break;
            default:
                [ucfg setObject:@"single" forKey:@"ReadStyle"]; break;
        }
        return YES;
    }
    @catch (NSException *exception)
    {
//        NSLog(@")updateConfig2 exception name: %@ reason: %@", [exception name],[exception reason]);
        return NO; //giving up
    }
}

- (int)getRange1:(NSMutableDictionary*)rcfg
{
//    CMDLOG;
    
    @try
    {
        int res;
        int MeterRange = [[rcfg objectForKey:@"MeterRange"] intValue];
        BOOL HighImpedence = [[rcfg objectForKey:@"HighImpedence"] boolValue];
        NSString *Current = [rcfg objectForKey:@"Current"];
        if (MeterRange == 40) {
            res = 0;
        } else if (MeterRange == 400) {
            res = 1;
        } else if (MeterRange == 5700) {
            res = 2;
        } else if (MeterRange == 40000) {
            if ([Current isEqualToString:@"DC"]) res = 3;
            else res = 8;
        } else if (MeterRange == 57000) {
            if (!HighImpedence) res = 4;
            else res = 5;  //res setting for 57 v ranges fixex LB 11/04/2014
        } else if (MeterRange == 400000) {
            if ([Current isEqualToString:@"DC"]) res = 6;
            else res = 9;
        } else if (MeterRange == 570000) {
            res = 7;
        } else {
            res = 2;		// 5700
        }
        return res;
    }
    @catch (NSException *exception)
    {
//        NSLog(@"getRange1 exception name: %@ reason: %@", [exception name],[exception reason]);
        return 2;
    }
}

// seems easier to share same values between config, and what javascript will send
- (int)getReadStyle:(NSString*)readstyle
{
    int res = 0;
    if ([self SameStr:readstyle str2:@"single"])
        res = 0;
    else if ([self SameStr:readstyle str2:@"on/off"])
        res = 1;
    else if ([self SameStr:readstyle str2:@"max/min"])
        res = 2;
    else if ([self SameStr:readstyle str2:@"single/predom"])
        res = 3;
    return res;
}

- (int)getReadStyle2:(NSMutableDictionary*)scfg
{
//    CMDLOG;
    
    @try
    {
        int res = 0;
        NSString *readStyle = [scfg objectForKey:@"ReadStyle"];
        res = [self getReadStyle:readStyle];
        return res;
    }
    @catch (NSException *exception)
    {
//        NSLog(@"getReadStyle2 exception name: %@ reason: %@", [exception name],[exception reason]);
        return 0;
    }
}


- (int)getACRejection1:(NSMutableDictionary*)gcfg
{
//    CMDLOG;
    
    @try
    {
        int res;
        
        int ACRejection = [[gcfg objectForKey:@"ACRejection"] intValue];
        if (ACRejection == 60) res = 0;
        else if (ACRejection == 50) res = 1;
        else if (ACRejection == 30) res = 2;
        else if (ACRejection == 25) res = 3;
        else if (ACRejection == 16.67) res = 4;
        else res = 5;
        return res;
    }
    @catch (NSException *exception)
    {
//        NSLog(@"getACRejection1 exception name: %@ reason: %@", [exception name],[exception reason]);
        return 5;
    }
}

- (int)getRange2:(NSString*)range
{
//    CMDLOG;
    
    int res = 2;
    
    if ([self SameStr:range str2:@"40 mVDC 10Mohm"])
        res = 0;
    else if ([self SameStr:range str2:@"400 mVDC 10Mohm"])
        res = 1;
    else if ([self SameStr:range str2:@"5.7 VDC 400Mohm"])
        res = 2;
    else if ([self SameStr:range str2:@"40 VDC  75Mohm"])
        res = 3;
    else if ([self SameStr:range str2:@"57 VDC  75Mohm"])
        res = 4;
    else if ([self SameStr:range str2:@"57 VDC  400Mohm"])
        res = 5;
    else if ([self SameStr:range str2:@"400 VDC 75Mohm"])
        res = 6;
    else if ([self SameStr:range str2:@"570 VDC 75Mohm"])
        res = 7;
    else if ([self SameStr:range str2:@"40 VAC  75Mohm"])
        res = 8;
    else if ([self SameStr:range str2:@"400 VAC 75Mohm"])
        res = 9;
    
    return res;
}
    
- (int)getACRejection2:(NSString *)acreject
{
//    CMDLOG;
    
    int res = 5;
    
    if ([self SameStr:acreject str2:@"60Hz AC rejection"])
        res = 0;
    else if ([self SameStr:acreject str2:@"50Hz AC rejection"])
        res = 1;
    else if ([self SameStr:acreject str2:@"30Hz AC rejection"])
        res = 2;
    else if ([self SameStr:acreject str2:@"25Hz AC rejection"])
        res = 3;
    else if ([self SameStr:acreject str2:@"16.6Hz AC rejection"])
        res = 4;
    else if ([self SameStr:acreject str2:@"No AC rejection"])
        res =  5;
    return res;
}

// json object is of form { range: "5.7 VDC 400Mohm", acreject: "60Hz AC rejection" }
- (BOOL)updateConfig3:(NSMutableDictionary*)ucfg :(NSMutableDictionary*)json
{
//    CMDLOG;
    
    @try
    {
        if (ucfg == nil)
            ucfg = [self createDefaultConfig];
        
        // parse out drop-down contents which are different from internal cfg object values
        // (objectForKey return value: The value associated with aKey, or nil if no value is associated with aKey. http://uplink.to/b0s )
        
        if ([json objectForKey:@"range"]) {
            vmRange = [self getRange2:[json objectForKey:@"range"]];
        }
        if ([json objectForKey:@"acreject"]) {
            vmACRejection = [self getACRejection2:[json objectForKey:@"acreject"]];
        }
        if ([json objectForKey:@"readstyle"]) {
            vmReadStyle = [self getReadStyle:[json objectForKey:@"readstyle"]];
        }
        
        // make sure we don't fall afoul of missing config
        [self fillDefaultConfig:json];
        
        // parse out any other separate items potentially coming from drop-downs
        if ([json objectForKey:@"CycleOn"])     [ucfg setObject:[json objectForKey:@"CycleOn"]     forKey:@"CycleOn"];
        msOn = [[ucfg objectForKey:@"CycleOn"] intValue];
        if ([json objectForKey:@"CycleOff"])    [ucfg setObject:[json objectForKey:@"CycleOff"]    forKey:@"CycleOff"];
        msOff = [[ucfg objectForKey:@"CycleOff"] intValue];
        if ([json objectForKey:@"CycleIRFree"]) [ucfg setObject:[json objectForKey:@"CycleIRFree"] forKey:@"CycleIRFree"];
        // ? = CycleIRFree
        
        if ([json objectForKey:@"GPSSynch"])  [ucfg setObject:[json objectForKey:@"GPSSynch"]     forKey:@"GPSSynch"];
        gpsSynchOnOff = [[ucfg objectForKey:@"GPSSynch"] boolValue];
        
    }
    @catch (NSException *exception)
    {
//        NSLog(@"updateConfig3 exception name: %@ reason: %@", [exception name],[exception reason]);
        return NO; //giving up
    }
    
    return [self updateConfig2:ucfg :vmRange :vmACRejection :vmReadStyle];
}

// json object is of form { range: "5.7 VDC 400Mohm", acreject: "60Hz AC rejection" }
- (BOOL)updateConfig4:(NSMutableDictionary*)json
{
//    CMDLOG;
    
    @try
    {
        if (self.cfg == nil)
            self.cfg = [self createDefaultConfig];
        
        NSMutableDictionary *new_cfg = [NSMutableDictionary dictionaryWithDictionary:self.cfg];  // JSONObject new_cfg = new JSONObject(cfg.toString()); ?
        BOOL res = [self updateConfig3:new_cfg :json];
        res = res && [self applyConfig:new_cfg];
        return res;
    }
    @catch (NSException *exception)
    {
//        NSLog(@"updateConfig4 exception name: %@ reason: %@", [exception name],[exception reason]);
        return NO; //giving up
    }
    
}

- (BOOL)configsDiffer:(NSMutableDictionary*)new_cfg :(NSMutableDictionary*)acfg :(NSString*)token
{
//    CMDLOG;
    
    @try
    {   // objectForKey will return nil if a key doesn't exist.
        if (![new_cfg objectForKey:token] || ![acfg objectForKey:token]) {
            return false;
        } else {
            return ![[new_cfg objectForKey:token] isEqual:[acfg objectForKey:token]];

        }
    }
    @catch (NSException *exception)
    {
//        NSLog(@"configsDiffer exception name: %@ reason: %@", [exception name],[exception reason]);
        return NO; //giving up
    }

}

// new_cfg object is of form { MeterRange: 5700, HighImpedence: false, Current: "DC", ACRejection: 60, .... }
- (BOOL)applyConfig:(NSMutableDictionary*)new_cfg
{
//    CMDLOG;

    @try
    {
        // if in continuous mode should probably stop/start
        
        if (self.cfg == nil) {
            self.cfg = [self createDefaultConfig];
        }
        
        // make sure we don't fall afoul of missing config
        [self fillDefaultConfig:new_cfg];
        
        if (forceConfig ||
            [self configsDiffer:new_cfg :self.cfg :@"MeterRange"] ||      // all NSNumbers here
            [self configsDiffer:new_cfg :self.cfg :@"Current"]    ||
            [self configsDiffer:new_cfg :self.cfg :@"HighImpedence"])
        {
            vmRange = [self getRange1:new_cfg];
            NSString *cmd = [self formatCommand:[NSString stringWithFormat:@"R0,%d",vmRange]];
            [self.bt sendCmd:@"" command:cmd];		// not checking return value - assuming it works!
            
            [self.cfg setObject:[new_cfg objectForKey:@"MeterRange"]    forKey:@"MeterRange"];
            [self.cfg setObject:[new_cfg objectForKey:@"Current"]       forKey:@"Current"];
            [self.cfg setObject:[new_cfg objectForKey:@"HighImpedence"] forKey:@"HighImpedence"];
        }
        
        if (forceConfig ||
            [self configsDiffer:new_cfg :self.cfg :@"ACRejection"])
        {
            vmACRejection = [self getACRejection1:new_cfg];
            NSString *cmd = [self formatCommand:[NSString stringWithFormat:@"A0,%d",vmACRejection]];
            [self.bt sendCmd:@"" command:cmd];		// not checking return value - assuming it works!
            [self.cfg setObject:[new_cfg objectForKey:@"ACRejection"] forKey:@"ACRejection"];
        }
        
        @try {
            // new_cfg.ReadStyle from TSS app is in form: 0,1,2 - not "on/off", etc.
            vmReadStyle = [[new_cfg objectForKey:@"ReadStyle"] intValue];
            [self updateConfig2:cfg :vmRange :vmACRejection :vmReadStyle];
        }  @catch (NSException *exception) {
            // Just in case ReadStyle *is* in form: "on/off", etc. as it would be if filled with default because empty
            [self.cfg setObject:[new_cfg objectForKey:@"ReadStyle"] forKey:@"ReadStyle"];
            vmReadStyle = [self getReadStyle:[new_cfg objectForKey:@"ReadStyle"]];
        }
        [self.cfg setObject:[new_cfg objectForKey:@"ReadStyle"]   forKey:@"ReadStyle"];
        
        [self.cfg setObject:[new_cfg objectForKey:@"CycleOn"]     forKey:@"CycleOn"];
        msOn = [[self.cfg objectForKey:@"CycleOn"] intValue];
        [self.cfg setObject:[new_cfg objectForKey:@"CycleOff"]    forKey:@"CycleOff"];
        msOff = [[self.cfg objectForKey:@"CycleOff"] intValue];
        [self.cfg setObject:[new_cfg objectForKey:@"CycleIRFree"] forKey:@"CycleIRFree"];
        
        [self.cfg setObject:[new_cfg objectForKey:@"GPSSynch"]     forKey:@"GPSSynch"];
        gpsSynchOnOff = [[self.cfg objectForKey:@"GPSSynch"] boolValue];
        
        forceConfig = NO;
        
    }
    @catch (NSException *exception)
    {
//        NSLog(@"applyConfig exception name: %@ reason: %@", [exception name],[exception reason]);
        return NO; //giving up
    }
    
    return YES;
}

@end
