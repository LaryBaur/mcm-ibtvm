//
//  SendBroadcastPlugin.h
//  MCM iBTVM
//
//  Created by user on 11/13/13.
//
//

#import <Cordova/CDV.h>

@interface SendBroadcastPlugin : CDVPlugin

- (void)MediaScanFile:(CDVInvokedUrlCommand*)command;

@end

