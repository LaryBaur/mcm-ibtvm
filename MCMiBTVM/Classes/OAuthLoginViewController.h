
#import <UIKit/UIKit.h>

@interface OAuthLoginViewController : UIViewController

@property (strong, nonatomic) NSString *userToken;
@property (strong, nonatomic) IBOutlet UIWebView *webview;

@end
