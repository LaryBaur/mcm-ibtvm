//
//  VersionInfo.h
//  ProCPNavPad
//
//  Created by Mark on 5/3/13.
//
//

#import <Cordova/CDV.h>

@interface VersionInfo : CDVPlugin

- (void)GetVersionName:(CDVInvokedUrlCommand*)command;
- (void)GetVersionCode:(CDVInvokedUrlCommand*)command;
- (void)GetAppName:(CDVInvokedUrlCommand*)command;
- (void)GetValueString:(CDVInvokedUrlCommand*)command;

@end
