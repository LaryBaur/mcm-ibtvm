//
//  ExternalStoragePlugin.m
//  MCM iBTVM
//
//  Created by user on 11/13/13.
//
//

#import "ExternalStoragePlugin.h"
#import "AppDelegate.h"

@implementation ExternalStoragePlugin


#pragma mark ======== init method =========
/*
 ** --------------------------------------------------------
 **  init method
 ** --------------------------------------------------------
 */
- init
{
	if (self = [super init])
	{
		CMDLOG;
	}
	return self;
}

/*
 
- (void)echo:(CDVInvokedUrlCommand*)command
 {
 NSString *result
 CDVPluginResult* pluginResult = nil;
 NSString* echo = [command.arguments objectAtIndex:0];
 
 if (echo != nil && [echo length] > 0) {
 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
 } else {
 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
 }
 
 [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
 }

*/


- (void)sd_card:(CDVInvokedUrlCommand*)command
{
    CMDLOG;
    
    NSString *result = nil;
    CDVPluginResult* pluginResult = nil;
    
        //no sd card
    
    if (result != nil && [result length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)external_sd_card:(CDVInvokedUrlCommand*)command
{
    CMDLOG;
    
    NSString *result = nil;
    CDVPluginResult* pluginResult = nil;
    
    //no external sd card
    
    if (result != nil && [result length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}



@end
