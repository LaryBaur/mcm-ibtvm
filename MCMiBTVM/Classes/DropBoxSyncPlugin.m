//
//  DropboxPlugin.m
//  PhoneGapSync
//


#import "DropBoxSyncPlugin.h"

#import "AppDelegate.h"

#import <Dropbox/Dropbox.h>
#import "Dropbox.h"

#import "OAuthLoginViewController.h"

// based on http://coenraets.org/blog/2013/06/phonegap-plugin-for-the-dropbox-sync-api/
// not 100% paramater return same as Android yet


@interface DropBoxSyncPlugin()

@property (nonatomic, strong) NSURLSessionUploadTask *uploadTask;
@property (nonatomic, strong) CDVInvokedUrlCommand *uploadCommand;

@end

@implementation DropBoxSyncPlugin

- (void)pluginInitialize
{
    CMDLOG;

}

- (void)readData:(CDVInvokedUrlCommand*)command
{
    NSLog(@"Executing readData()");

    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* path = [command.arguments objectAtIndex:0];
    
        DBPath *newPath = [[DBPath root] childPath:path];
        DBFile *file = [[DBFilesystem sharedFilesystem] openFile:newPath error:nil];
    
        NSData *data = [file readData:nil];
    
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArrayBuffer: data];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
    
}


- (void)readString:(CDVInvokedUrlCommand*)command
{
    NSLog(@"Executing readString()");

    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* path = [command.arguments objectAtIndex:0];
        
        DBPath *newPath = [[DBPath root] childPath:path];
        DBFile *file = [[DBFilesystem sharedFilesystem] openFile:newPath error:nil];
        
        NSString *data = [file readString:nil];
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: data];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
    
    
}

//-------------------------------------------------------------------------------------------------------------------------------
// 'execute' commands from js

- (void)isLinked:(CDVInvokedUrlCommand*)command
{
	CMDLOG;
    
    CDVCommandStatus commandStatus;
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"dbapi-1://"]]) {
//        
//        id requestToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"requestToken"];
//        id requestTokenSecret = [[NSUserDefaults standardUserDefaults] objectForKey:@"requestTokenSecret"];
//
//        if (requestToken != nil && requestTokenSecret != nil) {
//            commandStatus = CDVCommandStatus_OK;
//        } else {
//            commandStatus = CDVCommandStatus_ERROR;
//        }
//        
//    } else {
        DBAccount* account = [[DBAccountManager sharedManager] linkedAccount];
        commandStatus = (account ? CDVCommandStatus_OK : CDVCommandStatus_ERROR);
//    }
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:commandStatus];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}

- (void)linkAccount:(CDVInvokedUrlCommand*)command
{
	CMDLOG;
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    UIViewController *topView = appDelegate.viewController;

        NSLog(@"Dropbox is not installed.");
        [[DBAccountManager sharedManager] linkFromController:topView];
    
    
    appDelegate.dropBoxLinkCommand = command;
    appDelegate.dropBoxCommandDelegate = self.commandDelegate;
    
}


- (void)unlinkAccount:(CDVInvokedUrlCommand*)command
{
	CMDLOG;
    
    CDVPluginResult* pluginResult = nil;
    
    DBAccountManager *accountManager = [DBAccountManager sharedManager];
    
    [[accountManager linkedAccount] unlink];
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}

- (void)listFiles:(CDVInvokedUrlCommand*)command
{
	CMDLOG;
    
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* path = [command.arguments objectAtIndex:0];
        
        DBError *error;
        
        DBPath *newPath = [[DBPath root] childPath:path];
        NSArray *files = [[DBFilesystem sharedFilesystem] listFolder:newPath error:&error];
        
        NSMutableArray *items = [NSMutableArray array];
        
        for (DBFileInfo *file in files) {
            NSLog(@"\t%@", file.path.stringValue);
            NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
            [dictionary setObject:file.path.stringValue forKey:@"path"];
            [dictionary setObject:[NSNumber numberWithLongLong:[file.modifiedTime timeIntervalSince1970]*1000] forKey:@"modifiedTime"];
            [dictionary setObject:@(file.size) forKey:@"size"];
            [dictionary setValue:[NSNumber numberWithBool:file.isFolder] forKey:@"isFolder"];
            [items addObject:dictionary];
        }
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray: items];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];

	
}

- (void)createFolder:(CDVInvokedUrlCommand*)command
{
	CMDLOG;

    
    DBAccount *account = [[DBAccountManager sharedManager] linkedAccount];
    CDVPluginResult* pluginResult = nil;

    if (account) {

        DBFilesystem *filesystem = [[DBFilesystem alloc] initWithAccount:account];
        [DBFilesystem setSharedFilesystem:filesystem];
        NSString* path = [command.arguments objectAtIndex:0];
        DBPath *newPath = [[DBPath root] childPath:path];
        
        DBError *error;
        
        if ([filesystem createFolder:newPath error:&error] ) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            NSLog(@"Error Code: %d", error.code);
        }
        
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];

    }
    
//    CDVPluginResult* pluginResult = nil;
//    
//    NSString* path = [command.arguments objectAtIndex:0];
//    DBPath *newPath = [[DBPath root] childPath:path];
//    
//    DBError *error;
//    
//    if ([[DBFilesystem sharedFilesystem] createFolder:newPath error:&error])
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
//    else
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    
    
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
 
	
}

//note that this is not a full ForceDirectories type of thing

- (void)ensureFolderExists:(CDVInvokedUrlCommand*)command
{
	CMDLOG;
    
    CDVPluginResult* pluginResult = nil;
    
    NSString* path = [command.arguments objectAtIndex:0];
    DBPath *newPath = [[DBPath root] childPath:path];

    /** Returns the [file info](DBFileInfo) for the file or folder at `path`, or
     `nil` if an error occurred.  If there is no file or folder at `path`, returns
     `nil` and sets `error` to `DBErrorParamsNotFound`.*/
    
    if ([[DBFilesystem sharedFilesystem] fileInfoForPath:newPath error:nil])
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }
    
    [self createFolder:command];
}

- (void)deleteFolderOrFile:(CDVInvokedUrlCommand*)command
{
	CMDLOG;
    
    CDVPluginResult* pluginResult = nil;
    
    NSString* path = [command.arguments objectAtIndex:0];
    DBPath *newPath = [[DBPath root] childPath:path];
    
    if ([[DBFilesystem sharedFilesystem] createFolder:newPath error:nil])
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    else
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}

- (void)fileOrFolderExists:(CDVInvokedUrlCommand*)command
{
	CMDLOG;
    
    CDVPluginResult* pluginResult = nil;
    DBError *dberr = nil;
    
    NSString* pth = [command.arguments objectAtIndex:0];
    
    NSMutableDictionary *json = [NSMutableDictionary dictionaryWithCapacity:10];      //empty dictionary == new JSONObject()
    
    [json setObject:pth forKey:@"path"];
    
    DBPath *newPath = [[DBPath root] childPath:pth];
    
    /** Returns the [file info](DBFileInfo) for the file or folder at `path`, or
     `nil` if an error occurred.  If there is no file or folder at `path`, returns
     `nil` and sets `error` to `DBErrorParamsNotFound`.*/
    
    DBFileInfo *info = [[DBFilesystem sharedFilesystem] fileInfoForPath:newPath error:&dberr];
    
    if (info)
    {
        [json setObject:[NSNumber numberWithBool:YES] forKey:@"exists"];
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:json];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }

    if ([dberr code] == DBErrorParamsNoThumb)
    {
        [json setObject:[NSNumber numberWithBool:NO] forKey:@"exists"];
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:json];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}

- (void)addFile:(CDVInvokedUrlCommand*)command
{
	CMDLOG;
    
    self.uploadCommand = command;
    CDVPluginResult* pluginResult = nil;
    BOOL stealIt = YES;
    
    if ([command.arguments count] < 2)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Too few args to addFile (to Dropbox)"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }
    
    // we want to get the only the file name so i will cheat and just extract the content from
    // the url instead of changing the javascript code to return a proper url
    NSString *src = [command.arguments objectAtIndex:0];
    
    NSArray *filePathComponents = [src componentsSeparatedByString:@"/"];
    NSString *fileName = filePathComponents.lastObject;
    
    NSDate *nowDate = [NSDate date];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"YYYYMMddHHmmss";
    NSString *dateString = [dateFormatter stringFromDate:nowDate];
    
    NSArray *fileNameComponents = [fileName componentsSeparatedByString:@"."];
    NSString *fileNamePrefix = fileNameComponents[0];
    NSString *fileNameExtension = fileNameComponents[1];
    
    NSString *src2 = [NSString stringWithFormat:@"%@_%@.%@", fileNamePrefix, dateString, fileNameExtension];
    
    
    BOOL ovrwrite = NO; // [[command.arguments objectAtIndex:1] boolValue];
    
    NSString *dst = src2; // [src lastPathComponent]; //copy off the filename
    
//    if ([command.arguments count] > 2)
//    {
//        //explicit output file (no stealing)
//        dst = [command.arguments objectAtIndex:2];          //? dst never gets used
//        stealIt = NO;
//    }
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:src])
    {
        NSString *errs = [NSString stringWithFormat:@"Source file %@ does not exist",src];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errs];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }
    
    
    
    DBPath *dfp = [[DBPath root] childPath:dst];
    DBFile *df;
    DBError *dberr = nil;
    DBFilesystem *sharedFilesystem = [DBFilesystem sharedFilesystem];
    
    if (ovrwrite)
        [sharedFilesystem deletePath:dfp error:nil];     //eat any exception (ignore method result)
    
//    
//    DBFilesystem *currentFileSystem = [[DBFilesystem alloc] initWithAccount:[[DBAccountManager sharedManager] linkedAccount]];
    
    df = [sharedFilesystem createFile:dfp error:&dberr];
    DBAccount *linkedAccount = [DBAccountManager sharedManager].linkedAccount;
    NSLog(@"Linked Account: %@", linkedAccount);
    
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"dbapi-1://"]]) {
//        [self uploadFileFromPath:src name:src2];
//    } else {
    
        if (df)
        {
            [df writeContentsOfFile:src shouldSteal:stealIt error:&dberr];
            [df close];
            if (dberr)
            {
                NSString *errs = [NSString stringWithFormat:@"Error writing Dropbox file %@",dst];      //again dst never get used elsewhere?
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errs];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                return;
            }
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            return;
        }
        
        NSString *errs = [NSString stringWithFormat:@"Error sending file to Dropbox %@",dst];      //again dst never get used elsewhere?
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errs];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
//    }
    
    


}

- (void)getFile:(CDVInvokedUrlCommand*)command
{
	CMDLOG;
    
    CDVPluginResult* pluginResult = nil;
    
    if ([command.arguments count] < 1)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Too few args to getFile (from Dropbox)"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }

    
    NSString *src = [command.arguments objectAtIndex:0];
    DBPath *sfp = [[DBPath root] childPath:src];
    DBFile *sf = nil;
    
    sf = [[DBFilesystem sharedFilesystem] openFile:sfp error:nil];
    
    if (!sf)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"openFile error (from Dropbox)"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }
    
	
}

- (void)uploadFileFromPath:(NSString*)filePath name:(NSString*)name {
    
    // https://api-content.dropbox.com/1/files_put/dropbox/App/photos/byteclub_pano_512.jpg
    
    NSData *fileData = [[NSFileManager defaultManager] contentsAtPath:filePath];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.HTTPMaximumConnectionsPerHost = 1;
    [config setHTTPAdditionalHeaders:@{@"Authorization": [Dropbox apiAuthorizationHeader]}];
    
    NSURLSession *upLoadSession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    
    NSString *properExtension = [NSString stringWithFormat:@"MCMiBTVM/%@", name];
    
    NSMutableURLRequest *uploadRequest = [[NSMutableURLRequest alloc] initWithURL:[Dropbox uploadURLForPath:properExtension]];
    [uploadRequest setHTTPMethod:@"PUT"];
    
    self.uploadTask = [upLoadSession uploadTaskWithRequest:uploadRequest fromData:fileData];
    
    [self.uploadTask resume];
    
//    [self performSelectorInBackground:@selector(upload) withObject:nil];
    
    
}

- (void)upload {
    [self.uploadTask resume];
}

#pragma mark - NSURLSessionDelegate
- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
   didSendBodyData:(int64_t)bytesSent
    totalBytesSent:(int64_t)totalBytesSent
totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend
{
    
    NSLog(@"Total Bytes Expected To Send: %lld", totalBytesExpectedToSend);
    NSLog(@"Total Bytes Sent: %lld", totalBytesSent);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
    });
}

- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{

    dispatch_async(dispatch_get_main_queue(), ^{

    });

}

@end
