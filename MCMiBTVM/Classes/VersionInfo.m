//
//  VersionInfo.m
//  ProCPNavPad
//
//  Created by Mark on 5/3/13.
//
//

#import "VersionInfo.h"
#import "AppDelegate.h"

@implementation VersionInfo


#pragma mark ======== init method =========
/*
 ** --------------------------------------------------------
 **  init method
 ** --------------------------------------------------------
 */
- init
{
	if (self = [super init])
	{
		CMDLOG;
	}
	return self;
}

/*
 - (void)echo:(CDVInvokedUrlCommand*)command
 {
 NSString *result 
 CDVPluginResult* pluginResult = nil;
 NSString* echo = [command.arguments objectAtIndex:0];
 
 if (echo != nil && [echo length] > 0) {
 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
 } else {
 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
 }
 
 [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
 }
 */


- (void)GetVersionName:(CDVInvokedUrlCommand*)command
{
    CMDLOG;
    
    NSString *result = nil;
    CDVPluginResult* pluginResult = nil;

    NSBundle* mainBundle = [NSBundle mainBundle];
    
    NSLog([[mainBundle infoDictionary] description],nil);
    
    NSString *buildno = [mainBundle objectForInfoDictionaryKey:@"CFBundleShortVersionString"];   //build number
    NSString *version = [mainBundle objectForInfoDictionaryKey:@"CFBundleVersion"];
    
    result = [NSString stringWithFormat:@"%@.%@", version,buildno];     //for dev display purposes
    
    if (result != nil && [result length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


- (void)GetVersionCode:(CDVInvokedUrlCommand*)command
{
    CMDLOG;
    
    NSString *result = nil;
    CDVPluginResult* pluginResult = nil;
    
    NSBundle* mainBundle = [NSBundle mainBundle];
    result = [mainBundle objectForInfoDictionaryKey:@"CFBundleVersion"];    //build number
    
    if (result != nil && [result length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}

- (void)GetAppName:(CDVInvokedUrlCommand*)command
{
    CMDLOG;
    
    NSString *result = nil;
    CDVPluginResult* pluginResult = nil;
    
    NSBundle* mainBundle = [NSBundle mainBundle];
    result = [mainBundle objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        
    if (result != nil && [result length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)GetValueString:(CDVInvokedUrlCommand*)command;
{
    CMDLOG;
    
    NSString *result = nil;
    CDVPluginResult* pluginResult = nil;
    
    NSBundle* mainBundle = [NSBundle mainBundle];
    result = [mainBundle objectForInfoDictionaryKey:@"CFBundleIdentifier"];
    
    if (result != nil && [result length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


@end
