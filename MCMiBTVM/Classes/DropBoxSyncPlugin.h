//
//  DropboxPlugin.h
//  PhoneGapSync
//
//
//

#import <Cordova/CDV.h>


@interface DropBoxSyncPlugin : CDVPlugin <NSURLSessionTaskDelegate>

- (void)isLinked:(CDVInvokedUrlCommand*)command;
- (void)linkAccount:(CDVInvokedUrlCommand*)command;
- (void)unlinkAccount:(CDVInvokedUrlCommand*)command;
- (void)listFiles:(CDVInvokedUrlCommand*)command;
- (void)createFolder:(CDVInvokedUrlCommand*)command;
- (void)ensureFolderExists:(CDVInvokedUrlCommand*)command;
- (void)deleteFolderOrFile:(CDVInvokedUrlCommand*)command;
- (void)fileOrFolderExists:(CDVInvokedUrlCommand*)command;
- (void)addFile:(CDVInvokedUrlCommand*)command;
- (void)getFile:(CDVInvokedUrlCommand*)command;

@end
