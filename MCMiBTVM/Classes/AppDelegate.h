/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  AppDelegate.h
//  MCMiBTVM
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Cordova/CDVViewController.h>
#import "JSON.h"

//potential Android version of this http://stackoverflow.com/a/8592871

#ifdef DEBUG
#define CMDLOG NSLog(@"%@ : %s",[self description],(char*)sel_getName(_cmd))
#pragma message("             *** DEBUG BUILD ***")
#else
#define CMDLOG
#define NSLog
#pragma message("             *** RELEASE BUILD ***")
#endif


@interface AppDelegate : NSObject <UIApplicationDelegate>{}

// invoke string is passed to your app on launch, this is only valid if you
// edit MCMiBTVM-Info.plist to add a protocol
// a simple tutorial can be found here :
// http://iphonedevelopertips.com/cocoa/launching-your-own-application-via-a-custom-url-scheme.html

@property (nonatomic, strong) IBOutlet UIWindow* window;
@property (nonatomic, strong) IBOutlet CDVViewController* viewController;

// this is set when the javascript calls to link the dropbox
@property (nonatomic, strong) CDVInvokedUrlCommand *dropBoxLinkCommand;
@property (nonatomic, strong) id<CDVCommandDelegate> dropBoxCommandDelegate;

@end

@implementation NSString (util)

- (int) indexOf:(NSString *)text
{
    NSRange range = [self rangeOfString:text];
    if ( range.length > 0 ) {
        return range.location;
    } else {
        return -1;
    }
}

- (BOOL)contains:(NSString *)string
{
    NSRange range = [self rangeOfString:string];
    return (range.location != NSNotFound);
}

- (BOOL)isEmpty
{
    return ([self length] == 0);
}

@end

@interface NSMutableArray (ArrayDeque)
- (id)removeFirst;
- (id)peekFirst;
- (void)addLast:(id)obj;
@end

@implementation NSMutableArray (ArrayDeque)
// Queues are first-in-first-out, so we remove objects from the head
- (id)removeFirst
{
    if ([self count] == 0)
        return nil; // to avoid raising exception (Quinn)
    id headObject = [self objectAtIndex:0];
    if (headObject != nil)
    {
        [[headObject retain] autorelease]; // so it isn't dealloc'ed on remove
        [self removeObjectAtIndex:0];
    }
    return headObject;
}

// so our Android guests feel at home
- (id)peekFirst
{
    if ([self count] == 0)
        return nil; // to avoid raising exception
    
    return [self objectAtIndex:0];
}

// Add to the tail of the queue (no one likes it when people cut in line!)
- (void)addLast:(id)anObject
{
    [self addObject:anObject];
    //this method automatically adds to the end of the array
}
@end

@interface NSMutableArray (Stack)

- (void) push: (id)item;
- (id) pop;
- (id) peek;

@end

@implementation NSMutableArray (Stack)

- (void) push: (id)item {
    [self addObject:item];
}

- (id) pop {
    id item = nil;
    if ([self count] != 0) {
        item = [[[self lastObject] retain] autorelease];
        [self removeLastObject];
    }
    return item;
}

- (id) peek {
    id item = nil;
    if ([self count] != 0) {
        item = [[[self lastObject] retain] autorelease];
    }
    return item;
}

@end

//!!

@interface NSDate (Milliseconds)

- (double) getTime;

@end

@implementation NSDate (Milliseconds)


- (double) getTime {
    NSTimeInterval ti = [self timeIntervalSince1970];
    double d = ti * 1000;
    double ms = round(d);
    return ms;
}

@end

