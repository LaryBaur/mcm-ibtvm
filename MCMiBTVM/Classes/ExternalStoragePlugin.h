//
//  ExternalStoragePlugin.h
//  MCM iBTVM
//
//  Created by user on 11/13/13.
//
//

#import <Cordova/CDV.h>

@interface ExternalStoragePlugin : CDVPlugin

- (void)sd_card:(CDVInvokedUrlCommand*)command;
- (void)external_sd_card:(CDVInvokedUrlCommand*)command;

@end

